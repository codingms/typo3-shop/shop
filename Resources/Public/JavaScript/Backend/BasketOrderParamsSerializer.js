define([
    'jquery'
], function (jQuery) {
    /**
     *
     * @constructor
     */
    function BasketOrderParamsSerializer() {
        jQuery('document').ready(function () {
            initFields();
            initDeliveryAddress();
        });
    }

    function initFields() {
        var paramsHiddenInput = jQuery('[name$="[params]"]');
        var params = JSON.parse(jQuery('#field-definition-json-object').get(0).textContent);
        jQuery('[data-fieldname]').each(function () {
            if (typeof params.fields[jQuery(this).data('fieldname')] !== 'undefined') {
                if (typeof params.fields[jQuery(this).data('fieldname')].value === 'undefined') {
                    params.fields[jQuery(this).data('fieldname')].value = '';
                } else if (typeof params.fields[jQuery(this).data('fieldname')].value !== '') {
                    if (jQuery(this).attr('type') === 'checkbox') {
                        this.checked = params.fields[jQuery(this).data('fieldname')].value === '1';
                    } else {
                        jQuery(this).val(params.fields[jQuery(this).data('fieldname')].value);
                    }
                }
                jQuery(this).on('change', function () {
                    if (jQuery(this).attr('type') === 'checkbox') {
                        params.fields[jQuery(this).data('fieldname')].value = this.checked ? '1' : '0';
                    } else {
                        params.fields[jQuery(this).data('fieldname')].value = jQuery(this).val();
                    }
                    paramsHiddenInput.val(JSON.stringify(params));
                });
            }
        });
        paramsHiddenInput.val(JSON.stringify(params));
    }

    function initDeliveryAddress() {
        var deliveryDataToggle = jQuery('[data-delivery-toggle=1]');
        var dataDeliveryFields = jQuery('[data-delivery=1]');
        setDeliveryAddressFieldVisibility(deliveryDataToggle.get(0).checked);
        deliveryDataToggle.on('change', function (e) {
            setDeliveryAddressFieldVisibility(e.target.checked, dataDeliveryFields);
        });
    }

    function setDeliveryAddressFieldVisibility(visible) {
        var dataDeliveryFields = jQuery('[data-delivery=1]');
        dataDeliveryFields.each(function (_, field) {
            if (visible) {
                field.style = '';
            } else {
                field.style = 'display:none;';
            }
        });
    }

    return new BasketOrderParamsSerializer();

});
