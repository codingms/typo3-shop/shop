# SQL-Queries

## Get all ordered products
SELECT
	basketorder.uid,
	basketorder.invoice_number,
	basketorder.status,
	basketorder.type,
	basketorder.name,
	basketorder.email,
	basketorder.basket,
	basketitem.quantity,
	basketitem.price_with_tax,
	product.product_no,
	product.title

FROM `tx_shop_domain_model_basketorder` AS basketorder,
	`tx_shop_domain_model_basket` AS basket,
	`tx_shop_domain_model_basketitem` AS basketitem,
	`tx_shop_domain_model_product` AS product

WHERE basketorder.pid = 227
	AND basketorder.status != 'prepared'
	AND basketorder.basket = basket.uid
	AND basket.uid = basketitem.basket
	AND basketitem.product = product.uid;
