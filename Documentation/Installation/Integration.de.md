# Shop Integration


## Basket

### Hinzufügen eines globalen Warenkorb-Buttons

Es ist notwendig, einen globalen Warenkorb-Button in Deine Seite einzubinden – anderenfalls wird es nicht möglich sein, Produkte in den Warenkorb zu legen!

Der *Warenkorb-Button* kann entweder als ein Content-Element oder als ein TypoScript Marker eingebunden werden.

1.	Hinzufügen des vordefinierten *Warenkorb-Button* Markers im Fluid-Template:
	```xml
	<f:cObject typoscriptObjectPath="lib.shop.basketButton" />
	```
2.	Hinzufügen des vordefinierten *Warenkorb-Buttons* im TypoScript:
	```typo3_typoscript
	page = PAGE
	page {
	  10 = USER
	  10 < lib.shop.basketButton
	  # ...
	}
	```

Alternativ kannst Du den *Warenkorb-Button* als Plugin hinzufügen.


## Allgemeines

### Schnellsuche

Um ein globales Suchfeld für Produkte einzubinden, musst Du nur den folgenden Marker im Fluid hinzufügen:

```xml
<f:cObject typoscriptObjectPath="lib.shop.showQuickSearch" />
```

Alternativ kannst Du die Schnellsuche mithilfe von TypoScript einbinden:

```typo3_typoscript
page = PAGE
page {
  20 = USER
  20 < lib.shop.showQuickSearch
  # ...
}
```

### Flash messages

Die Shop-Erweiterung verwendet einen globalen HTML-Wrapper, in dem Nachrichten angezeigt werden können.

Diesen Wrapper kannst Du mit Hilfe von TypoScript zu Deiner Seite hinzufügen:

```typo3_typoscript
page = PAGE
page {
  30 = TEXT
  30.value = <div id="shop-flash-messages" style="position: absolute; right: 0; bottom: 0"></div>
}
```


