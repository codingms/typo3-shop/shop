# Shop Integration


## Basket

### Adding a global shopping basket button

You need to put a global shopping basket button on your page otherwise it will not be possible to add products to the shopping basket!

The *shopping basket button* can be included either as a content element or as a TypoScript marker.

1.	Adding the predefined *shopping basket button* marker in the Fluid template:
	```xml
	<f:cObject typoscriptObjectPath="lib.shop.basketButton" />
	```
2.	Adding the predefined *shopping basket button* in TypoScript:
	```typo3_typoscript
	page = PAGE
	page {
	  10 = USER
	  10 < lib.shop.basketButton
	  # ...
	}
	```

Alternatively, add the *shopping basket button* as a plugin.


## General

### Quicksearch

To include a global search field for products, just add the following marker in fluid:

```xml
<f:cObject typoscriptObjectPath="lib.shop.showQuickSearch" />
```

Alternatively, add quicksearch using TypoScript:

```typo3_typoscript
page = PAGE
page {
  20 = USER
  20 < lib.shop.showQuickSearch
  # ...
}
```

### Flash messages

The Shop extension uses a global HTML wrapper for displaying messages.

Add this wrapper to your page with TypoScript:

```typo3_typoscript
page = PAGE
page {
  30 = TEXT
  30.value = <div id="shop-flash-messages" style="position: absolute; right: 0; bottom: 0"></div>
}
```


