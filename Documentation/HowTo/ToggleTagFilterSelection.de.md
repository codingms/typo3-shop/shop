# Produkttag-Filterwechsel

Wenn Du den Tag-Filter so nutzen möchtest, dass immer nur ein Filter aktiv sein kann, fügst Du das folgende JavaScript zusätzlich hinzu:

```javascript
jQuery('.products-list-filter input[type=\'checkbox\'').on('change', function() {
  var checkbox = jQuery(this);
  if(checkbox.prop('checked')) {
    jQuery.each(jQuery('.products-list-filter input[type=\'checkbox\''), function() {
      var tempCheckbox = jQuery(this);
      if(tempCheckbox.attr('name') !== checkbox.attr('name')) {
        tempCheckbox.prop('checked', false).trigger('change', false);
      }
    });
  }
});
```
