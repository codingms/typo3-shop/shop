# Frontend-Benutzergruppen bei Kauf zuweisen

Um einem Frontend-Benutzer bei einem erfolgreichen Kauf Gruppen zuzuweisen, müssen die erforderlichen Gruppen nur im Produkt ausgewählt werden.

>	#### Warnung: {.alert .alert-danger}
>
>	Bitte beachten Sie, dass diese Funktion nur für Checkout-Typen mit sofortiger Wertstellung implementiert ist (bspw. PayPal, PayPal-Plus, Klarna und Stripe)!
