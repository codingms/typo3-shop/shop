# Produkttag Varianten anpassen

Die Produkttag-Varianten können einfach mit Page-TypoScript angepasst werden. Mit dem folgenden Snippet entfernst Du die vorhandenen Einträge:

```typo3_typoscript
TCEFORM {
	tx_shop_domain_model_producttag {
		variant {
			removeItems = default, primary, secondary, success, info, warning, danger
		}
    }
}
```

Mit dem nächsten Snippet kannst Du eigene Einträge bereitstellen:

```typo3_typoscript
TCEFORM {
	tx_shop_domain_model_producttag {
		variant {
			addItems.wordpress = Wordpress
			addItems.shopware = Shopware
			addItems.magento = Magento
			addItems.contao = Contao
			addItems.drupal = Drupal
		}
    }
}
```
