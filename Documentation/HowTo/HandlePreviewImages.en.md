# Handle preview images

Put a check in the checkbox "Show in preview" (`preview`) to decide whether product images should be used in the list or in teasers, for example. This can be one or more images. The following variables can then be used in fluid:

- `{product.imagesPreviewOnly}` - Returns all images marked as "Show in preview" (`preview`)
- `{product.imagesNonPreview}` - Returns all images not marked as "Show in preview" (`preview`)

How to use:

```xml
<f:for each="{product.imagesPreviewOnly}" as="image" iteration="imageIteration">
	<f:if condition="{imageIteration.isFirst}">
		<f:then>
			<f:image src="{image.uid}" treatIdAsReference="1" class="first"
					 width="{settings.list.image.width}" height="{settings.list.image.height}"
					 alt="{product.title}" title="{product.title}" />

		</f:then>
		<f:else>
			<f:image src="{image.uid}" treatIdAsReference="1" class="non-first"
					 width="{settings.list.image.width}" height="{settings.list.image.height}"
					 alt="{product.title}" title="{product.title}" />
		</f:else>
	</f:if>
</f:for >
```

## Feature icons

You can do the same thing with feature icons:

- `{product.featureIconsPreviewOnly}` - Returns all images marked as "Show in preview" (`preview`)
- `{product.featureIconsNonPreview}` - Returns all images not marked as "Show in preview" (`preview`)
