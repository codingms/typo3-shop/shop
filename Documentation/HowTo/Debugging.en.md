# Debugging the Shop extension

>	#### Warning: {.alert .alert-danger}
>
>	Don't forget to deactivate debugging in your live system!



## Debug shopping basket and shopping basket totals

1.  Set TypoScript constant `themes.configuration.extension.shop.debug = 1`
2.  The shopping basket is continually reloaded via AJAX. When debug is activated, extra information about how totals are calculated is available from JSON.



## Debug checkout

Checkout is debugged via the `sys_log`, allowing you to monitor checkouts over a period of time and examine individual cases.

1.  Switch to the Settings module and open *Extension Configuration* by clicking on *Configure extensions*.
2.  Expand the *Shop* section and switch to the *Logging* tab.
3.  Set the *Log level* to `DEBUG` - all logging of the Shop extension is now activated.
4.  You can view the log entries in the backend module 'Log'.

>	#### Warning: {.alert .alert-danger}
>
>	If the log doesn't log any debug entries, make sure that [SYS][belogErrorReporting] has a fitting setting, for example 30711.
