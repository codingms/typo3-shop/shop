# Shop Erweiterung debuggen

>	#### Achtung: {.alert .alert-danger}
>
>	Vergiss nicht das Debugging im Live-Betrieb wieder zu deaktivieren!



## Warenkorb und Warenkorb-Berechnung debuggen

1.  Schalte via TypoScript-Konstante das `themes.configuration.extension.shop.debug = 1`
2.  Der Warenkorb wird immer via AJAX nachgeladen. Mit aktiviertem Debug hast Du an dem JSON zusätzliche Informationen über Berechnungen und deren Ergebnisse.



## Checkout debuggen

Das Debugging des Checkouts läuft über das `sys_log`, sodass Du über einen Zeitraum die Checkouts beobachten und dann gezielt untersuchen kannst.

1.  Wechsel in das Settings-Modul und öffne die *Extension Configuration* durch einen Klick auf *Configure extensions*
2.  Klappe den Abschnitt *Shop* auf und wechsel auf den Tab *Logging*.
3.  Hier stellst Du das *Log level* auf `DEBUG` - nun hast Du das Logging der Shop-Erweiterung grundsätzlich aktiviert.
4.  Du kannst die Log-Einträge in dem Backend-Modul `Log` einsehen.

>	#### Achtung: {.alert .alert-danger}
>
>	Wenn das Protokoll keine Debug-Einträge protokolliert, stelle sicher das [SYS][belogErrorReporting] eine passende Einstellung hat, zum Beispiel 30711.
