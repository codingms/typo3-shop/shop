# Create a frontend user on order products

First of all, you need to insert the `createFrontendUser` checkbox and the `password` field in the checkout form, by adding this values to the field-list.

For the `onInvoice` the TypoScript-Constants it could look like:

```
themes.configuration.extension.shop.checkout.onInvoice.fields.available = company, vatId, gender, firstname, lastname, street, houseNumber, postalCode, city, country, phone, email, password, message, deliveryAddressEnabled, deliveryAddressCompany, deliveryAddressFirstname, deliveryAddressLastname, deliveryAddressStreet, deliveryAddressHouseNumber, deliveryAddressPostalCode, deliveryAddressCity, deliveryAddressCountry, privacyProtectionConfirmed, termsConfirmed, disclaimerConfirmed, createFrontendUser
```

If a frontend user is already logged-in in the frontend, the checkbox and password field won't be shown. If you want to make a user-account mandatory, you need to insert this value in the `required` section as well. The password should be always mandatory, therefore we need it anyway.

```
themes.configuration.extension.shop.checkout.onInvoice.fields.required = firstname, lastname, street, houseNumber, postalCode, city, country, email, password, privacyProtectionConfirmed, termsConfirmed, disclaimerConfirmed, deliveryAddressFirstname, deliveryAddressLastname, deliveryAddressStreet, deliveryAddressHouseNumber, deliveryAddressPostalCode, deliveryAddressCity, deliveryAddressCountry, createFrontendUser
```


>	#### Attention: {.alert .alert-danger}
>
>	You need to enable this checkbox and password field for each payment type!


You need to add the static-include `Modules - FE-Registration & Profile (modules)` in your TypoScript-Root-Template and configure the data container, where the frontend-user must be stored in, the frontend-user-groups which must be attached and optionally the recordType of the frontend-user by using TypoScript-Constants:

```
themes.configuration.container.frontendUser = 35
themes.configuration.extension.modules.registration.frontendUserGroups = 1
themes.configuration.extension.modules.registration.recordType =
```


After a successful checkout and frontend user creation, the order will be attached to the frontend user. If there was already a user logged-in during the checkout process, only the order will be attached to the frontend-user.



>	#### Achtung: {.alert .alert-danger}
>
>	The creation of the frontend user in the PayPal checkout, for example, takes place asynchronously in the callback, and can therefore take place a little later!
