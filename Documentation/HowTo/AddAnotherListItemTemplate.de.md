# Template für List-Item hinzufügen

Zuerst musst Du einen neuen Eintrag für die Auswahlbox in der FlexForm erzeugen. Dafür einfach folgendes Seiten-TypoScript (tsconfig) hinzufügen:

```typo3_typoscript
TCEFORM.tt_content.pi_flexform.shop_products.sDEF {
	settings\.displayType {
		addItems {
			MyListItem = My list item label
		}
	}
}
```

Anschließend das passende Template in den Templates erstellen:

```text
Partials/Product/List/Item/MyListItem.html
```

>	#### Achtung: {.alert .alert-danger}
>
>	Der Item-Index in der FlexForm Konfiguration muss mit dem Template-Dateinamen übereinstimmen!
