# Product-Tag filter toggle

If you like to have a Product-Tag filter, where the selection toggles so that there's always one filter selected at one time, add the following JavaScript:

```javascript
jQuery('.products-list-filter input[type=\'checkbox\']').on('change', function() {
  var checkbox = jQuery(this);
  if(checkbox.prop('checked')) {
    jQuery.each(jQuery('.products-list-filter input[type=\'checkbox\']'), function() {
      var tempCheckbox = jQuery(this);
      if(tempCheckbox.attr('name') !== checkbox.attr('name')) {
        tempCheckbox.prop('checked', false).trigger('change', false);
      }
    });
  }
});
```
