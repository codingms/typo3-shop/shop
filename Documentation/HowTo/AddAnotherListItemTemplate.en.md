# Add template for list item

First add a new entry in the FlexForm for the select box by adding following Page-TypoScript (tsconfig):

```typo3_typoscript
TCEFORM.tt_content.pi_flexform.shop_products.sDEF {
	settings\.displayType {
		addItems {
			MyListItem = My list item label
		}
	}
}
```

Then create the appropriate template file:

```text
Partials/Product/List/Item/MyListItem.html
```

>	#### Attention: {.alert .alert-danger}
>
>	The item index in the FlexForm configuration must match the template file name!
