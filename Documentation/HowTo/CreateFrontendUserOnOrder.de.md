# Erstellung eines Frontend-Benutzer beim Checkout

Zuerst muss die `createFrontendUser` Checkbox und das `password` Feld in die Checkout-Form integriert werden, in dem diese Werte in der Field-List hinzugefügt werden.

Für den `onInvoice` könnten die TypoScript-Konstanten wie folgt aussehen:

```
themes.configuration.extension.shop.checkout.onInvoice.fields.available = company, vatId, gender, firstname, lastname, street, houseNumber, postalCode, city, country, phone, email, password, message, deliveryAddressEnabled, deliveryAddressCompany, deliveryAddressFirstname, deliveryAddressLastname, deliveryAddressStreet, deliveryAddressHouseNumber, deliveryAddressPostalCode, deliveryAddressCity, deliveryAddressCountry, privacyProtectionConfirmed, termsConfirmed, disclaimerConfirmed, createFrontendUser
```

Wenn bereits ein Frontend-Benutzer im Frontend eingeloggt ist, wird die Checkbox und das Passwortfeld nicht angezeigt. Wenn es Pflicht sein soll, das ein Benutzer während des Checkouts erstellt wird, muss dieser Wert auch in die `required` Sektion eingefügt werden. Das Passwort sollte sowieso immer Pflicht sein, daher fügen wir es auch ein.

```
themes.configuration.extension.shop.checkout.onInvoice.fields.required = firstname, lastname, street, houseNumber, postalCode, city, country, email, password, privacyProtectionConfirmed, termsConfirmed, disclaimerConfirmed, deliveryAddressFirstname, deliveryAddressLastname, deliveryAddressStreet, deliveryAddressHouseNumber, deliveryAddressPostalCode, deliveryAddressCity, deliveryAddressCountry, createFrontendUser
```


>	#### Achtung: {.alert .alert-danger}
>
>	Du musst die Checkbox und das Passwort-Feld für alle Payment-Types hinzufügen!


Du musst ebenfalls das statische TypoScript-Template `Modules - FE-Registration & Profile (modules)` in deinem TypoSript-Root-Template hinzufügen und via TypoScript-Konstanten den Daten-Container definieren, in dem die Frontend-Benutzer abgelegt werden sollen, die Frontend-Benutzergruppen die zugewiesen werden sollen und optional den Record-Type des Frontend-Benutzers:

```
themes.configuration.container.frontendUser = 35
themes.configuration.extension.modules.registration.frontendUserGroups = 1
themes.configuration.extension.modules.registration.recordType =
```


Nach erfolgreichem Checkout und Frontend-Benutzererstellung wird die Bestellung dem Frontend-Benutzer zugeordnet. War während des Checkout-Prozesses bereits ein Benutzer angemeldet, wird nur die Bestellung dem Frontend-Benutzer zugeordnet.



>	#### Achtung: {.alert .alert-danger}
>
>	Die Erstellun der Frontend-Benutzers findet im PayPal-Checkout bspw. asynchron im Callback statt, und kann daher zeitlich ein wenig später stattfinden!
