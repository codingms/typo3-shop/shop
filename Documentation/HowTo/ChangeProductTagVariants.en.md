# Customize product tags

Product tags can easily be customized using Page TypoScript. The following snippet removes existing entries:

```typo3_typoscript
TCEFORM {
	tx_shop_domain_model_producttag {
		variant {
			removeItems = default, primary, secondary, success, info, warning, danger
		}
    }
}
```

The following snippet shows how you can set your own entries:

```typo3_typoscript
TCEFORM {
	tx_shop_domain_model_producttag {
		variant {
			addItems.wordpress = Wordpress
			addItems.shopware = Shopware
			addItems.magento = Magento
			addItems.contao = Contao
			addItems.drupal = Drupal
		}
    }
}
```
