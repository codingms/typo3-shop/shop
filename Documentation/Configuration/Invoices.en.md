# Automatic invoice generation

*   Products are given a consecutive number for each container
*   The new invoice number is generated via a service. Since the service is implemented via dependency injection it can be replaced and invoice number generation can be modified.
*   The invoice layout can be created using Fluid (EXT:fluid_fpdf) and configured for each page/branch using TypoScript


## Displaying VAT/sales tax

| Destination Country | B2B (Business)        | B2C (Private)         |
|:--------------------|:----------------------|:----------------------|
| Domestic            | Display VAT           | Display VAT           |
| EU                  | No VAT to display     | Display VAT           |
| non-EU              | No VAT to display     | No VAT to display     |


##  Notes on VAT/sales tax

| Destination Country | B2B (Business)                                                                          | B2C (Private)                                 |
|:--------------------|:----------------------------------------------------------------------------------------|:----------------------------------------------|
| Domestic            | -                                                                                       | -                                             |
| EU                  | Tax-exempt intra-community delivery (§4 No. 1b UstG). The VAT ID must be entered here!  | -                                             |
| non-EU              | Tax-exempt export delivery (§4 No. 1a UstG).                                            | Tax-exempt export delivery (§4 No. 1a UstG).  |
