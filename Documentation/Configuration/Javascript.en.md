# JavaScript

## JavaScript-Events

The shop extension provides the following JavaScript events for configuring behavior.

*   **shop.after_list_lazy_load_items** Event which is triggered when items are loaded dynamically.

Beispiel für die Verwendung:

```javascript
jQuery(function() {
  jQuery(window).on('shop.after_list_lazy_load_items', function() {
    // code to be executed
  });
});
```
