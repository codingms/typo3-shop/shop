# Sitemap.xml configuration for products

The following configuration enables you to generate a Sitemap.xml for your products:

```typo3_typoscript
plugin.tx_seo {
	config {
		xmlSitemap {
			sitemaps {
				shop {
					provider = CodingMs\Shop\XmlSitemap\RecordsXmlSitemapDataProvider
					config {
						table = tx_shop_domain_model_product
						sortField = sorting
						lastModifiedField = tstamp
						recursive = 1
						pid = 2
						url {
							pageId = 6
							fieldToParameterMap {
								uid = tx_shop_products[product]
							}
							additionalGetParameters {
								tx_shop_products.controller = Product
								tx_shop_products.action = show
							}
							useCacheHash = 1
						}
					}
				}
			}
		}
	}
}
```

It's often the case that your products are displayed in different places on a website. In order to avoid *duplicate content* on your website or in your sitemap.xml, you have the option to enter a canonical URL in each product. This URL will be used in the canonical tag of your HTML header and the sitemap.xml. This field currently only supports links in typolink format - for example `t3: // page? Uid = 54`.
