# List filters

The available filters are of two types: regular filters (a form element that passes its value to the backend) and filters that can be used with AJAX/IsoTope.

| **Filter**     | **Available** | **Description**                                                                                                                                                                                                                             |
|:---------------|:--------------|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Word           |               | Input field for a search term.                                                                                                                                                                                                              |
| Category       |               | Category selection field. Only one selection is possible.                                                                                                                                                                                   |
| TagSingle      |               | Tag selection box. Only one selection is possible.                                                                                                                                                                                          |
| TagMultiple    | Isotope       | Tag check box. Each tag is represented by its own checkbox. Multiple choices are possible. If more than one tag is selected, entries with all selected tags are searched for. This means that the tags are used in a logical AND operation. |
| TagCategorized | Isotope only  | Tag checkboxes, grouped by tag categories. Similar to *TagMultiple* but the tags are grouped according to the assigned tag category.                                                                                                        |
| Submit         | Ordinary only | Submit button for sending the filter settings.                                                                                                                                                                                              |

>	#### Note: {.alert .alert-warning}
>
>	Use only one tag filter at a time, otherwise filtering may fail.

