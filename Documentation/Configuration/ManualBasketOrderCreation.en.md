# Manual creation of orders in the shop backend module

Create orders manually in the shop backend module under *Manage basket orders* > *Create a new basket order*. The form for entering customer data can can be configured by overwriting the following TypoScript variables:

```typo3_typoscript
plugin.tx_shop.settings.basketOrder.orderOptions.manual.fields.available = company, firstname, lastname, phone, email, message, privacyProtectionConfirmed, termsConfirmed, deliveryAddressEnabled, deliveryAddressCompany, deliveryAddressFirstname, deliveryAddressLastname
```
