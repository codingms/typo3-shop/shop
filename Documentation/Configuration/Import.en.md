# Import

The webshop can easily import its data via an interface such as BMEcat or OPAL. There are a variety of options for easy automation here, so that you save a lot of effort and time - feel free to contact us to optimize your online shop!

>	#### Attention: {.alert .alert-danger}
>
>	In order to be able to use the import function, you must have installed the extension `shop_import`!



## OPAL

With the extension `shop_import` you can import data from OPAL (OPAL - ERP for the watch and jewelery trade), see https://be-software.de/opalsoftware/ for further information.

### configure import

Proceed as follows to configure the import:

* Create a file folder in which the OPAL import files are stored, e.g. B. `fileadmin/opal_import`. It is recommended to prevent access to this folder from outside, e.g. with a `.htaccess` file:

```
order deny, allow
deny from all
```

* Create a folder in which the imported images are to be saved, e.g. `fileadmin/product_images`
* Create a new page, select `Shop Import` as the page type.
* A new tab is displayed: `Import configuration`. Switch to this tab.
	* Store the folders you have already created under "Import folder" and "Pictures folder".
	* Select "Products" in the "Contains Plugin" box.
	* Enter the tax rate to be used. The price in the CSV date ("Final price" column) is used as the gross price, the net price is calculated using the VAT rate stored here during the import.

### Execute the import

You can now start the import with the following console command:

``
vendor/bin/typo3cms shop_import:import
``

In order to have the import executed automatically, create a task for the TYPO3 planner and select `Execute console commands` and then the schedulable command` shop_import:import`.

Products are then created in the import folder.

### Upload import files

The import files delivered by OPAL must be uploaded to the created import folder via FTP. .csv and .jpg files are processed.

### Information about the import

* If a .csv file contains the addition "PlusDeschreibung" in the file name (e.g. `ArtikelPlusDeschreibung.csv`), then a full import is executed and new products are created. If the file does not contain this addition (e.g. `Article.csv`), only the available quantities are updated.
* The comparison is based on the article number.
* Products that are not listed in the import file but are still created as a product in TYPO3 are deactivated (set to "hidden"). If they are listed again in a later import, they will be reactivated.

### Adjust the mapping
The fields from the .csv file are transferred to fields in the products ("mapping"). This is done in the method `mapCsvRowToProduct` in` Classes/Importer/OpalImporter.php`.

There is an `event` that allows you to use your own mapping. To do this, an EventListener must be registered for the `afterMappingCsvRowToProductEvent` event.

You can find an example in the extension `shop_import` itself, have a look at the following files:


```
Configuration/Services.yaml
Classes/Importer/Event/afterMappingCsvRowToProductEvent.php
Classes/EventListener/ModifyMapping.php
```
