# UPS Delivery

As of EXT: shop_pro version 2.5.0, creation of UPS shipping labels can be integrated by using an API into the shop backend.
You will need to have the extension with key `ups_api` installed. After installation, a `BasketOrder` record will appear with an additional `Shipping` tab.


## Configuration
This chapter is about configuration options for EXT: shop that allow easy integration of EXT: ups_api. See documentation on EXT: ups_api for more information on configuration of that extension.

### StoragePid
Specify where UPS shipping records are stored by overwriting the following constant:

```typo3_typoscript
themes.configuration.extension.shop.ups.pid =
```

### Mapping
In order to simplify the process of creating labels, values stored in the `BasketOrder` record
(e.g. name, email, etc.) can be used for the shipping address or parcel configuration. A mapping will need to be created.

Example:
```typo3_typoscript
plugin.tx_shop.settings.ups.mapping {
	addressLine {
		default = params.fields.street.value, params.fields.houseNumber.value
		deliveryAddressEnabled = params.fields.deliveryAddressStreet.value, params.fields.deliveryAddressHouseNumber.value
		format = %s %s
	}
	postalCode {
		default = params.fields.postalCode.value
		deliveryAddressEnabled = params.fields.deliveryAddressPostalCode.value
		format = %s
	}
	city {
		default = params.fields.city.value
		deliveryAddressEnabled = params.fields.deliveryAddressCity.value
		format = %s
	}
	countryCode {
		default = params.fields.country.value
		deliveryAddressEnabled = params.fields.deliveryAddressCountry.value
		format = %s
	}
	companyName {
		default = params.fields.company.value
		deliveryAddressEnabled = params.fields.deliveryAddressCompany.value
		format = %s
	}
	attentionName {
		default = params.fields.firstname.value, params.fields.lastname.value
		deliveryAddressEnabled = params.fields.deliveryAddressFirstname.value, params.fields.deliveryAddressLastname.value
		format = %s %s
	}
	emailAddress {
		default = params.fields.email.value
		format = %s
	}
	phone_number {
		default = params.fields.phone.value
		format = %s
	}
	referenceNumber {
		default = invoice_number
		format = %s
	}
}
```

Info:
The address `addressLine` is made up of the road and house number that the shop user entered in the `street` and` houseNumber` fields on the order form (or `deliveryAddressStreet` and` deliveryAddressHouseNumber`
if they provided a different delivery address). These are formatted with the `% s% s` string.
You can find out more about the field configuration in the "Alternative delivery address" chapter.

### Defaults
Default values for parcel shipping can be defined:

```typo3_typoscript
plugin.tx_shop.settings.ups.defaults {
	service = 11
	packagingType = 02
	weightUnit = KGS
	packageDimensionsUnit = CM
	referenceNumberCode = IK
	weight = 1
	height = 30
	width = 30
	length = 30
	description = EXT:shop package
}
```
