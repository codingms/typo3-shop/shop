# List filter

Bei den verfügbaren Filtern unterscheiden wir zwischen Filtern, die auf herkömmliche Weise verwendet werden können (ein Formularelement, das seinen Wert
an das Backend sendet) und Filter, die mit AJAX/IsoTope verwendet werden können.

| **Filter**     | **Available** | **Description**                                                                                                                                                                                                                                                                                         |
|:---------------|:--------------|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Word           |               | Eingabefeld für einen Suchbegriff.                                                                                                                                                                                                                                                                      |
| Category       |               | Kategorie-Auswahlfeld. Es ist nur eine einzige Auswahl möglich.                                                                                                                                                                                                                                         |
| TagSingle      |               | Tag-Auswahlfeld. Es ist nur eine einzige Auswahl möglich.                                                                                                                                                                                                                                               |
| TagMultiple    | Isotope       | Tag-Kontrollkästchen. Jedes Tag wird durch eine eigene Checkbox dargestellt. Mehrfachauswahl ist möglich. Wenn mehr als ein Tag ausgewählt ist, werden Einträge gesucht, denen alle ausgewählten Tags zugeordnet sind. Das bedeutet, dass die Tags in einer logischen UND-Verknüpfung verwendet werden. |
| TagCategorized | Isotope only  | Tag-Checkboxes, gruppiert nach den Tag-Kategorien. Ähnlich wie *TagMultiple*, aber die Tags werden nach der zugewiesenen Tag-Kategorie gruppiert.                                                                                                                                                       |
| Submit         | Ordinary only | Submit-Button zum Senden der Filtereinstellungen.                                                                                                                                                                                                                                                       |

>	#### Anmerkung: {.alert .alert-warning}
>
>	Achte darauf, nur einen Tag-Filter auf einmal zu verwenden, da sonst die Filterung fehlschlagen kann.

