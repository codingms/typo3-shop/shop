# Rabatte

Rabatte und auch andere Kosten die abgezogen sollen, können über die *discounts* konfiguriert werden. Diese Datensätze müssen im Produkt-Container angelegt werden und werden auf dem Preis (netto) angewendet.

*   **Label:** Dies ist das Label für dieen Rabatt. Dieses Label wird auch im Warenkorb, in der E-Mail und in der Rechnung verwendet.
*   **Type:** Mit dem Typ kann gesteuert werden, ob der Wert als fixer Wert (*Fixed price*) oder prozentual (*In percent*) verwendet werden soll.
*   **Only on order option:** Hier kann angegeben werden, bei welchen Bestelloptionen dieser Rabatt verwendet werden sollen.
    *   **all:** Wird bei jeder Bestellung verwendet.
    *   **on product:** Dieser Rabatt kann in einem Produkt ausgewählt werden. Bspw. wenn ein Produkt veraltet ist, kann hier ein zusätzlicher Rabatt ausgewählt werden.
    *   **request:** Betrifft alle Checkouts vom Typ *Anfrage*
    *   **Pre-payment:** Betrifft alle Checkouts vom Typ *Vorkasse*
    *   **On-invoice:** Betrifft alle Checkouts vom Typ *Rechnungskauf*
    *   **PayPal:** Betrifft alle Checkouts vom Typ *PayPal*
    *   **PayPal-Plus:** Betrifft alle Checkouts vom Typ *PayPal-Plus*
    *   **Klarna:** Betrifft alle Checkouts vom Typ *Klarna*
    *   **Stripe:** Betrifft alle Checkouts vom Typ *Stripe*
*   **Fixed value:** Dieses Feld ist nur sichtbar, wenn der Typ *Fixed price* ausgewählt ist. Dies ist der feste Wert der verwendet werden soll.
*   **Value in percent:** Dieses Feld ist nur sichtbar, wenn der Typ *In percent* ausgewählt ist. Dies ist der prozentuale Wert der verwendet werden soll.

Aktuell ist nur ein Rabatt je Produkt möglich - der erste zutreffende Rabatt gewinnt.

Die Reihenfolge für die Ermittlung ist:
1. Ist im Produkt ein Rabatt ausgewählt
2. Ist ein Rabatt für die Bestellart vorhanden
3. ist ein Rabatt für alle Bestellarten vorhanden



## Beispiele

Bei Vorkasse 2% Skonto geben:

```
label = Skonto:
order_option = prePayment
type = percent
subtract = 1
value_fixed = 2,00
```

Sonderrabatt auf ein Produkt:

```
label = Sonderrabatt:
order_option = product
type = fixed
subtract = 1
value = 2,50
```
