# Working with Product Variants

Products in the shop can be displayed with product variants. This could, for example, be a T-shirt that comes in various colors and sizes.

## Defining a Product Variant

First, a product type must be configured, which will later define the product variant. The product type could be _T-shirt_, _Notebook_, _Beverages_, and much more. Within each product type, the characteristics of the product can be defined. For example, a T-shirt, in addition to the standard fields, has _size_ and _color_, while a beverage might have fields like _alcoholic_, _volume_, etc.

In the product type, it can also be defined whether a field is inherited by the product variants (i.e., always the same) or varies by variant. Additionally, the product type can specify which variant filter should be used in the detail view.
