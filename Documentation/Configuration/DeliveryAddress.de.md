# Abweichende Lieferadresse

Seit der EXT:shop Version 2.2.0 gibt es eine Möglichkeit im Checkout-Formular eine optionale abweichende Lieferadresse anzugeben.

## Konfiguration

Um diese Funktion zu aktivieren, müssen folgende TypoScript Variablen überschrieben werden:

```typo3_typoscript
plugin.tx_shop.settings.basketOrder.onInvoice.fields {
    available = deliveryAddressEnabled, deliveryAddressCompany, deliveryAddressFirstname, deliveryAddressLastname, deliveryAddressStreet, deliveryAddressPostalCode, deliveryAddressCity
    required = deliveryAddressCompany, deliveryAddressFirstname, deliveryAddressLastname, deliveryAddressStreet, deliveryAddressPostalCode, deliveryAddressCity
}
```

Dabei ist das vordefinierte Feld `deliveryAddressEnabled` eine Checkbox. Falls diese Checkbox beim Absenden des Formulars gesetzt ist, werden die in der `plugin.tx_shop.settings.basketOrder.onInvoice.fields.required` Variable aufgeführten Felder validiert. Falls das nicht der Fall ist, werden die Felder als optionale Felder behandelt.

## Vordefinierte Felder und Felddefinition

Die Adressenfelder `deliveryAddressCompany`, `deliveryAddressFirstname`, `deliveryAddressLastname`, `deliveryAddressStreet`, `deliveryAddressPostalCode` und `deliveryAddressCity` sind vordefiniert.

Es lassen sich aber auch eigene Felder definieren in dem man folgende TypoScript Variablen ergänzt:

```typo3_typoscript
plugin.tx_shop.settings.basketOrder.fieldDefinition {
    deliveryAddressHouseNumber {
    	label = Hausnummer
    	type = input
    	eval = trim
    	deliveryAddress = 1
    	placeholder = Hausnummer
    	errors {
    		isEmpty = Bitte geben Sie Ihre Hausnummer ein
    	}
    }
}
```

Das Flag `deliveryAddress = 1` sorgt dafür, dass das Feld wie eine abweichende Lieferadresse behandelt und nur bei Bedarf validiert wird.
