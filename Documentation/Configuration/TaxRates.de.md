# Steuersätze

Seit der Version 4 des Shops werden Steuern mit Steuersatz-Datensätzen definiert. Hierbei repräsentiert jeder Steuer-Prozentsatz ein Datensatz. Zudem gibt es die Möglichkeit zu jedem Steuersatz einen abweichenden Steuersatz je Lieferland anzulegen.

>	#### Achtung: {.alert .alert-danger}
>
>	Wenn du den Shop zwischen Version 3 und 4 updatest, vergesse nicht die Upgrade-Wizards auszuführen!

Wenn Sie Stripe mit Abonnements verwenden, müssen Sie einen zugehörigen Steuerdatensatz auf der Stripe-Plattform definieren. Dazu müssen Sie sich auf dashboard.stripe.com anmelden und auf _Produkte_ -> _Steuersätze_ klicken - hier müssen Sie einen Datensatz für jeden Steuerprozentsatz erstellen. Wenn Sie in verschiedenen Ländern verkaufen, kann es sinnvoll sein, für jedes Land und jeden Steuerprozentsatz unterschiedliche Datensätze zu erstellen. Wichtig ist, dass Sie die zugehörige Stripe-Steuer-ID in Ihren TYPO3-Shop-Steuersatz einfügen müssen, damit sie verbunden sind. Eine Stripe-Steuernummer beginnt normalerweise mit `txr_…`.
