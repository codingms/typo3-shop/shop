# Abonnements

Mit dem Shop können Sie ein Abonnement-System für Frontend-Nutzer mit Stripe anbieten.

## Abonnements Konfigurieren

1. Loggen Sie sich auf dashboard.stripe.com ein und wechseln Sie zum Abschnitt _Products_. Hier müssen Sie für jedes Abonnement, das Sie anbieten, ein Produkt erstellen - zum Beispiel _Basic-Plan_ und _Premium-Plan_. Diese Produkte benötigen die folgenden Daten:
	- Name: Produktnamen sind für Kunden an der Kasse, auf Quittungen, Rechnungen und im Kundenportal sichtbar.
	- Beschreibung: Produktbeschreibungen erscheinen an der Kasse und im Kundenportal.
	- Fügen Sie einen Preis ein und stellen Sie sicher, dass _wiederkehrende_ Zahlung ausgewählt ist.
    - Fügen Sie dem Produkt ein Meta-Tag mit dem Schlüssel `category` hinzu. Produkte können nach dem Wert dieses Tags im Plugin `SubscriptionTable` gefiltert werden (siehe Abschnitt "Tabelle der Abonnementprodukte anzeigen").
    - Fügen Sie dem Produkt ein Meta-Tag mit dem Schlüssel `usergroups` hinzu. Setzen Sie den Wert dieses Tags auf eine durch Kommata getrennte Liste von Frontends-Nutzergruppen. Diese Nutzergruppen werden automatisch zu den Frontendnutzern hinzugefügt, die ein Abonnement erwerben, das dieses Produkt enthält. Wenn ein Abonnement endet, werden diese Benutzergruppen automatisch aus dem Frontendnutzer-Datensatz entfernt.
    - Konfigurieren Sie Webhooks (siehe Abschnitt "Konfiguration der Stripe-Webhooks").



>	#### Attention: {.alert .alert-danger}
>
>	Bei der Verwendung von Stripe-Kunden benötigt der Frontend-Benutzer in seinem Benutzerdatensatz den kurzen Iso-Code seines Landes (z.B.: DE)!

## Konfiguration der Stripe-Webhooks

Damit die, mit dem Stripe-Produkt verknüpften Benutzergruppen, dem Benutzer automatisch zugewiesen bzw. aus dem Benutzer-Datensatz entfernt werden können, muss ein Webhook für den Empfang von Stripe-Events eingerichtet werden.

Um die Konfiguration zu vereinfachen, kann eine Kurz-URL für die Seite mit einem `SubscriptionTable`- oder `Subscriptions`-Plugin eingerichtet werden.

```yaml
  ShopSubscriptionsPlugin:
    type: Extbase
    limitToPages:
      - {page uid with the Subscriptions plugin}
    extension: ShopPro
    plugin: Subscriptions
    routes:
      - routePath: '/stripe-subscription-callback'
        _controller: 'Subscription::stripeSubscriptionCallback'
    defaultController: 'Subscription::list'
```

or

```yaml
  ShopSubscriptionsPlugin:
    type: Extbase
    limitToPages:
      - {page uid with the SubscriptionTable plugin}
    extension: ShopPro
    plugin: SubscriptionTable
    routes:
      - routePath: '/stripe-subscription-callback'
        _controller: 'Subscription::stripeSubscriptionCallback'
    defaultController: 'Subscription::list'
```

#### Konfiguration des Webhooks im Stripe Dashboard

1. Gehen Sie auf die Seite https://dashboard.stripe.com/webhooks und klicken Sie auf Endpunkt hinzufügen.
2. Geben Sie die URL der Seite ein, die im Schritt "Einrichten der Webhook-Seite" eingerichtet wurde.
3. Wählen Sie "Select Events" und wählen Sie `customer.subscription.created`, `customer.subscription.updated` und `customer.subscription.deleted`.
4. klicken Sie auf "Endpunkt hinzufügen".
5. Klicken Sie auf den neu erstellten Webhook-Eintrag und kopieren Sie das Endpoint Secret.
6. Fügen Sie das Endpoint Secret in die `subscriptionEndpointSecret` TypoScript Konstante ein.

![Stripe Webhook Creation](https://www.coding.ms/fileadmin/extensions/shop/current/Documentation/Images/StripeSubscriptionWebhookConfiguration.png)


## PSR-14 Events

Wenn
- der Webhook korrekt eingerichtet ist
- trifft eines der unterstützten Ereignisse beim Webhook ein
  - https://stripe.com/docs/api/events/types#event_types-customer.subscription.created
  - https://stripe.com/docs/api/events/types#event_types-customer.subscription.updated
  - https://stripe.com/docs/api/events/types#event_types-customer.subscription.deleted
- es existiert ein Benutzer in der Datenbank mit der entsprechenden Stripe-Kunden-ID

wird ein PSR-14-Event ausgelöst.

Fügen Sie Folgendes zu `Configuration/Services.yaml` hinzu, um auf die Events zu reagieren:

```yaml
services:
  Vendor\MyExtension\EventListener\SubscriptionCreatedEventListener:
    tags:
      - name: event.listener
        identifier: 'SubscriptionCreatedEventListener'
        event: CodingMs\ShopPro\Event\Stripe\Subscriptions\StripeSubscriptionCreatedEvent
  Vendor\MyExtension\EventListener\SubscriptionUpdatedEventListener:
    tags:
      - name: event.listener
        identifier: 'SubscriptionUpdatedEventListener'
        event: CodingMs\ShopPro\Event\Stripe\Subscriptions\StripeSubscriptionUpdatedEvent
  Vendor\MyExtension\EventListener\SubscriptionDeletedEventListener:
    tags:
      - name: event.listener
        identifier: 'SubscriptionDeletedEventListener'
        event: CodingMs\ShopPro\Event\Stripe\Subscriptions\StripeSubscriptionDeletedEvent
```

Dieses Ereignis enthält das Stripe-Subscription-Objekt und den zugehörigen Frontend-Benutzer.

Weitere Informationen zur Registrierung eines Event-Listeners finden Sie hier: https://docs.typo3.org/m/typo3/reference-coreapi/main/en-us/ApiOverview/Events/EventDispatcher/Index.html#registering-the-event-listener


## Tabelle der Abonnementprodukte anzeigen

- verwenden Sie das Plugin `SubscriptionTable`
- fügen Sie bei der Produkterstellung dem Produkt ein Meta-Tag mit dem Schlüssel `category` hinzu und setzen Sie einen benutzerdefinierten Bezeichner (z.B.: _subscription_). Dies ermöglicht Ihnen, bestimmte Produkte in Abonnement-Tabellen anzuzeigen.


## Liste der Abonnements anzeigen

- Verwenden Sie das Plugin `Subscriptions`

Der Nutzer hat die Möglichkeit, die Liste seiner aktiven und bereits abgelaufenen Abonnements einzusehen und die Abonnements sofort oder zum Ende der Laufzeit zu kündigen. Für jedes Abonnement hat der Benutzer die Möglichkeit, die Liste der Rechnungen einzusehen und diese herunterzuladen.


## Nebenbemerkung: Benutzer-Synchronisation

Stripe benötigt einen Stripe-Kundendatensatz für jeden unserer Frontend-Benutzer. Um diese Anforderung zu erfüllen, hat jeder Frontend-Benutzer ein Feld _Stripe-Customer ID_. Wenn ein Frontend-Benutzer eingeloggt ist und die _Stripe-Customer ID_ leer ist, wird ein Versuch unternommen, einen neuen Stripe-Kunden anzulegen. Die neue Stipe-Kunden-ID wird dem Benutzerdatensatz zugeordnet. Wenn die Adressdaten des Frontend-Benutzers geändert werden, werden die geänderten Daten automatisch mit Stripe synchronisiert.

Es ist sehr wichtig, den Stripe-Kunden-Datensatz auf dem neuesten Stand zu halten, wenn Tools oder Skripte von Drittanbietern zum Ändern von Benutzerdaten im Frontend verwendet werden. Falls erforderlich, kann die Benutzersynchronisation manuell aufgerufen werden. Ein Beispiel finden Sie in `EXT:shop_pro/Classes/EventListener/AfterProfileUpdateEventListener.php`

```php
public function __invoke(AfterProfileUpdatedEvent $event): void
{
    $frontendUserRepository = GeneralUtility::makeInstance(FrontendUserRepository::class);
    $frontendUser = $frontendUserRepository->findOneByUid($event->getFrontendUser()->getUid() ?? 0);
    if(!isset($frontendUser)){
        return;
    }
    $subscriptionServiceSettings = TypoScriptService::getTypoScript(
        (int)$GLOBALS['TSFE']->id
    )['plugin']['tx_shop']['settings']['basketOrder']['orderOptions']['stripe'];
    $subscriptionService = GeneralUtility::makeInstance(SubscriptionService::class, $subscriptionServiceSettings);
    $subscriptionService->updateStripeCustomer($frontendUser);
}
```

>	#### Hinweis: {.alert .alert-info}
>
>	Alle Stripe-Elemente werden in der Sprache verwendet, die der Benutzer gerade auf der Website verwendet. Diese wird aus der Site-Configuration ausgelesen.
