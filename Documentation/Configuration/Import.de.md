# Import

Der Webshop kann mit einer Schnittstelle einfach seine Daten über eine Schnittstelle wie bspw. BMEcat oder auch OPAL importieren. Hier gibt es vielfätige Möglickeiten für eine einfache Automatisierung, so dass Du viel Aufwand und Zeit sparst - sprich uns gerne an um deinen Online-Shop zu optimieren!

>	#### Achtung: {.alert .alert-danger}
>
>	Um die Import-Funktion nutzen zu können, musst Du die Erweiterung `shop_import` installiert haben!



## OPAL

Du kannst mit der Erweiterung `shop_import` Daten von OPAL (OPAL – Warenwirtschaft für den Uhren- und Schmuckhandel) importieren, siehe https://be-software.de/opalsoftware/ für weitere Informationen.

### Import konfigurieren

Gehe wie folgt vor, um den Import zu konfigurieren:

* Lege einen Datei-Ordner an, in dem die Importdateien von OPAL abgelegt werden, z. B. `fileadmin/opal_import`. Es ist empfehlenswert, den Zugriff auf diesen Ordner von außen zu unterbinden, z. B. mit einer `.htaccess`-Datei:

```
order deny,allow
deny from all
```

* Lege einen Ordner an, in dem die importierten Bilder gespeichert werden sollen, z. B. `fileadmin/product_images`
* Erstelle eine neue Seite, wähle als Seitentyp `Shop Import`.
* Es wird ein neuer Reiter angezeigt: `Import-Konfiguration`. Wechsel in diesen Reiter.
  * Hinterlege die bereits angelegten Ordner unter "Import-Ordner" und "Bilder-Ordner".
  * Wähle "Products" im Feld "Enthält Plugin".
  * Hinterlege den zu verwendenden Steuersatz. Der Preis in der CSV-Date (Spalte "Endpreis") wird als Brutto-Preis verwendet, der Netto-Preis wird anhand des hier hinterlegten MwSt.-Satzes während des Imports berechnet.

### Import ausführen

Du kannst den Import nun starten mit folgendem Konsolenbefehl:

```
vendor/bin/typo3cms shop_import:import
```

Um den Import automatisiert ausführen zu lassen, erstelle einen Task für den TYPO3 Planer und wähle `Konsolenbefehle ausführen` und dann den planbaren Befehl `shop_import:import`.

Es werden dann Produkte in dem Import-Ordner angelegt.

### Import-Dateien hochladen

Die von OPAL gelieferten Importdateien müssen in dem angelegten Import-Ornder per FTP hochgeladen werden. Es werden .csv und .jpg-Dateien verarbeitet.

### Informationen zum Import

* Wenn eine .csv-Datei den Zusatz "PlusBeschreibung" im Dateinamen enthält (z. B. `ArtikelPlusBeschreibung.csv`), dann wird ein Voll-Import ausgeführt und neue Produkte werden angelegt. Wenn die Datei diesen Zusatz nicht enthält (z. B. `Artikel.csv`), dann wird ein Abgleich der verfügbaren Mengen ausgeführt.
* Der Abgleich erfolgt anhand der Artikelnummer.
* Produkte, die nicht in der Importdatei gelistet sind, aber noch als Produkt in TYPO3 angelegt sind, werden deaktiviert (auf "versteckt" gesetzt). Wenn sie in einem einem späteren Import wieder gelistet sind, werden sie wieder aktiviert.
* The fields "Marke", "Produktgruppe" and "Warengruppe" are imported as shop categories.

### Anpassen des Mappings
Die Felder aus der .csv-Datei werden auf Felder in den Produkten übertragen ("Mapping"). Dies geschieht in der Methode `mapCsvRowToProduct` in `Classes/Importer/OpalImporter.php`.

Es gibt ein `Event`, das es erlaubt, ein eigenes Mapping zu verwenden. Dazu muss ein EventListener für das Event `afterMappingCsvRowToProductEvent` registriert werden.

Ein Beispiel findest Du in der Erweiterung `shop_import` selber, schau Dir dazu die folgenden Dateien an:

```
Configuration/Services.yaml
Classes/Importer/Event/afterMappingCsvRowToProductEvent.php
Classes/EventListener/ModifyMapping.php
```
