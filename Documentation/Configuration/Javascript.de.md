# JavaScript

## JavaScript-Events

Die Shop Erweiterung bietet folgende JavaScript-Events, mit denen Du das Verhalten manipulieren kannst.

*   **shop.after_list_lazy_load_items** Dieses Event wird getriggert, wenn Items nachgeladen werden.

Beispiel für die Verwendung:

```javascript
jQuery(function() {
  jQuery(window).on('shop.after_list_lazy_load_items', function() {
    // auszuführender Code
  });
});
```
