# UPS Versand

Ab der EXT:shop_pro Version 2.5.0 kann die Erzeugung von UPS-Versandetiketten via API/Schnittstelle in das Shop-Backend integriert werden.
Dafür muss die Extension mit dem Key `ups_api` installiert werden. Nach der Installation erscheint im Record
`BasketOrder` ein zusätzlicher Tab mit dem namen `Shipping`.


## Konfiguration
In diesem Kapitel wird die Konfiguration von EXT:shop behandelt, die eine Verwendung der Extension zusammen mit der
EXT:ups_api ermöglicht oder vereinfacht. In der Dokumentation von EXT:ups_api findest Du mehr Informationen zur
Konfiguration der Extension.

### StoragePid
Man kann festlegen, wo die UPS-shipping-records gespeichert werden, indem man die Konstante überschreibt.

```typo3_typoscript
themes.configuration.extension.shop.ups.pid =
```


### Mapping
Um den Prozess der Etikettenstellung zu vereinfachen, können in dem `BasketOrder`-Record gespeicherten Werte
(z. B. Name, E-Mail, etc.) vür die Versandadresse oder die Paketkonfiguration verwendet werden. Dafür muss eine
Wertezuordnung (mapping) erstellt werden.

Beispiel:
```typo3_typoscript
plugin.tx_shop.settings.ups.mapping {
	addressLine {
		default = params.fields.street.value, params.fields.houseNumber.value
		deliveryAddressEnabled = params.fields.deliveryAddressStreet.value, params.fields.deliveryAddressHouseNumber.value
		format = %s %s
	}
	postalCode {
		default = params.fields.postalCode.value
		deliveryAddressEnabled = params.fields.deliveryAddressPostalCode.value
		format = %s
	}
	city {
		default = params.fields.city.value
		deliveryAddressEnabled = params.fields.deliveryAddressCity.value
		format = %s
	}
	countryCode {
		default = params.fields.country.value
		deliveryAddressEnabled = params.fields.deliveryAddressCountry.value
		format = %s
	}
	companyName {
		default = params.fields.company.value
		deliveryAddressEnabled = params.fields.deliveryAddressCompany.value
		format = %s
	}
	attentionName {
		default = params.fields.firstname.value, params.fields.lastname.value
		deliveryAddressEnabled = params.fields.deliveryAddressFirstname.value, params.fields.deliveryAddressLastname.value
		format = %s %s
	}
	emailAddress {
		default = params.fields.email.value
		format = %s
	}
	phone_number {
		default = params.fields.phone.value
		format = %s
	}
	referenceNumber {
		default = invoice_number
		format = %s
	}
}
```

Erklärung:
die Adresszeile `addressLine` setzt sich aus der Straße und der Hausnummer zusammen, die der Shop-User im Bestellformular
in den Feldern `street` und `houseNumber` (oder `deliveryAddressStreet` und `deliveryAddressHouseNumber`
im Falle einer abweichenden Lieferadresse) eingegeben hat. Diese werden mit dem String `%s %s` formatiert.
Mehr zur Feldkonfiguration findest Du im Kapitel „Abweichende Lieferadresse“.

### Defaults
Es können Standardwerte für den Paketversand definiert werden:

```typo3_typoscript
plugin.tx_shop.settings.ups.defaults {
	service = 11
	packagingType = 02
	weightUnit = KGS
	packageDimensionsUnit = CM
	referenceNumberCode = IK
	weight = 1
	height = 30
	width = 30
	length = 30
	description = EXT:shop package
}
```
