# Link handler

## TYPO3 8.7

Page-TypoScript

```typo3_typoscript
// Page TSconfig for registering the linkhandler for "product" records
TCEMAIN.linkHandler.tx_shop_product {
	handler = TYPO3\CMS\Recordlist\LinkHandler\RecordLinkHandler
	label = LLL:EXT:shop/Resources/Private/Language/locallang_db.xlf:tx_shop_domain_model_product
	configuration {
		table = tx_shop_domain_model_product
	}
	scanBefore = page
}
```

Setup-TypoScript

```typo3_typoscript
config.recordLinks {
    tx_shop_product {
        typolink {
            parameter = {$themes.configuration.pages.shop.detail}
            additionalParams.data = field:uid
            additionalParams.wrap = &tx_shop_products[controller]=Product&tx_shop_products[action]=show&tx_shop_products[product]=|
            useCacheHash = 1
            ATagParams.data = parameters:allParams
            target.data = parameters:target
            title.data = parameters:title
            extTarget = _blank
            extTarget.override.data = parameters:target
        }
        forceLink = 1
    }
}
```
