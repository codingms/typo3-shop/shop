# Staffel-Preise

Staffel-Preise können wie folgt abgebildet werden:

*   Im Produkt-Datensatz an sich steht der Einzelpreis (für die hinterlegte Verpackungseinheit).
    D.h. `{product.price}` bzw. `{product.priceAsFloat}` gibt nur den Einzelpreis bei einer Menge von einem Produkt zurück.
*   Im Datensatz kann dann zusätzlich angegeben werden, wie teuer das Produkt am Menge X ist.
*   Im Basket bzw. Basket-Service wird dann der aktuelle Warenkorb berechnet. Hier wird dann auch geschaut, wie viele Einheiten von einem Produkt gekauft werden und damit dann der reduzierte Preis ermittelt,
*   Hinweis: Staffelpreise können nicht zusammen mit digitalen Produkten verwendet werden, da die Anzahl der digitalen Produkte im Warenkorb auf 1 begrenzt ist.
