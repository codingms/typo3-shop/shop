# Arbeiten mit Produkt-Varianten

Die Produkte im Shop können mit Produkt-Varianten dargestellt werden. Dies könnte bspw. ein T-Shirt sein, welches es in verschiedenen Farben und Größen gibt.

## Definition einer Produkt-Variante

Zuerst muss ein Produkt-Typ konfiguriert werden, welcher später die Produkt-Variante definiert. Der Produkt-Typ könnte somit _T-Shirt_, _Notebook_, _Getränke_ und vieles mehr sein. In jedem Produkt-Typ kann nun die Ausprägung des Produktes definiert werden, so hat ein T-Shirt neben den Standard-Feldern eine _Größe_ und eine _Farbe_, und ein Getränk Felder wie _alkoholisch_, _Füllmenge_, etc.
Dabei kann im Produkt-Typ auch definiert werden, ob ein Feld in die Produkt-Varianten vererbt wird (sprich immer gleich ist) oder je Variante unterschiedlich ist. Desweiteren kann im Produkt-Typ auch angegeben werden, welcher Varianten-Filter in der Detailansicht verwendet werden soll.
