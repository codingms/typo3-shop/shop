# Klarna Checkout konfigurieren

>	#### Achtung: {.alert .alert-danger}
>
>	Der Klarna Checkout erfordert der die Pro-Version der Shop-Erweiterung!



## Vorbereitungen

Um den Klarna Checkout nutzen zu können, müssen folgende Voraussetzungen erfüllt sein:

*   Im Checkout muss das Country/Land-Feld verwendet werden.



## Klarna API-Zugangsdaten

1.  Wechsel im Klarna-Login in den Bereich *Einstellungen* -> *Klarna Zugangsdaten*
2.  Erstelle hier neue API-Zugangsdaten
3.  Hier bekommst Du nun einen Benutzernamen (UID) und ein Passwort angezeigt
4.  Speicher sich diese Zugangsdaten und hake dann die Checkbox an, um das Modal zu schließen.


Eine Test-Umgebung (Sandbox/Playground) findest Du hier: https://auth.playground.eu.portal.klarna.com/auth/realms/merchants/protocol/openid-connect/auth?client_id=merchant-portal



## Shop-Informationen bei Klarna konfigurieren

Unter *Einstellungen* -> *Shop-Informationen*: https://playground.eu.portal.klarna.com/settings/store



## Aktionen nach erfolgreicher Bezahlung

Wenn Du nach der erfolgreichen Bezahlung weitere Aktionen durchführen möchtest, kannst Du dazu den `KlarnaPaid` Event Listener nutzen.

1.	Dazu muss ein Event Listener erstellt werden:
	```php
	<?php
	namespace YourVendor\YourExtension\EventListener;
	use CodingMs\ShopPro\Event\BasketOrder\KlarnaPaidEvent;
	class DoSomeThingEventListener
	{
		public function __invoke(KlarnaPaidEvent $event): void
		{
			/* Do something */
		}
	}
	```
2.	Dieser Event Listener muss anschließend in der `Configuration/Services.yaml` registriert werden:
	```yaml
	services:
		YourVendor\YourExtension\EventListener\DoSomeThingEventListener:
			tags:
				- name: event.listener
				  identifier: 'myListener'
				  event: CodingMs\ShopPro\Event\BasketOrder\KlarnaPaidEvent
	```



## Testing

Zuerst braucht man einen Test-Account, den man hier erstellen kann: https://playground.eu.portal.klarna.com/developer-sign-up?

Siehe: https://developers.klarna.com/documentation/testing-environment/


>	#### Achtung: {.alert .alert-danger}
>
>	Wenn Du mit Deinem Test-Account nicht die Zahlarten, die Du gerne einsetzen bzw. testen möchtest, freigeschaltet habst, kannst Du diese via Mail bei shop@klarna.de anfragen.


###  Test Credit Card

Um den Kartenzahlungsfluss aus der Sicht des Endbenutzers zu simulieren/testen, verwendest Du die folgenden Kartendetails im geladenen Klarna-Widget:

Credit card number: 4111 1111 1111 1111
CVV: 123
Exp: 12/25 (Oder ein anderes gültiges Datum in der Zukunft)


### Test bank account

Um mit einer Bank-Verbindung zu testen muss nur irgendein Text eingegeben werden, anschließend wird einem die *Demo Bank* vorgeschlagen. Diese muss ausgewählt werden und irgendeine mindestens 8 Ziffern lange Kontonummer + Pin eingegeben werden. Abschließend wählst Du ein Konto aus und gibst *12345* als TAN ein.


### Überprüfe Deine Details/Telefonnummer

Zum Testen kann die Telefonnummer *+491755555555* eingegeben werden. Anschließend wird man nach einem 6 stelligen Code gefragt. Wenn Du hier `999999` eingibst, kannst Du einen Fehlversuch simulieren. Wenn Du irgendeinen anderen 6 stelligen Code eingibst, wird die Nummer bestätigt.



### Support bei Klarna

Wenn Du Unterstützung mit Deinem Händler-Account benötigst, kannst Du entweder telefonisch (0221 669 50130) oder via Mail shop@klarna.de Kontakt aufnehmen.
