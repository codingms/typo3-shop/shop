# Configuring Klarna Checkout

>	#### Attention: {.alert .alert-danger}
>
>	Klarna Checkout requires the Pro version of the shop extension!



## Preparation

In order to use Klarna Checkout you need to do the following:

*   Make sure the Country/Land field is set in Checkout.



## Klarna API access credentials

1. On the Klarna login page, switch to *Settings* -> *Klarna access data*
2. Create your new API credentials
3. Your new user name (UID) and password will be displayed
4. Save and then tick the checkbox to close the modal.


This link will take you to the test environment (Sandbox/Playground): https://auth.playground.eu.portal.klarna.com/auth/realms/merchants/protocol/openid-connect/auth?client_id=merchant-portal



## Configuring Shop information in Klarna

Under *Settings* -> *Store information*: https://playground.eu.portal.klarna.com/settings/store



## Add additional actions after a successful payment

If you need actions to be carried out after successful payment, use the `KlarnaPaid` event listener.

1.	Create a new slot:
	```php
	<?php
	namespace YourVendor\YourExtension\EventListener;
	use CodingMs\ShopPro\Event\BasketOrder\KlarnaPaidEvent;
	class DoSomeThingEventListener
	{
		public function __invoke(KlarnaPaidEvent $event): void
		{
			/* Do something */
		}
	}
	```
2.	Then register the event listener in `Configuration/Services.yaml`:
	```yaml
	services:
		YourVendor\YourExtension\EventListener\DoSomeThingEventListener:
			tags:
				- name: event.listener
				  identifier: 'myListener'
				  event: CodingMs\ShopPro\Event\BasketOrder\KlarnaPaidEvent
	```



## Testing

You will need a test account that you can create here: https://playground.eu.portal.klarna.com/developer-sign-up?

See: https://developers.klarna.com/documentation/testing-environment/


>	#### Warning: {.alert .alert-danger}
>
> Payment methods that you would like to use or test that don't appear in your test account can be requested by sending an email to shop@klarna.de.


###  Test credit card

In order to simulate/test the card payment flow from the end user perspective use the following card details in the loaded Klarna widget:

Credit card number: 4111 1111 1111 1111
CVV: 123
Exp: 12/25 (or any valid date in the future)


### Test bank account

To test using a bank connection, start typing and the system will suggest *Demo Bank*. Select it and enter any account number + pin with minimum 8 digits. Then select an account and enter *12345* as the TAN.


### Verify your details/phone number

You can test this by entering the telephone number *+491755555555*. You will then need to enter a 6-digit code. Enter `999999` to simulate a failed attempt. Any other 6 digit code apart from `999999` will be validated.



### Support from Klarna

If you need help with your retailer account, you can contact Karna directly by phone (0221 669 50130) or email at shop@klarna.de.
