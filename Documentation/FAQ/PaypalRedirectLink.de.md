# Links für die PayPal Redirects sind nicht absolute

Vermutlich wurde bei der SiteConfiguration kein *Entry Point* mit einer Domain eingegeben – hier sollte `https://www.domain.tld/` verwendet werden.
