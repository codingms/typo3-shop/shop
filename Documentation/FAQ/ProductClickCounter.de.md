# Der Produkt-Klick-Counter zählt nicht

Vermutlich besteht in einem anderen Browser-Tab eine Backend-Session. Der Produkt-Klick-Counter zählt nicht wenn ein Benutzer im Backend eingeloggt ist.
