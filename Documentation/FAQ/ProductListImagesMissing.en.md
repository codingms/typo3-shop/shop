# Missing Images in the Product List

If images go missing in the product list after an extension update, go to the Upgrade Module, open the Upgrade Wizards, and run the `EXT:shop: Migrate all showinpreview flags in file-references to preview` wizard. After execution, all marked preview images should be displayed correctly again.
