# In der Produkt-Liste fehlen die Bilder

Wenn in der Produkt-Liste nach einem Update der Extension plötzlich die Bilder fehlen sollten, gehe in das Upgrade-Module, öffene die Upgrade-Wizards und führe den `EXT:shop: Migrate all showinpreview flags in file-references to preview` Wizard aus. Danach sollten wieder alle markierten Vorschau-Bilder angezeigt werden.
