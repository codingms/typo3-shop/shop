# Produkte können nicht in den Warenkorb gelegt werden

Vermutlich wurden die Route-Enhancer noch nicht konfiguriert. Wenn der Page-Type `shop.json: 1496773586` nicht definiert ist, können die AJAX-Requests nicht geroutet werden!

Wenn der Warenkorb-Button immer noch nicht angezeigt wird, hast Du vielleicht die statischen Templates in der falschen Reihenfolge. Die Shop static Templates müssen nach dem static Template eingebunden werden, welches die Inhaltselemente definiert.
