# Refreshing the view statistics

The shop extension collects statistics internally about how often a product was called up by visitors in the frontend. This gives you an overview of which products seem to be of much interest to your visitors.

So that these statistics are always up-to-date in your dashboard widget, a command (CLI, cron, scheduler) is supplied, which keeps the summation of the data up to date.

>	#### Attention: {.alert .alert-danger}
>
>	The view counter does not count product views if you are logged in to the backend in the same browser!
