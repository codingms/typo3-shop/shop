# Aktualisierung der View-Statistiken

Die Shop-Erweiterung führt intern Statistik darüber, wie oft ein Produkt von Besuchern im Frontend aufgerufen wurde. Dies gibt dir einen Überblick darüber welche Produkte für deine Besucher besonders interessant zu sein scheinen.

Damit diese Statistik in deinem Dashboard-Widget immer aktuell ist, wird ein Command (CLI, cron, Scheduler) mitgeliefert, welcher die Summierung der Daten aktuell hält.

>	#### Achtung: {.alert .alert-danger}
>
>	Der View-Counter zählt keine Produktaufrufe, wenn Du im gleichen Browser im Backend eingeloggt bist!
