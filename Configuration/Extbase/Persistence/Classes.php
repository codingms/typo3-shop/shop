<?php

declare(strict_types=1);

return [
    \CodingMs\Shop\Domain\Model\FrontendUser::class => [
        'tableName' => 'fe_users',
        'properties' => [
            'stripeId' => [
                'fieldName' => 'tx_shop_stripe_id'
            ],
            'orderGroup' => [
                'fieldName' => 'tx_shop_order_group'
            ],
            'orderGroupOrdersVisible' => [
                'fieldName' => 'tx_shop_order_group_orders_visible'
            ],
        ],
    ],
    \CodingMs\Modules\Domain\Model\FrontendUser::class => [
        'subclasses' => [
            'ShopFrontendUser' => \CodingMs\Shop\Domain\Model\FrontendUser::class,
        ],
    ],
    \CodingMs\Shop\Domain\Model\FrontendUserGroup::class => [
        'tableName' => 'fe_groups',
    ],
    \CodingMs\Modules\Domain\Model\FrontendUserGroup::class => [
        'subclasses' => [
            'ShopFrontendUserGroup' => \CodingMs\Shop\Domain\Model\FrontendUserGroup::class,
        ],
    ],
    \CodingMs\Shop\Domain\Model\FileReference::class => [
        'tableName' => 'sys_file_reference',
    ],
    \CodingMs\Shop\Domain\Model\Product::class => [
        'tableName' => 'tx_shop_domain_model_product',
        'properties' => [
            'creationDate' => [
                'fieldName' => 'crdate'
            ],
            'modificationDate' => [
                'fieldName' => 'tstamp'
            ],
            'creationUser' => [
                'fieldName' => 'cruser_id'
            ],
        ],
    ],
    \CodingMs\Shop\Domain\Model\ProductTag::class => [
        'tableName' => 'tx_shop_domain_model_producttag',
        'properties' => [
            'creationDate' => [
                'fieldName' => 'crdate'
            ],
            'modificationDate' => [
                'fieldName' => 'tstamp'
            ],
            'creationUser' => [
                'fieldName' => 'cruser_id'
            ],
        ],
    ],
    \CodingMs\Shop\Domain\Model\ProductTagCategory::class => [
        'tableName' => 'tx_shop_domain_model_producttagcategory',
        'properties' => [
            'creationDate' => [
                'fieldName' => 'crdate'
            ],
            'modificationDate' => [
                'fieldName' => 'tstamp'
            ],
            'creationUser' => [
                'fieldName' => 'cruser_id'
            ],
        ],
    ],
    \CodingMs\Shop\Domain\Model\ProductCategory::class => [
        'tableName' => 'tx_shop_domain_model_productcategory',
        'properties' => [
            'creationDate' => [
                'fieldName' => 'crdate'
            ],
            'modificationDate' => [
                'fieldName' => 'tstamp'
            ],
            'creationUser' => [
                'fieldName' => 'cruser_id'
            ],
        ],
    ],
    \CodingMs\Shop\Domain\Model\ProductAttributeCategory::class => [
        'tableName' => 'tx_shop_domain_model_productattributecategory',
        'properties' => [
            'creationDate' => [
                'fieldName' => 'crdate'
            ],
            'modificationDate' => [
                'fieldName' => 'tstamp'
            ],
            'creationUser' => [
                'fieldName' => 'cruser_id'
            ],
        ],
    ],
    \CodingMs\Shop\Domain\Model\BasketOrder::class => [
        'tableName' => 'tx_shop_domain_model_basketorder',
        'properties' => [
            'creationDate' => [
                'fieldName' => 'crdate'
            ],
            'modificationDate' => [
                'fieldName' => 'tstamp'
            ],
            'creationUser' => [
                'fieldName' => 'cruser_id'
            ],
        ],
    ],
];
