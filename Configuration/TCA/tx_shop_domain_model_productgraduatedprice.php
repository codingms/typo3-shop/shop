<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}

$extKey = 'shop';
$table = 'tx_shop_domain_model_productgraduatedprice';
$lll = 'LLL:EXT:' . $extKey . '/Resources/Private/Language/locallang_db.xlf:' . $table;

$return = [
    'ctrl' => [
        'title' => $lll,
        'label' => 'quantity',
        'label_alt' => 'price',
        'label_userFunc' => CodingMs\Shop\Tca\LabelService::class . '->getProductGraduatedPriceLabel',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'sortby' => 'sorting',
        'default_sortby' => 'sorting',
        'versioningWS' => true,
        'hideTable' => true,
        'origUid' => 't3_origuid',
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
        ],
        'searchFields' => '',
        'typeicon_classes' => [
            'default' => 'mimetypes-x-content-shop-productgraduatedprice'
        ],
    ],
    'types' => [
        '1' => ['showitem' => '--palette--;;quantity_price'],
    ],
    'palettes' => [
        'quantity_price' => ['showitem' => 'quantity, price'],
    ],
    'columns' => [
        'sys_language_uid'  => \CodingMs\AdditionalTca\Tca\Configuration::full('sys_language_uid'),
        'l10n_parent' => \CodingMs\AdditionalTca\Tca\Configuration::full('l10n_parent', $table),
        'l10n_diffsource' => \CodingMs\AdditionalTca\Tca\Configuration::full('l10n_diffsource'),
        't3ver_label' => \CodingMs\AdditionalTca\Tca\Configuration::full('t3ver_label'),
        'hidden' => \CodingMs\AdditionalTca\Tca\Configuration::full('hidden'),
        'quantity' => [
            'exclude' => 0,
            'label' => $lll . '.quantity',
            'description' => $lll . '.quantity_description',
            'config' => \CodingMs\Shop\Tca\Configuration::get('int', true),
        ],
        'price' => [
            'exclude' => 0,
            'label' => $lll . '.price',
            'description' => $lll . '.price_description',
            'config' => \CodingMs\Shop\Tca\Configuration::get('currency', true),
        ],
        'product' => [
            'config' => [
                'type' => 'passthrough'
            ],
        ],
    ],
];

if ((int)\TYPO3\CMS\Core\Utility\VersionNumberUtility::getCurrentTypo3Version() < 12) {
    $return['ctrl']['cruser_id'] = 'cruser_id';
}
return $return;
