<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}
//
// Page tree icon
if ((int)\TYPO3\CMS\Core\Utility\VersionNumberUtility::getCurrentTypo3Version() < 12) {
    $GLOBALS['TCA']['pages']['columns']['module']['config']['items'][] = [
        0 => 'LLL:EXT:shop/Resources/Private/Language/locallang_db.xlf:tx_shop_label.contains_products',
        1 => 'products',
        2 => 'apps-pagetree-folder-contains-products'
    ];
} else {
    $GLOBALS['TCA']['pages']['columns']['module']['config']['items'][] = [
        'label' => 'LLL:EXT:shop/Resources/Private/Language/locallang_db.xlf:tx_shop_label.contains_products',
        'value' => 'products',
        'icon' => 'apps-pagetree-folder-contains-products'
    ];
}
$GLOBALS['TCA']['pages']['ctrl']['typeicon_classes']['contains-products'] = 'apps-pagetree-folder-contains-products';
