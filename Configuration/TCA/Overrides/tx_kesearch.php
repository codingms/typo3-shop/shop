<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}
//
// KeSearch indexer
if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('ke_search')) {
    $GLOBALS['TCA']['tx_kesearch_indexerconfig']['columns']['sysfolder']['displayCond'] .= ',shop';
}
