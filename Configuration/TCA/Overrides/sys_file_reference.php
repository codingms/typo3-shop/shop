<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}

/**
 * Add extra field preview and some special news controls to sys_file_reference record
 */
$newSysFileReferenceColumns = [
    'preview' => [
        'exclude' => true,
        'label' => 'LLL:EXT:shop/Resources/Private/Language/locallang_db.xlf:sys_file_reference.preview',
        'config' => [
            'type' => 'check',
            'default' => 0
        ]
    ],
    'attach_to_email' => [
        'exclude' => true,
        'label' => 'LLL:EXT:shop/Resources/Private/Language/locallang_db.xlf:sys_file_reference.attach_to_email',
        'config' => [
            'type' => 'check',
            'default' => 0
        ]
    ],
    'filename' => [
        'exclude' => true,
        'label' => 'LLL:EXT:shop/Resources/Private/Language/locallang_db.xlf:sys_file_reference.filename',
        'description' => 'LLL:EXT:shop/Resources/Private/Language/locallang_db.xlf:sys_file_reference.filename_description',
        'config' => [
            'type' => 'input',
            'eval' => 'trim',
            'default' => '',
        ]
    ],
    'limited_time_access' => [
        'exclude' => true,
        'label' => 'LLL:EXT:shop/Resources/Private/Language/locallang_db.xlf:sys_file_reference.limited_time_access',
        'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false,'LLL:EXT:shop/Resources/Private/Language/locallang_db.xlf:sys_file_reference.limited_time_access_checkbox_label')
    ],
];

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('sys_file_reference', $newSysFileReferenceColumns);

// add special news palette
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette('sys_file_reference', 'shopProductPalette', 'preview');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette('sys_file_reference', 'shopProductFilePalette', 'filename, attach_to_email, limited_time_access');

// Palette with only title and alternative text
$GLOBALS['TCA']['sys_file_reference']['palettes']['titleAlternativePalette']['showitem'] = 'title,alternative';
