<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}

$extKey = 'shop';
$table = 'tx_shop_domain_model_productshippingcost';
$lll = 'LLL:EXT:' . $extKey . '/Resources/Private/Language/locallang_db.xlf:' . $table;

$return = [
    'ctrl' => [
        'title' => $lll,
        'label' => 'label',
        'label_userFunc' => CodingMs\Shop\Tca\LabelService::class . '->getProductShippingCostLabel',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'default_sortby' => 'weight',
        'versioningWS' => true,
        'origUid' => 't3_origuid',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
        ],
        'searchFields' => '',
        'typeicon_classes' => [
            'default' => 'mimetypes-x-content-shop-productshippingcost'
        ],
        'accessUtility' => \CodingMs\ShopPro\Utility\AccessUtility::class
    ],
    'types' => [
        '1' => ['showitem' => '
            label,
            weight,
            --palette--;;price_bulky,
            overlay
        '],
    ],
    'palettes' => [
        'price_bulky' => [
            'showitem' => 'label_public,label_bulky_public,--linebreak--,price,bulky',
        ],
    ],
    'columns' => [
        'hidden' => \CodingMs\AdditionalTca\Tca\Configuration::full('hidden'),
        'label' => [
            'exclude' => 0,
            'label' => $lll . '.label',
            'description' => $lll . '.label_description',
            'config' => \CodingMs\Shop\Tca\Configuration::get('string', true),
        ],
        'label_public' => [
            'exclude' => 0,
            'label' => $lll . '.label_public',
            'description' => $lll . '.label_public_description',
            'config' => \CodingMs\Shop\Tca\Configuration::get('string', true),
        ],
        'label_bulky_public' => [
            'exclude' => 0,
            'label' => $lll . '.label_bulky_public',
            'description' => $lll . '.label_bulky_public_description',
            'config' => \CodingMs\Shop\Tca\Configuration::get('string', true),
        ],
        'weight' => [
            'exclude' => 0,
            'label' => $lll . '.weight',
            'description' => $lll . '.weight_description',
            'config' => \CodingMs\Shop\Tca\Configuration::get('weight', true),
        ],
        'bulky' => [
            'exclude' => 0,
            'label' => $lll . '.bulky',
            'description' => $lll . '.bulky_description',
            'config' => \CodingMs\Shop\Tca\Configuration::get('currency', true),
        ],
        'price' => [
            'exclude' => 0,
            'label' => $lll . '.price',
            'description' => $lll . '.price_description',
            'config' => \CodingMs\Shop\Tca\Configuration::get('currency', true),
        ],
        'overlay' => [
            'exclude' => 0,
            'label' => $lll . '.overlay',
            'description' => $lll . '.overlay_description',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_shop_domain_model_productshippingcostcountryoverlay',
                'foreign_field' => 'default_shipping_cost',
            ],
        ],
    ],
];

if ((int)\TYPO3\CMS\Core\Utility\VersionNumberUtility::getCurrentTypo3Version() < 12) {
    $return['ctrl']['cruser_id'] = 'cruser_id';
}
return $return;
