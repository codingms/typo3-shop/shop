<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}

$extKey = 'shop';
$table = 'tx_shop_domain_model_producttype';
$lll = 'LLL:EXT:' . $extKey . '/Resources/Private/Language/locallang_db.xlf:' . $table;

$return = [
    'ctrl' => [
        'title' => $lll,
        'label' => 'title',
        'label_alt_force' => true,
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'sortby' => 'sorting',
        'versioningWS' => 2,
        'versioning_followPages' => true,
        'origUid' => 't3_origuid',
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
        ],
        'searchFields' => 'title,import_id',
        'typeicon_classes' => ['default' => 'mimetypes-x-content-shop-producttype'],
        'accessUtility' => \CodingMs\ShopPro\Utility\AccessUtility::class
    ],
    'types' => [
        '0' => [
            'showitem' => '
            --div--;' . \CodingMs\AdditionalTca\Tca\Configuration::label('tab_general') . ',
                --palette--;;notice_title_subtitle_filter_import_id_description,
                --palette--;' . $lll . '.slug;slug_enabled_slug_enabled_variant,
                --palette--;' . $lll . '.title;title_enabled_title_enabled_variant,
                --palette--;' . $lll . '.product_no;product_no_enabled_product_no_enabled_variant,
                --palette--;' . $lll . '.global_trade_item_no;global_trade_item_no_enabled_global_trade_item_no_enabled_variant,
                --palette--;' . $lll . '.product_type;product_type_enabled_product_type_enabled_variant,
                --palette--;' . $lll . '.subtitle;subtitle_enabled_subtitle_enabled_variant,
                --palette--;' . $lll . '.highlight;highlight_enabled_highlight_enabled_variant,
                --palette--;' . $lll . '.first_in_list;first_in_list_enabled_first_in_list_enabled_variant,
                --palette--;' . $lll . '.teaser;teaser_enabled_teaser_enabled_variant,
                --palette--;' . $lll . '.digital_product;digital_product_enabled_digital_product_enabled_variant,
                --palette--;' . $lll . '.description;description_enabled_description_enabled_variant,
                --palette--;' . $lll . '.pdf_description;pdf_description_enabled_pdf_description_enabled_variant,
                --palette--;' . $lll . '.description_file;description_file_enabled_description_file_enabled_variant,
                --palette--;' . $lll . '.www;www_enabled_www_enabled_variant,
                --palette--;' . $lll . '.weight;weight_enabled_weight_enabled_variant,
                --palette--;' . $lll . '.bulky;bulky_enabled_bulky_enabled_variant,
                --palette--;' . $lll . '.color;color_enabled_color_enabled_variant,
                --palette--;' . $lll . '.size;size_enabled_size_enabled_variant,
            --div--;' . \CodingMs\AdditionalTca\Tca\Configuration::label('tab_seo') . ',
                --palette--;' . $lll . '.canonical_link;canonical_link_enabled_canonical_link_enabled_variant,
                --palette--;' . $lll . '.html_title;html_title_enabled_html_title_enabled_variant,
                --palette--;' . $lll . '.meta_abstract;meta_abstract_enabled_meta_abstract_enabled_variant,
                --palette--;' . $lll . '.meta_description;meta_description_enabled_meta_description_enabled_variant,
                --palette--;' . $lll . '.meta_keywords;meta_keywords_enabled_meta_keywords_enabled_variant,
            --div--;' . \CodingMs\AdditionalTca\Tca\Configuration::label('tab_prices') . ',
                --palette--;' . $lll . '.price;price_enabled_price_enabled_variant,
                --palette--;' . $lll . '.price_type;price_type_enabled_price_type_enabled_variant,
                --palette--;' . $lll . '.tax;tax_enabled_tax_enabled_variant,
                --palette--;' . $lll . '.offer;offer_enabled_offer_enabled_variant,
                --palette--;' . $lll . '.offer_rebate_value;offer_rebate_value_offer_rebate_value_enabled_variant,
                --palette--;' . $lll . '.stock;stock_enabled_stock_enabled_variant,
                --palette--;' . $lll . '.unit;unit_enabled_unit_enabled_variant,
                --palette--;' . $lll . '.discount;discount_enabled_discount_enabled_variant,
                --palette--;' . $lll . '.graduated_prices;graduated_prices_enabled_graduated_prices_enabled_variant,
            --div--;' . \CodingMs\AdditionalTca\Tca\Configuration::label('tab_relations') . ',
                --palette--;' . $lll . '.categories;categories_enabled_categories_enabled_variant,
                --palette--;' . $lll . '.tags;tags_enabled_tags_enabled_variant,
                --palette--;' . $lll . '.related_products;related_products_enabled_related_products_enabled_variant,
                --palette--;' . $lll . '.accessories;accessories_enabled_accessories_enabled_variant,
                --palette--;' . $lll . '.accessories_required;accessories_required_enabled_accessories_required_enabled_variant,
                --palette--;' . $lll . '.file_collections;file_collections_enabled_file_collections_enabled_variant,
                --palette--;' . $lll . '.files;files_enabled_files_enabled_variant,
                --palette--;' . $lll . '.question_categories;question_categories_enabled_question_categories_enabled_variant,
                --palette--;' . $lll . '.attach_frontend_user_groups;attach_frontend_user_groups_enabled_attach_frontend_user_groups_enabled_variant,
            --div--;' . \CodingMs\AdditionalTca\Tca\Configuration::label('tab_images') . ',
                --palette--;' . $lll . '.images;images_enabled_images_enabled_variant,
                --palette--;' . $lll . '.feature_icons;feature_icons_enabled_feature_icons_enabled_variant,
                --palette--;' . $lll . '.other_images;other_images_enabled_other_images_enabled_variant,
            --div--;' . \CodingMs\AdditionalTca\Tca\Configuration::label($lll . '.tab_videos') . ',
                --palette--;' . $lll . '.videos;videos_enabled_videos_enabled_variant,
            --div--;' . \CodingMs\AdditionalTca\Tca\Configuration::label($lll . '.tab_attributes') . ',
                --palette--;' . $lll . '.attributes;attributes_enabled_attributes_enabled_variant,
            --div--;' . $lll . '.tab_custom,
                --palette--;' . $lll . '.palette_custom_information;custom_information,
                --palette--;' . $lll . '.attribute1;attribute1_enabled_attribute1_enabled_variant_attribute1_label_attribute1_type_description,
                --palette--;' . $lll . '.attribute2;attribute2_enabled_attribute2_enabled_variant_attribute2_label_attribute2_type_description,
                --palette--;' . $lll . '.attribute3;attribute3_enabled_attribute3_enabled_variant_attribute3_label_attribute3_type_description,
                --palette--;' . $lll . '.attribute4;attribute4_enabled_attribute4_enabled_variant_attribute4_label_attribute4_type_description,
                --palette--;' . $lll . '.attribute5;attribute5_enabled_attribute5_enabled_variant_attribute5_label_attribute5_type_description,
                --palette--;' . $lll . '.attribute6;attribute6_enabled_attribute6_enabled_variant_attribute6_label_attribute6_type_description,
                --palette--;' . $lll . '.attribute7;attribute7_enabled_attribute7_enabled_variant_attribute7_label_attribute7_type_description,
                --palette--;' . $lll . '.attribute8;attribute8_enabled_attribute8_enabled_variant_attribute8_label_attribute8_type_description,
                --palette--;' . $lll . '.attribute9;attribute9_enabled_attribute9_enabled_variant_attribute9_label_attribute9_type_description,
                --palette--;' . $lll . '.attribute10;attribute10_enabled_attribute10_enabled_variant_attribute10_label_attribute10_type_description,
                --palette--;' . $lll . '.attribute11;attribute11_enabled_attribute11_enabled_variant_attribute11_label_attribute11_type_description,
                --palette--;' . $lll . '.attribute12;attribute12_enabled_attribute12_enabled_variant_attribute12_label_attribute12_type_description,
            --div--;' . \CodingMs\AdditionalTca\Tca\Configuration::label('tab_language') . ',
                sys_language_uid;;;;1-1-1,
                l10n_parent,
                l10n_diffsource,
            --div--;' . \CodingMs\AdditionalTca\Tca\Configuration::label('tab_access') . ',
                hidden;;1,
                starttime,
                endtime
            '
        ],
    ],
    'palettes' => [
        'notice_title_subtitle_filter_import_id_description' => ['showitem' => 'notice, --linebreak--, title, subtitle, --linebreak--, filter, import_id, --linebreak--, description, --linebreak--, image'],
        'slug_enabled_slug_enabled_variant' => ['showitem' => 'slug_enabled, slug_enabled_variant'],
        'title_enabled_title_enabled_variant' => ['showitem' => 'title_enabled, title_enabled_variant'],
        'product_no_enabled_product_no_enabled_variant' => ['showitem' => 'product_no_enabled, product_no_enabled_variant'],
        'global_trade_item_no_enabled_global_trade_item_no_enabled_variant' => ['showitem' => 'global_trade_item_no_enabled, global_trade_item_no_enabled_variant'],
        'product_type_enabled_product_type_enabled_variant' => ['showitem' => 'product_type_enabled, product_type_enabled_variant'],
        'subtitle_enabled_subtitle_enabled_variant' => ['showitem' => 'subtitle_enabled, subtitle_enabled_variant'],
        'highlight_enabled_highlight_enabled_variant' => ['showitem' => 'highlight_enabled, highlight_enabled_variant'],
        'first_in_list_enabled_first_in_list_enabled_variant' => ['showitem' => 'first_in_list_enabled, first_in_list_enabled_variant'],
        'teaser_enabled_teaser_enabled_variant' => ['showitem' => 'teaser_enabled, teaser_enabled_variant'],
        'digital_product_enabled_digital_product_enabled_variant' => ['showitem' => 'digital_product_enabled, digital_product_enabled_variant'],
        'description_enabled_description_enabled_variant' => ['showitem' => 'description_enabled, description_enabled_variant'],
        'pdf_description_enabled_pdf_description_enabled_variant' => ['showitem' => 'pdf_description_enabled, pdf_description_enabled_variant'],
        'description_file_enabled_description_file_enabled_variant' => ['showitem' => 'description_file_enabled, description_file_enabled_variant'],
        'www_enabled_www_enabled_variant' => ['showitem' => 'www_enabled, www_enabled_variant'],
        'weight_enabled_weight_enabled_variant' => ['showitem' => 'weight_enabled, weight_enabled_variant'],
        'bulky_enabled_bulky_enabled_variant' => ['showitem' => 'bulky_enabled, bulky_enabled_variant'],
        'color_enabled_color_enabled_variant' => ['showitem' => 'color_enabled, color_enabled_variant'],
        'size_enabled_size_enabled_variant' => ['showitem' => 'size_enabled, size_enabled_variant'],
        // Tab: SEO
        'canonical_link_enabled_canonical_link_enabled_variant' => ['showitem' => 'canonical_link_enabled, canonical_link_enabled_variant'],
        'html_title_enabled_html_title_enabled_variant' => ['showitem' => 'html_title_enabled, html_title_enabled_variant'],
        'meta_abstract_enabled_meta_abstract_enabled_variant' => ['showitem' => 'meta_abstract_enabled, meta_abstract_enabled_variant'],
        'meta_description_enabled_meta_description_enabled_variant' => ['showitem' => 'meta_description_enabled, meta_description_enabled_variant'],
        'meta_keywords_enabled_meta_keywords_enabled_variant' => ['showitem' => 'meta_keywords_enabled, meta_keywords_enabled_variant'],
        // Tab: Prices
        'price_enabled_price_enabled_variant' => ['showitem' => 'price_enabled, price_enabled_variant'],
        'price_type_enabled_price_type_enabled_variant' => ['showitem' => 'price_type_enabled, price_type_enabled_variant'],
        'tax_enabled_tax_enabled_variant' => ['showitem' => 'tax_enabled, tax_enabled_variant'],
        'offer_enabled_offer_enabled_variant' => ['showitem' => 'offer_enabled, offer_enabled_variant'],
        'offer_rebate_value_offer_rebate_value_enabled_variant' => ['showitem' => 'offer_rebate_value_enabled, offer_rebate_value_enabled_variant'],
        'stock_enabled_stock_enabled_variant' => ['showitem' => 'stock_enabled, stock_enabled_variant'],
        'unit_enabled_unit_enabled_variant' => ['showitem' => 'unit_enabled, unit_enabled_variant'],
        'discount_enabled_discount_enabled_variant' => ['showitem' => 'discount_enabled, discount_enabled_variant'],
        'graduated_prices_enabled_graduated_prices_enabled_variant' => ['showitem' => 'graduated_prices_enabled, graduated_prices_enabled_variant'],

        // Tab: Relations
        'categories_enabled_categories_enabled_variant' => ['showitem' => 'categories_enabled, categories_enabled_variant'],
        'tags_enabled_tags_enabled_variant' => ['showitem' => 'tags_enabled, tags_enabled_variant'],
        'related_products_enabled_related_products_enabled_variant' => ['showitem' => 'related_products_enabled, related_products_enabled_variant'],
        'accessories_enabled_accessories_enabled_variant' => ['showitem' => 'accessories_enabled, accessories_enabled_variant'],
        'accessories_required_enabled_accessories_required_enabled_variant' => ['showitem' => 'accessories_required_enabled, accessories_required_enabled_variant'],
        'file_collections_enabled_file_collections_enabled_variant' => ['showitem' => 'file_collections_enabled, file_collections_enabled_variant'],
        'files_enabled_files_enabled_variant' => ['showitem' => 'files_enabled, files_enabled_variant'],
        'question_categories_enabled_question_categories_enabled_variant' => ['showitem' => 'question_categories_enabled, question_categories_enabled_variant'],
        'attach_frontend_user_groups_enabled_attach_frontend_user_groups_enabled_variant' => ['showitem' => 'attach_frontend_user_groups_enabled, attach_frontend_user_groups_enabled_variant'],
        // Tab: images
        'images_enabled_images_enabled_variant' => ['showitem' => 'images_enabled, images_enabled_variant'],
        'feature_icons_enabled_feature_icons_enabled_variant' => ['showitem' => 'feature_icons_enabled, feature_icons_enabled_variant'],
        'other_images_enabled_other_images_enabled_variant' => ['showitem' => 'other_images_enabled, other_images_enabled_variant'],
        // Tab: videos
        'videos_enabled_videos_enabled_variant' => ['showitem' => 'videos_enabled, videos_enabled_variant'],
        'custom_information' => ['showitem' => 'custom_information_information, --linebreak--, custom_information_enabled, custom_information_required, --linebreak--, custom_information_label, custom_information_type'],
        // Tab: attributes
        'attributes_enabled_attributes_enabled_variant' => ['showitem' => 'attributes_enabled, attributes_enabled_variant'],
        'attribute1_enabled_attribute1_enabled_variant_attribute1_label_attribute1_type_description' =>
            ['showitem' => 'attribute1_enabled, attribute1_enabled_variant, --linebreak--, attribute1_label, attribute1_type, --linebreak--, ,attribute1_description'],
        'attribute2_enabled_attribute2_enabled_variant_attribute2_label_attribute2_type_description' =>
            ['showitem' => 'attribute2_enabled, attribute2_enabled_variant, --linebreak--, attribute2_label, attribute2_type, --linebreak--, ,attribute2_description'],
        'attribute3_enabled_attribute3_enabled_variant_attribute3_label_attribute3_type_description' =>
            ['showitem' => 'attribute3_enabled, attribute3_enabled_variant, --linebreak--, attribute3_label, attribute3_type, --linebreak--, ,attribute3_description'],
        'attribute4_enabled_attribute4_enabled_variant_attribute4_label_attribute4_type_description' =>
            ['showitem' => 'attribute4_enabled, attribute4_enabled_variant, --linebreak--, attribute4_label, attribute4_type, --linebreak--, ,attribute4_description'],
        'attribute5_enabled_attribute5_enabled_variant_attribute5_label_attribute5_type_description' =>
            ['showitem' => 'attribute5_enabled, attribute5_enabled_variant, --linebreak--, attribute5_label, attribute5_type, --linebreak--, ,attribute5_description'],
        'attribute6_enabled_attribute6_enabled_variant_attribute6_label_attribute6_type_description' =>
            ['showitem' => 'attribute6_enabled, attribute6_enabled_variant, --linebreak--, attribute6_label, attribute6_type, --linebreak--, ,attribute6_description'],
        'attribute7_enabled_attribute7_enabled_variant_attribute7_label_attribute7_type_description' =>
            ['showitem' => 'attribute7_enabled, attribute7_enabled_variant, --linebreak--, attribute7_label, attribute7_type, --linebreak--, ,attribute7_description'],
        'attribute8_enabled_attribute8_enabled_variant_attribute8_label_attribute8_type_description' =>
            ['showitem' => 'attribute8_enabled, attribute8_enabled_variant, --linebreak--, attribute8_label, attribute8_type, --linebreak--, ,attribute8_description'],
        'attribute9_enabled_attribute9_enabled_variant_attribute9_label_attribute9_type_description' =>
            ['showitem' => 'attribute9_enabled, attribute9_enabled_variant, --linebreak--, attribute9_label, attribute9_type, --linebreak--, ,attribute9_description'],
        'attribute10_enabled_attribute10_enabled_variant_attribute10_label_attribute10_type_description' =>
            ['showitem' => 'attribute10_enabled, attribute10_enabled_variant, --linebreak--, attribute10_label, attribute10_type, --linebreak--, ,attribute10_description'],
        'attribute11_enabled_attribute11_enabled_variant_attribute11_label_attribute11_type_description' =>
            ['showitem' => 'attribute11_enabled, attribute11_enabled_variant, --linebreak--, attribute11_label, attribute11_type, --linebreak--, ,attribute11_description'],
        'attribute12_enabled_attribute12_enabled_variant_attribute12_label_attribute12_type_description' =>
            ['showitem' => 'attribute12_enabled, attribute12_enabled_variant, --linebreak--, attribute12_label, attribute12_type, --linebreak--, ,attribute12_description'],
        // Tab: custom
    ],
    'columns' => [
        'sys_language_uid' => \CodingMs\AdditionalTca\Tca\Configuration::full('sys_language_uid'),
        'l10n_parent' => \CodingMs\AdditionalTca\Tca\Configuration::full('l10n_parent', $table),
        'l10n_diffsource' => \CodingMs\AdditionalTca\Tca\Configuration::full('l10n_diffsource'),
        't3ver_label' => \CodingMs\AdditionalTca\Tca\Configuration::full('t3ver_label'),
        'hidden' => \CodingMs\AdditionalTca\Tca\Configuration::full('hidden'),
        'crdate' => [
            'config' => [
                'type' => 'passthrough'
            ],
        ],
        'tstamp' => [
            'config' => [
                'type' => 'passthrough'
            ],
        ],
        'cruser_id' => [
            'config' => [
                'type' => 'passthrough'
            ],
        ],
        'title' => [
            'exclude' => 0,
            'label' => $lll . '.title',
            'config' => \CodingMs\Shop\Tca\Configuration::get('string', true),
        ],
        'subtitle' => [
            'exclude' => 0,
            'label' => $lll . '.subtitle',
            'config' => \CodingMs\Shop\Tca\Configuration::get('string'),
        ],
        'import_id' => [
            'exclude' => 1,
            'label' => $lll . '.import_id',
            'description' => $lll . '.import_id.description',
            'config' => \CodingMs\Shop\Tca\Configuration::get('string', false, true),
        ],
        'filter' => [
            'exclude' => 0,
            'label' => $lll . '.filter',
            'description' => $lll . '.filter_description',
            'config' => \CodingMs\Shop\Tca\Configuration::get('productTypeFilter'),
        ],
        'notice' => [
            'exclude' => 0,
            'label' => '',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('notice', false, false, '', ['notice' => $lll . '.notice']),
        ],
        'description' => [
            'exclude' => 1,
            'label' => $lll . '.description',
            'config' => \CodingMs\Shop\Tca\Configuration::get('rte'),
        ],
        'image' => [
            'exclude' => 0,
            'label' => $lll . '.image',
            'config' => \CodingMs\Shop\Tca\Configuration::get('imageSingleAltTitle', false, false, '', ['fileTypes' => 'png,jpg,jpeg,svg']),
        ],
        'title_enabled' => [
            'exclude' => 0,
            'label' => $lll . '.title_enabled',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', true, true, $lll . '.enabled_label', ['default' => 1]),
        ],
        'title_enabled_variant' => [
            'exclude' => 0,
            'label' => $lll . '.title_enabled_variant',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', true, true, $lll . '.enabled_in_variants_label', ['default' => 1]),
        ],
        'slug_enabled' => [
            'exclude' => 0,
            'label' => $lll . '.slug_enabled',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', true, true, $lll . '.enabled_label', ['default' => 1]),
        ],
        'slug_enabled_variant' => [
            'exclude' => 0,
            'label' => $lll . '.slug_enabled_variant',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', true, true, $lll . '.enabled_in_variants_label', ['default' => 1]),
        ],
        'subtitle_enabled' => [
            'exclude' => 0,
            'label' => $lll . '.subtitle_enabled',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', true, true, $lll . '.enabled_label', ['default' => 1]),
        ],
        'subtitle_enabled_variant' => [
            'exclude' => 0,
            'label' => $lll . '.subtitle_enabled_variant',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', true, true, $lll . '.enabled_in_variants_label', ['default' => 1]),
        ],
        'product_no_enabled' => [
            'exclude' => 0,
            'label' => $lll . '.product_no_enabled',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', true, true, $lll . '.enabled_label', ['default' => 1]),
        ],
        'product_no_enabled_variant' => [
            'exclude' => 0,
            'label' => $lll . '.product_no_enabled_variant',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', true, true, $lll . '.enabled_in_variants_label', ['default' => 1]),
        ],
        'global_trade_item_no_enabled' => [
            'exclude' => 0,
            'label' => $lll . '.global_trade_item_no_enabled',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_label', ['default' => 1]),
        ],
        'global_trade_item_no_enabled_variant' => [
            'exclude' => 0,
            'label' => $lll . '.global_trade_item_no_enabled_variant',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_in_variants_label', ['default' => 1]),
        ],
        'product_type_enabled' => [
            'exclude' => 0,
            'label' => $lll . '.product_type_enabled',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', true, true, $lll . '.enabled_label', ['default' => 1]),
        ],
        'product_type_enabled_variant' => [
            'exclude' => 0,
            'label' => $lll . '.product_type_enabled_variant',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', true, true, $lll . '.enabled_in_variants_label', ['default' => 0]),
        ],
        'price_enabled' => [
            'exclude' => 0,
            'label' => $lll . '.price_enabled',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', true, true, $lll . '.enabled_label', ['default' => 1]),
        ],
        'price_enabled_variant' => [
            'exclude' => 0,
            'label' => $lll . '.price_enabled_variant',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_in_variants_label', ['default' => 1]),
        ],
        'price_type_enabled' => [
            'exclude' => 0,
            'label' => $lll . '.price_type_enabled',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', true, true, $lll . '.enabled_label', ['default' => 1]),
        ],
        'price_type_enabled_variant' => [
            'exclude' => 0,
            'label' => $lll . '.price_type_enabled_variant',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', true, true, $lll . '.enabled_in_variants_label', ['default' => 0]),
        ],
        'tax_enabled' => [
            'exclude' => 0,
            'label' => $lll . '.tax_enabled',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', true, true, $lll . '.enabled_label', ['default' => 1]),
        ],
        'tax_enabled_variant' => [
            'exclude' => 0,
            'label' => $lll . '.tax_enabled_variant',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', true, true, $lll . '.enabled_in_variants_label', ['default' => 0]),
        ],
        'digital_product_enabled' => [
            'exclude' => 0,
            'label' => $lll . '.digital_product_enabled',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_label', ['default' => 1]),
        ],
        'digital_product_enabled_variant' => [
            'exclude' => 0,
            'label' => $lll . '.digital_product_enabled_variant',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', true, true, $lll . '.enabled_in_variants_label', ['default' => 0]),
        ],
        'graduated_prices_enabled' => [
            'exclude' => 0,
            'label' => $lll . '.graduated_prices_enabled',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_label'),
        ],
        'graduated_prices_enabled_variant' => [
            'exclude' => 0,
            'label' => $lll . '.graduated_prices_enabled_variant',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_in_variants_label'),
        ],
        'discount_enabled' => [
            'exclude' => 0,
            'label' => $lll . '.discount_enabled',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_label'),
        ],
        'discount_enabled_variant' => [
            'exclude' => 0,
            'label' => $lll . '.discount_enabled_variant',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_in_variants_label'),
        ],
        'teaser_enabled' => [
            'exclude' => 0,
            'label' => $lll . '.teaser_enabled',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_label'),
        ],
        'teaser_enabled_variant' => [
            'exclude' => 0,
            'label' => $lll . '.teaser_enabled_variant',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_in_variants_label'),
        ],
        'description_enabled' => [
            'exclude' => 0,
            'label' => $lll . '.description_enabled',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_label'),
        ],
        'description_enabled_variant' => [
            'exclude' => 0,
            'label' => $lll . '.description_enabled_variant',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_in_variants_label'),
        ],
        'pdf_description_enabled' => [
            'exclude' => 0,
            'label' => $lll . '.pdf_description_enabled',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_label'),
        ],
        'pdf_description_enabled_variant' => [
            'exclude' => 0,
            'label' => $lll . '.pdf_description_enabled_variant',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_in_variants_label'),
        ],
        'description_file_enabled' => [
            'exclude' => 0,
            'label' => $lll . '.description_file_enabled',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_label'),
        ],
        'description_file_enabled_variant' => [
            'exclude' => 0,
            'label' => $lll . '.description_file_enabled_variant',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_in_variants_label'),
        ],
        'unit_enabled' => [
            'exclude' => 0,
            'label' => $lll . '.unit_enabled',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_label'),
        ],
        'unit_enabled_variant' => [
            'exclude' => 0,
            'label' => $lll . '.unit_enabled_variant',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_in_variants_label'),
        ],
        'www_enabled' => [
            'exclude' => 0,
            'label' => $lll . '.www_enabled',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_label'),
        ],
        'www_enabled_variant' => [
            'exclude' => 0,
            'label' => $lll . '.www_enabled_variant',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_in_variants_label'),
        ],
        'stock_enabled' => [
            'exclude' => 0,
            'label' => $lll . '.stock_enabled',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_label'),
        ],
        'stock_enabled_variant' => [
            'exclude' => 0,
            'label' => $lll . '.stock_enabled_variant',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_in_variants_label'),
        ],
        'weight_enabled' => [
            'exclude' => 0,
            'label' => $lll . '.weight_enabled',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_label'),
        ],
        'weight_enabled_variant' => [
            'exclude' => 0,
            'label' => $lll . '.weight_enabled_variant',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_in_variants_label'),
        ],
        'bulky_enabled' => [
            'exclude' => 0,
            'label' => $lll . '.bulky_enabled',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_label'),
        ],
        'bulky_enabled_variant' => [
            'exclude' => 0,
            'label' => $lll . '.bulky_enabled_variant',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_in_variants_label'),
        ],
        'offer_enabled' => [
            'exclude' => 0,
            'label' => $lll . '.offer_enabled',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_label'),
        ],
        'offer_enabled_variant' => [
            'exclude' => 0,
            'label' => $lll . '.offer_enabled_variant',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_in_variants_label'),
        ],
        'offer_rebate_value_enabled' => [
            'exclude' => 0,
            'label' => $lll . '.offer_rebate_value_enabled',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_label'),
        ],
        'offer_rebate_value_enabled_variant' => [
            'exclude' => 0,
            'label' => $lll . '.offer_rebate_value_enabled_variant',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_in_variants_label'),
        ],
        'highlight_enabled' => [
            'exclude' => 0,
            'label' => $lll . '.highlight_enabled',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_label'),
        ],
        'highlight_enabled_variant' => [
            'exclude' => 0,
            'label' => $lll . '.highlight_enabled_variant',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_in_variants_label'),
        ],
        'first_in_list_enabled' => [
            'exclude' => 0,
            'label' => $lll . '.first_in_list_enabled',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_label'),
        ],
        'first_in_list_enabled_variant' => [
            'exclude' => 0,
            'label' => $lll . '.first_in_list_enabled_variant',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_in_variants_label'),
        ],
        'color_enabled' => [
            'exclude' => 0,
            'label' => $lll . '.color_enabled',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_label'),
        ],
        'color_enabled_variant' => [
            'exclude' => 0,
            'label' => $lll . '.color_enabled_variant',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_in_variants_label'),
        ],
        'size_enabled' => [
            'exclude' => 0,
            'label' => $lll . '.size_enabled',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_label'),
        ],
        'size_enabled_variant' => [
            'exclude' => 0,
            'label' => $lll . '.size_enabled_variant',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_in_variants_label'),
        ],
        'canonical_link_enabled' => [
            'exclude' => 0,
            'label' => $lll . '.canonical_link_enabled',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_label'),
        ],
        'canonical_link_enabled_variant' => [
            'exclude' => 0,
            'label' => $lll . '.canonical_link_enabled_variant',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_in_variants_label'),
        ],
        'html_title_enabled' => [
            'exclude' => 0,
            'label' => $lll . '.html_title_enabled',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_label'),
        ],
        'html_title_enabled_variant' => [
            'exclude' => 0,
            'label' => $lll . '.html_title_enabled_variant',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_in_variants_label'),
        ],
        'meta_abstract_enabled' => [
            'exclude' => 0,
            'label' => $lll . '.meta_abstract_enabled',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_label'),
        ],
        'meta_abstract_enabled_variant' => [
            'exclude' => 0,
            'label' => $lll . '.meta_abstract_enabled_variant',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_in_variants_label'),
        ],
        'meta_description_enabled' => [
            'exclude' => 0,
            'label' => $lll . '.meta_description_enabled',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_label'),
        ],
        'meta_description_enabled_variant' => [
            'exclude' => 0,
            'label' => $lll . '.meta_description_enabled_variant',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_in_variants_label'),
        ],
        'meta_keywords_enabled' => [
            'exclude' => 0,
            'label' => $lll . '.meta_keywords_enabled',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_label'),
        ],
        'meta_keywords_enabled_variant' => [
            'exclude' => 0,
            'label' => $lll . '.meta_keywords_enabled_variant',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_in_variants_label'),
        ],
        'custom_information_information' => [
            'exclude' => 0,
            'label' => '',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('notice', false, false, '', [
                'notice' => $lll . '.custom_information_information',
            ]),
        ],
        'custom_information_enabled' => [
            'exclude' => 0,
            'label' => $lll . '.custom_information_enabled',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_label'),
        ],
        'custom_information_required' => [
            'exclude' => 0,
            'label' => $lll . '.custom_information_required',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_label'),
        ],
        'custom_information_label' => [
            'exclude' => 0,
            'label' => $lll . '.custom_information_label',
            'config' => \CodingMs\Shop\Tca\Configuration::get('string'),
        ],
        'custom_information_type' => [
            'exclude' => 0,
            'label' => $lll . '.custom_information_type',
            'config' => \CodingMs\Shop\Tca\Configuration::get('select', true, false, '', [
                ['label' => $lll . '.custom_information_type_string', 'value' => 'string'],
                ['label' => $lll . '.custom_information_type_int', 'value' => 'int'],
            ]),
        ],
        'attribute1_enabled' => [
            'exclude' => 0,
            'label' => $lll . '.attribute1_enabled',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_label'),
        ],
        'attribute1_enabled_variant' => [
            'exclude' => 0,
            'label' => $lll . '.attribute1_enabled_variant',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_in_variants_label'),
        ],
        'attribute1_label' => [
            'exclude' => 0,
            'label' => $lll . '.attribute_label',
            'config' => \CodingMs\Shop\Tca\Configuration::get('string'),
        ],
        'attribute1_description' => [
            'exclude' => 0,
            'label' => $lll . '.attribute_description',
            'config' => \CodingMs\Shop\Tca\Configuration::get('string'),
        ],
        'attribute1_type' => [
            'exclude' => 0,
            'label' => $lll . '.attribute_type',
            'config' => \CodingMs\Shop\Tca\Configuration::get('attributeType'),
        ],
        'attribute2_enabled' => [
            'exclude' => 0,
            'label' => $lll . '.attribute2_enabled',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_label'),
        ],
        'attribute2_enabled_variant' => [
            'exclude' => 0,
            'label' => $lll . '.attribute2_enabled_variant',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_in_variants_label'),
        ],
        'attribute2_label' => [
            'exclude' => 0,
            'label' => $lll . '.attribute_label',
            'config' => \CodingMs\Shop\Tca\Configuration::get('string'),
        ],
        'attribute2_description' => [
            'exclude' => 0,
            'label' => $lll . '.attribute_description',
            'config' => \CodingMs\Shop\Tca\Configuration::get('string'),
        ],
        'attribute2_type' => [
            'exclude' => 0,
            'label' => $lll . '.attribute_type',
            'config' => \CodingMs\Shop\Tca\Configuration::get('attributeType'),
        ],
        'attribute3_enabled' => [
            'exclude' => 0,
            'label' => $lll . '.attribute3_enabled',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_label'),
        ],
        'attribute3_enabled_variant' => [
            'exclude' => 0,
            'label' => $lll . '.attribute3_enabled_variant',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_in_variants_label'),
        ],
        'attribute3_label' => [
            'exclude' => 0,
            'label' => $lll . '.attribute_label',
            'config' => \CodingMs\Shop\Tca\Configuration::get('string'),
        ],
        'attribute3_description' => [
            'exclude' => 0,
            'label' => $lll . '.attribute_description',
            'config' => \CodingMs\Shop\Tca\Configuration::get('string'),
        ],
        'attribute3_type' => [
            'exclude' => 0,
            'label' => $lll . '.attribute_type',
            'config' => \CodingMs\Shop\Tca\Configuration::get('attributeType'),
        ],
        'attribute4_enabled' => [
            'exclude' => 0,
            'label' => $lll . '.attribute4_enabled',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_label'),
        ],
        'attribute4_enabled_variant' => [
            'exclude' => 0,
            'label' => $lll . '.attribute4_enabled_variant',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_in_variants_label'),
        ],
        'attribute4_label' => [
            'exclude' => 0,
            'label' => $lll . '.attribute_label',
            'config' => \CodingMs\Shop\Tca\Configuration::get('string'),
        ],
        'attribute4_description' => [
            'exclude' => 0,
            'label' => $lll . '.attribute_description',
            'config' => \CodingMs\Shop\Tca\Configuration::get('string'),
        ],
        'attribute4_type' => [
            'exclude' => 0,
            'label' => $lll . '.attribute_type',
            'config' => \CodingMs\Shop\Tca\Configuration::get('attributeType'),
        ],
        'attribute5_enabled' => [
            'exclude' => 0,
            'label' => $lll . '.attribute5_enabled',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_label'),
        ],
        'attribute5_enabled_variant' => [
            'exclude' => 0,
            'label' => $lll . '.attribute5_enabled_variant',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_in_variants_label'),
        ],
        'attribute5_label' => [
            'exclude' => 0,
            'label' => $lll . '.attribute_label',
            'config' => \CodingMs\Shop\Tca\Configuration::get('string'),
        ],
        'attribute5_description' => [
            'exclude' => 0,
            'label' => $lll . '.attribute_description',
            'config' => \CodingMs\Shop\Tca\Configuration::get('string'),
        ],
        'attribute5_type' => [
            'exclude' => 0,
            'label' => $lll . '.attribute_type',
            'config' => \CodingMs\Shop\Tca\Configuration::get('attributeType'),
        ],
        'attribute6_enabled' => [
            'exclude' => 0,
            'label' => $lll . '.attribute6_enabled',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_label'),
        ],
        'attribute6_enabled_variant' => [
            'exclude' => 0,
            'label' => $lll . '.attribute6_enabled_variant',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_in_variants_label'),
        ],
        'attribute6_label' => [
            'exclude' => 0,
            'label' => $lll . '.attribute_label',
            'config' => \CodingMs\Shop\Tca\Configuration::get('string'),
        ],
        'attribute6_description' => [
            'exclude' => 0,
            'label' => $lll . '.attribute_description',
            'config' => \CodingMs\Shop\Tca\Configuration::get('string'),
        ],
        'attribute6_type' => [
            'exclude' => 0,
            'label' => $lll . '.attribute_type',
            'config' => \CodingMs\Shop\Tca\Configuration::get('attributeType'),
        ],
        'attribute7_enabled' => [
            'exclude' => 0,
            'label' => $lll . '.attribute7_enabled',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_label'),
        ],
        'attribute7_enabled_variant' => [
            'exclude' => 0,
            'label' => $lll . '.attribute7_enabled_variant',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_in_variants_label'),
        ],
        'attribute7_label' => [
            'exclude' => 0,
            'label' => $lll . '.attribute_label',
            'config' => \CodingMs\Shop\Tca\Configuration::get('string'),
        ],
        'attribute7_description' => [
            'exclude' => 0,
            'label' => $lll . '.attribute_description',
            'config' => \CodingMs\Shop\Tca\Configuration::get('string'),
        ],
        'attribute7_type' => [
            'exclude' => 0,
            'label' => $lll . '.attribute_type',
            'config' => \CodingMs\Shop\Tca\Configuration::get('attributeType'),
        ],
        'attribute8_enabled' => [
            'exclude' => 0,
            'label' => $lll . '.attribute8_enabled',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_label'),
        ],
        'attribute8_enabled_variant' => [
            'exclude' => 0,
            'label' => $lll . '.attribute8_enabled_variant',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_in_variants_label'),
        ],
        'attribute8_label' => [
            'exclude' => 0,
            'label' => $lll . '.attribute_label',
            'config' => \CodingMs\Shop\Tca\Configuration::get('string'),
        ],
        'attribute8_description' => [
            'exclude' => 0,
            'label' => $lll . '.attribute_description',
            'config' => \CodingMs\Shop\Tca\Configuration::get('string'),
        ],
        'attribute8_type' => [
            'exclude' => 0,
            'label' => $lll . '.attribute_type',
            'config' => \CodingMs\Shop\Tca\Configuration::get('attributeType'),
        ],
        'attribute9_enabled' => [
            'exclude' => 0,
            'label' => $lll . '.attribute9_enabled',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_label'),
        ],
        'attribute9_enabled_variant' => [
            'exclude' => 0,
            'label' => $lll . '.attribute9_enabled_variant',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_in_variants_label'),
        ],
        'attribute9_label' => [
            'exclude' => 0,
            'label' => $lll . '.attribute_label',
            'config' => \CodingMs\Shop\Tca\Configuration::get('string'),
        ],
        'attribute9_description' => [
            'exclude' => 0,
            'label' => $lll . '.attribute_description',
            'config' => \CodingMs\Shop\Tca\Configuration::get('string'),
        ],
        'attribute9_type' => [
            'exclude' => 0,
            'label' => $lll . '.attribute_type',
            'config' => \CodingMs\Shop\Tca\Configuration::get('attributeType'),
        ],
        'attribute10_enabled' => [
            'exclude' => 0,
            'label' => $lll . '.attribute10_enabled',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_label'),
        ],
        'attribute10_enabled_variant' => [
            'exclude' => 0,
            'label' => $lll . '.attribute10_enabled_variant',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_in_variants_label'),
        ],
        'attribute10_label' => [
            'exclude' => 0,
            'label' => $lll . '.attribute_label',
            'config' => \CodingMs\Shop\Tca\Configuration::get('string'),
        ],
        'attribute10_description' => [
            'exclude' => 0,
            'label' => $lll . '.attribute_description',
            'config' => \CodingMs\Shop\Tca\Configuration::get('string'),
        ],
        'attribute10_type' => [
            'exclude' => 0,
            'label' => $lll . '.attribute_type',
            'config' => \CodingMs\Shop\Tca\Configuration::get('attributeType'),
        ],
        'attribute11_enabled' => [
            'exclude' => 0,
            'label' => $lll . '.attribute11_enabled',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_label'),
        ],
        'attribute11_enabled_variant' => [
            'exclude' => 0,
            'label' => $lll . '.attribute11_enabled_variant',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_in_variants_label'),
        ],
        'attribute11_label' => [
            'exclude' => 0,
            'label' => $lll . '.attribute_label',
            'config' => \CodingMs\Shop\Tca\Configuration::get('string'),
        ],
        'attribute11_description' => [
            'exclude' => 0,
            'label' => $lll . '.attribute_description',
            'config' => \CodingMs\Shop\Tca\Configuration::get('string'),
        ],
        'attribute11_type' => [
            'exclude' => 0,
            'label' => $lll . '.attribute_type',
            'config' => \CodingMs\Shop\Tca\Configuration::get('attributeType'),
        ],
        'attribute12_enabled' => [
            'exclude' => 0,
            'label' => $lll . '.attribute12_enabled',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_label'),
        ],
        'attribute12_enabled_variant' => [
            'exclude' => 0,
            'label' => $lll . '.attribute12_enabled_variant',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_in_variants_label'),
        ],
        'attribute12_label' => [
            'exclude' => 0,
            'label' => $lll . '.attribute_label',
            'config' => \CodingMs\Shop\Tca\Configuration::get('string'),
        ],
        'attribute12_description' => [
            'exclude' => 0,
            'label' => $lll . '.attribute_description',
            'config' => \CodingMs\Shop\Tca\Configuration::get('string'),
        ],
        'attribute12_type' => [
            'exclude' => 0,
            'label' => $lll . '.attribute_type',
            'config' => \CodingMs\Shop\Tca\Configuration::get('attributeType'),
        ],
        'categories_enabled' => [
            'exclude' => 0,
            'label' => $lll . '.categories_enabled',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_label'),
        ],
        'categories_enabled_variant' => [
            'exclude' => 0,
            'label' => $lll . '.categories_enabled_variant',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_in_variants_label'),
        ],
        'tags_enabled' => [
            'exclude' => 0,
            'label' => $lll . '.tags_enabled',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_label'),
        ],
        'tags_enabled_variant' => [
            'exclude' => 0,
            'label' => $lll . '.tags_enabled_variant',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_in_variants_label'),
        ],
        'related_products_enabled' => [
            'exclude' => 0,
            'label' => $lll . '.related_products_enabled',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_label'),
        ],
        'related_products_enabled_variant' => [
            'exclude' => 0,
            'label' => $lll . '.related_products_enabled_variant',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_in_variants_label'),
        ],
        'accessories_enabled' => [
            'exclude' => 0,
            'label' => $lll . '.accessories_enabled',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_label'),
        ],
        'accessories_enabled_variant' => [
            'exclude' => 0,
            'label' => $lll . '.accessories_enabled_variant',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_in_variants_label'),
        ],
        'accessories_required_enabled' => [
            'exclude' => 0,
            'label' => $lll . '.accessories_required_enabled',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_label'),
        ],
        'accessories_required_enabled_variant' => [
            'exclude' => 0,
            'label' => $lll . '.accessories_required_enabled_variant',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_in_variants_label'),
        ],
        'file_collections_enabled' => [
            'exclude' => 0,
            'label' => $lll . '.file_collections_enabled',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_label'),
        ],
        'file_collections_enabled_variant' => [
            'exclude' => 0,
            'label' => $lll . '.file_collections_enabled_variant',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_in_variants_label'),
        ],
        'files_enabled' => [
            'exclude' => 0,
            'label' => $lll . '.files_enabled',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_label'),
        ],
        'files_enabled_variant' => [
            'exclude' => 0,
            'label' => $lll . '.files_enabled_variant',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_in_variants_label'),
        ],
        'question_categories_enabled' => [
            'exclude' => 0,
            'label' => $lll . '.question_categories_enabled',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_label'),
        ],
        'question_categories_enabled_variant' => [
            'exclude' => 0,
            'label' => $lll . '.question_categories_enabled_variant',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_in_variants_label'),
        ],
        'attach_frontend_user_groups_enabled' => [
            'exclude' => 0,
            'label' => $lll . '.attach_frontend_user_groups_enabled',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_label'),
        ],
        'attach_frontend_user_groups_enabled_variant' => [
            'exclude' => 0,
            'label' => $lll . '.attach_frontend_user_groups_enabled_variant',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_in_variants_label'),
        ],
        'images_enabled' => [
            'exclude' => 0,
            'label' => $lll . '.images_enabled',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_label'),
        ],
        'images_enabled_variant' => [
            'exclude' => 0,
            'label' => $lll . '.images_enabled_variant',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_in_variants_label'),
        ],
        'feature_icons_enabled' => [
            'exclude' => 0,
            'label' => $lll . '.feature_icons_enabled',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_label'),
        ],
        'feature_icons_enabled_variant' => [
            'exclude' => 0,
            'label' => $lll . '.feature_icons_enabled_variant',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_in_variants_label'),
        ],
        'other_images_enabled' => [
            'exclude' => 0,
            'label' => $lll . '.other_images_enabled',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_label'),
        ],
        'other_images_enabled_variant' => [
            'exclude' => 0,
            'label' => $lll . '.other_images_enabled_variant',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_in_variants_label'),
        ],
        'videos_enabled' => [
            'exclude' => 0,
            'label' => $lll . '.videos_enabled',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_label'),
        ],
        'videos_enabled_variant' => [
            'exclude' => 0,
            'label' => $lll . '.videos_enabled_variant',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_in_variants_label'),
        ],
        'attributes_enabled' => [
            'exclude' => 0,
            'label' => $lll . '.attributes_enabled',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_label'),
        ],
        'attributes_enabled_variant' => [
            'exclude' => 0,
            'label' => $lll . '.attributes_enabled_variant',
            'config' => \CodingMs\Shop\Tca\Configuration::get('checkbox', false, false, $lll . '.enabled_in_variants_label'),
        ],
    ],
];

if ((int)\TYPO3\CMS\Core\Utility\VersionNumberUtility::getCurrentTypo3Version() < 12) {
    $return['ctrl']['cruser_id'] = 'cruser_id';
    $return['columns']['custom_information_type']['config']['items'] = [
        [$lll . '.custom_information_type_string', 'string'],
        [$lll . '.custom_information_type_int', 'int'],
    ];
}

return $return;
