<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}

$extKey = 'shop';
$table = 'tx_shop_domain_model_producttax';
$lll = 'LLL:EXT:' . $extKey . '/Resources/Private/Language/locallang_db.xlf:' . $table;

$return = [
    'ctrl' => [
        'title' => $lll . '',
        'label' => 'value',
        'label_userFunc' => CodingMs\Shop\Tca\LabelService::class . '->getProductTaxLabel',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'versioningWS' => true,
        'origUid' => 't3_origuid',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
        ],
        'searchFields' => 'title,',
        'typeicon_classes' => [
            'default' => 'mimetypes-x-content-shop-tax'
        ],
        'accessUtility' => \CodingMs\ShopPro\Utility\AccessUtility::class
    ],
    'types' => [
        '1' => ['showitem' => 'title,value,stripe_id,overlay,--div--;LLL:EXT:shop/Resources/Private/Language/locallang_db.xlf:tx_shop_domain_model_producttax.tab_language,sys_language_uid,--palette--,l10n_parent,l10n_diffsource,--div--;LLL:EXT:shop/Resources/Private/Language/locallang_db.xlf:tx_shop_domain_model_producttax.tab_access,hidden,--palette--;;1'],
    ],
    'columns' => [
        'crdate' => [
            'config' => [
                'type' => 'passthrough'
            ],
        ],
        'tstamp' => [
            'config' => [
                'type' => 'passthrough'
            ],
        ],
        'cruser_id' => [
            'config' => [
                'type' => 'passthrough'
            ],
        ],
        'title' => [
            'exclude' => 0,
            'label' => $lll . '.title',
            'config' => \CodingMs\Shop\Tca\Configuration::get('string', true),
        ],
        'value' => [
            'exclude' => 0,
            'label' => $lll . '.value',
            'config' => \CodingMs\Shop\Tca\Configuration::get('percent', true),
        ],
        'stripe_id' => [
            'exclude' => 0,
            'label' => $lll . '.stripe_id',
            'description' => $lll . '.stripe_id_description',
            'config' => \CodingMs\Shop\Tca\Configuration::get('string'),
        ],
        'overlay' => [
            'exclude' => 0,
            'label' => $lll . '.overlay',
            'description' => $lll . '.overlay_description',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_shop_domain_model_producttaxcountryoverlay',
                'foreign_field' => 'default_tax',
            ],
        ],
    ],
];

if ((int)\TYPO3\CMS\Core\Utility\VersionNumberUtility::getCurrentTypo3Version() < 12) {
    $return['ctrl']['cruser_id'] = 'cruser_id';
}
return $return;
