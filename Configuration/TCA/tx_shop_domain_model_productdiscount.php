<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}

$extKey = 'shop';
$table = 'tx_shop_domain_model_productdiscount';
$lll = 'LLL:EXT:' . $extKey . '/Resources/Private/Language/locallang_db.xlf:' . $table;

$return = [
    'ctrl' => [
        'title' => $lll,
        'label' => 'label',
        'label_userFunc' => CodingMs\Shop\Tca\LabelService::class . '->getProductDiscountLabel',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'sortby' => 'sorting',
        'default_sortby' => 'sorting',
        'versioningWS' => true,
        'origUid' => 't3_origuid',
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
        ],
        'searchFields' => '',
        'typeicon_classes' => [
            'default' => 'mimetypes-x-content-shop-productshippingcost'
        ],
        'accessUtility' => \CodingMs\ShopPro\Utility\AccessUtility::class
    ],
    'types' => [
        '1' => ['showitem' => 'label,label_public,type,order_option,value_fixed,value_percent'],
    ],
    'palettes' => [],
    'columns' => [
        'sys_language_uid'  => \CodingMs\AdditionalTca\Tca\Configuration::full('sys_language_uid'),
        'l10n_parent' => \CodingMs\AdditionalTca\Tca\Configuration::full('l10n_parent', $table),
        'l10n_diffsource' => \CodingMs\AdditionalTca\Tca\Configuration::full('l10n_diffsource'),
        't3ver_label' => \CodingMs\AdditionalTca\Tca\Configuration::full('t3ver_label'),
        'hidden' => \CodingMs\AdditionalTca\Tca\Configuration::full('hidden'),
        'label' => [
            'exclude' => 0,
            'label' => $lll . '.label',
            'description' => $lll . '.label_description',
            'config' => \CodingMs\Shop\Tca\Configuration::get('string', true),
        ],
        'label_public' => [
            'exclude' => 0,
            'label' => $lll . '.label_public',
            'description' => $lll . '.label_public_description',
            'config' => \CodingMs\Shop\Tca\Configuration::get('string', true),
        ],
        'type' => [
            'exclude' => 0,
            'label' => $lll . '.type',
            'description' => $lll . '.type_description',
            'onChange' => 'reload',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['label' => $lll . '.type_fixed', 'value' => 'fixed'],
                    ['label' => $lll . '.type_percent', 'value' => 'percent'],
                ],
            ],
        ],
        'order_option' => [
            'exclude' => 0,
            'label' => $lll . '.order_option',
            'description' => $lll . '.order_option_description',
            'displayCond' => 'REC:NEW:false',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['label' => $lll . '.order_option_all', 'value' => 'all'],
                    ['label' => $lll . '.order_option_product', 'value' => 'product'],
                    ['label' => $lll . '.order_option_request', 'value' => 'request'],
                    ['label' => $lll . '.order_option_pre_payment', 'value' => 'prePayment'],
                    ['label' => $lll . '.order_option_on_invoice', 'value' => 'onInvoice'],
                    ['label' => $lll . '.order_option_pay_pal', 'value' => 'payPal'],
                    ['label' => $lll . '.order_option_pay_pal_plus', 'value' => 'payPalPlus'],
                    ['label' => $lll . '.order_option_klarna', 'value' => 'klarna'],
                    ['label' => $lll . '.order_option_stripe', 'value' => 'stripe'],
                ],
            ],
        ],
        'value_fixed' => [
            'exclude' => 0,
            'label' => $lll . '.value_fixed',
            'description' => $lll . '.value_fixed_description',
            'displayCond' => [
                'AND' => [
                    'FIELD:type:=:fixed',
                    'REC:NEW:false',
                ],
            ],
            'config' => \CodingMs\Shop\Tca\Configuration::get('currency', true),
        ],
        'value_percent' => [
            'exclude' => 0,
            'label' => $lll . '.value_percent',
            'description' => $lll . '.value_percent_description',
            'displayCond' => [
                'AND' => [
                    'FIELD:type:=:percent',
                    'REC:NEW:false',
                ],
            ],
            'config' => \CodingMs\Shop\Tca\Configuration::get('percent', true),
        ],
    ],
];

if ((int)\TYPO3\CMS\Core\Utility\VersionNumberUtility::getCurrentTypo3Version() < 12) {
    $return['ctrl']['cruser_id'] = 'cruser_id';
    $return['columns']['type']['config']['items'] = [
        [$lll . '.type_fixed', 'fixed'],
        [$lll . '.type_percent', 'percent'],
    ];
    $return['columns']['order_option']['config']['items'] = [
        [$lll . '.order_option_all', 'all'],
        [$lll . '.order_option_product', 'product'],
        [$lll . '.order_option_request', 'request'],
        [$lll . '.order_option_pre_payment', 'prePayment'],
        [$lll . '.order_option_on_invoice', 'onInvoice'],
        [$lll . '.order_option_pay_pal', 'payPal'],
        [$lll . '.order_option_pay_pal_plus', 'payPalPlus'],
        [$lll . '.order_option_klarna', 'klarna'],
        [$lll . '.order_option_stripe', 'stripe'],
    ];
}
return $return;
