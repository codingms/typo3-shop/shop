<?php

return [
    'shop:updateViewCount' => [
        'class' => \CodingMs\Shop\Command\UpdateViewCountCommand::class
    ],
];
