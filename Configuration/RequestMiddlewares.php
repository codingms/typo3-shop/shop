<?php

return [
    'frontend' => [
        'codingms/shop' => [
            'target' => \CodingMs\Shop\Middleware\ProductMiddleware::class,
            'after' => [
                'typo3/cms-frontend/backend-user-authentication',
                'typo3/cms-frontend/page-resolver',
            ],
        ],
    ],
];
