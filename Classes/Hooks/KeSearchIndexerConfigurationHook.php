<?php

declare(strict_types=1);

namespace CodingMs\Shop\Hooks;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Thomas Deuling <typo3@coding.ms>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use Doctrine\DBAL\Driver\Statement;
use Tpwd\KeSearch\Indexer\IndexerRunner;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\PathUtility;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

/**
 * IndexerConfiguration-Hook for KeSearch
 *
 * @author Thomas Deuling <typo3@coding.ms>
 */
class KeSearchIndexerConfigurationHook
{
    /**
     * @param array $params
     * @param array $pObj
     */
    public function registerIndexerConfiguration(&$params, $pObj): void
    {
        // add item to "type" field
        $newArray = [
            'Shop: Product indexer',
            'shop',
            PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath('shop')) . 'Resources/Public/Icons/Extension.svg'
        ];
        $params['items'][] = $newArray;
        // enable "sysfolder" field
        $GLOBALS['TCA']['tx_kesearch_indexerconfig']['columns']['sysfolder']['displayCond'] .= ',shop';
    }

    /**
     * Custom indexer for ke_search
     *
     * @param array $indexerConfig Configuration from TYPO3 Backend
     * @param IndexerRunner $indexerObject Reference to indexer class.
     * @return string Output.
     */
    /** @phpstan-ignore-next-line */
    public function customIndexer(array &$indexerConfig, IndexerRunner &$indexerObject): string
    {
        $content = '';
        if ($indexerConfig['type'] == 'shop') {
            // get all the entries to index
            // don't index hidden or deleted elements, BUT
            // get the elements with frontend user group access restrictions
            // or time (start / stop) restrictions.
            // Copy those restrictions to the index.
            $fields = '*';
            $table = 'tx_shop_domain_model_product';
            $where = 'pid IN (' . $indexerConfig['sysfolder'] . ') AND hidden = 0 AND deleted = 0';

            /** @var ConnectionPool $connectionPool */
            $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
            $queryBuilder = $connectionPool->getQueryBuilderForTable($table);
            $queryBuilder->select($fields)->from($table)->where($where);
            /** @var Statement */
            $res = $queryBuilder->execute();

            $resCount = $res->rowCount();

            // Loop through the records and write them to the index.
            if ($resCount) {
                while (($record = $res->fetch())) {
                    // compile the information which should go into the index
                    // the field names depend on the table you want to index!
                    $title = strip_tags($record['title'] ?? '');
                    $abstract = strip_tags($record['teaser'] ?? '');
                    $content = strip_tags($record['description'] ?? '');
                    $fullContent = $title . "\n" . $abstract . "\n" . $content;
                    $params = '&tx_shop_products[product]=' . $record['uid'] . '&tx_shop_products[action]=show';
                    $tags = '';
                    $additionalFields = [
                        'sortdate' => $record['crdate'],
                        'orig_uid' => $record['uid'],
                        'orig_pid' => $record['pid'],
                    ];
                    // add something to the title, just to identify the entries
                    // in the frontend
                    $translationKey = 'tx_shop_label.ke_search_result_item_prefix';
                    $resultItemPrefix = LocalizationUtility::translate($translationKey, 'Shop');
                    // ... and store the information in the index
                    /** @phpstan-ignore-next-line */
                    $indexerObject->storeInIndex(
                        $indexerConfig['storagepid'], // storage PID
                        $resultItemPrefix . $title, // record title
                        'shop', // content type
                        $indexerConfig['targetpid'], // target PID: where is the single view?
                        $fullContent, // indexed content, includes the title (linebreak after title)
                        $tags, // tags for faceted search
                        $params, // typolink params for singleview
                        $abstract, // abstract; shown in result list if not empty
                        $record['sys_language_uid'], // language uid
                        $record['starttime'], // starttime
                        $record['endtime'], // endtime
                        $record['fe_group'], // fe_group
                        false, // debug only?
                        $additionalFields // additionalFields
                    );
                }
                $content = '<p><b>Shop Indexer: ' . $resCount . ' Products has been indexed by "' . $indexerConfig['title'] . '".<b></p>';
            }
        }
        return $content;
    }
}
