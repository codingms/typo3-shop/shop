<?php

declare(strict_types=1);

namespace CodingMs\Shop\Hooks;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\Shop\Domain\Model\Basket;
use CodingMs\Shop\Domain\Repository\BasketRepository;
use CodingMs\Shop\Service\BasketOrderService;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\DataHandling\DataHandler;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager;

class TCEmainHook
{
    /**
     * Initialize object
     */
    protected function initialize(): void
    {
        // Clear state of extbase persistence to fetch objects freshly from the database
        /** @var PersistenceManager $persistenceManager */
        $persistenceManager = GeneralUtility::makeInstance(PersistenceManager::class);
        $persistenceManager->persistAll();
        $persistenceManager->clearState();
    }

    /** @phpstan-ignore-next-line */
    public function processCmdmap_preProcess($command, $table, $id, $value, DataHandler &$pObj)
    {
    }

    /** @phpstan-ignore-next-line */
    public function processCmdmap_postProcess($command, $table, $id, $value, DataHandler &$pObj)
    {
    }

    /** @phpstan-ignore-next-line */
    public function processDatamap_preProcessFieldArray(array &$fieldArray, $table, $id, DataHandler &$pObj)
    {
    }

    /** @phpstan-ignore-next-line */
    public function processCmdmap_deleteAction($table, $id, $recordToDelete, $recordWasDeleted, DataHandler &$pObj)
    {
    }

    /** @phpstan-ignore-next-line */
    public function processDatamap_afterAllOperations(DataHandler &$pObj)
    {
        $this->initialize();
        $isReturnType = false;
        if (is_array($pObj->datamap)) {
            foreach ($pObj->datamap as $key => $datamapArray) {
                if ($key == 'tx_shop_domain_model_basketorder') {
                    foreach ($datamapArray as $id => $fieldArray) {
                        if (is_string($id)) {
                            $id = $pObj->substNEWwithIDs[$id];
                        }
                        $basketOrder = BackendUtility::getRecord('tx_shop_domain_model_basketorder', $id);
                        if ($basketOrder['type'] === 'return') {
                            $isReturnType = true;
                        }
                    }
                }
                if ($key == 'tx_shop_domain_model_basket' && $isReturnType) {
                    foreach ($datamapArray as $id => $fieldArray) {
                        // Save basket items to data array in basket from basketorder
                        $basketRepository = GeneralUtility::makeInstance(BasketRepository::class);
                        if (is_string($id)) {
                            $id = $pObj->substNEWwithIDs[$id];
                        }
                        /** @var Basket $basket */
                        $basket = $basketRepository->findByIdentifier($id);
                        //
                        // Transfer whole order data into JSON string then persist to db
                        $basket->setData($this->jsonEncode($basket->toArray()));
                        $basketRepository->update($basket);
                        /** @var PersistenceManager $persistenceManager */
                        $persistenceManager = GeneralUtility::makeInstance(PersistenceManager::class);
                        $persistenceManager->persistAll();
                    }
                }
            }
        }
    }

    /** @phpstan-ignore-next-line */
    public function processDatamap_postProcessFieldArray($status, $table, $id, array $fieldArray, DataHandler &$pObj)
    {
    }

    /** @phpstan-ignore-next-line */
    public function processDatamap_afterDatabaseOperations($status, $table, $id, array $fieldArray, DataHandler &$pObj)
    {
        $this->initialize();
        if ($table === 'tx_shop_domain_model_basketorder') {
            /** @var BasketOrderService $basketOrderService*/
            $basketOrderService = GeneralUtility::makeInstance(BasketOrderService::class);
            if (is_string($id)) {
                $uid = $pObj->substNEWwithIDs[$id];
            } else {
                $uid = $id;
            }
            $basketOrderService->getTotalSum($uid, $pObj);
        }
    }

    /**
     * Wrapper method for error handling
     *
     * @param array $arguments
     * @param bool $utf8Fix
     * @return string
     */
    protected function jsonEncode(array $arguments, bool $utf8Fix = false): string
    {
        if ($utf8Fix) {
            foreach ($arguments as $key => $value) {
                $arguments[$key] = mb_convert_encoding($value, 'UTF-8', 'ISO-8859-1');
            }
        }
        $result = (string)json_encode($arguments, JSON_PRETTY_PRINT);
        if (json_last_error() > 0) {
            $message = 'jsonEncode failed with error: ';
            $message .= json_last_error() . ' - ' . json_last_error_msg();
            $this->log($message);
        }
        return $result;
    }
}
