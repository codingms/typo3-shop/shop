<?php

declare(strict_types=1);

namespace CodingMs\Shop\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\Shop\Domain\Model\Product;
use CodingMs\Shop\Domain\Repository\ProductCategoryRepository;
use CodingMs\Shop\Domain\Repository\ProductRepository;
use CodingMs\Shop\Domain\Repository\ProductTagCategoryRepository;
use CodingMs\Shop\Domain\Repository\ProductTagRepository;
use CodingMs\Shop\Domain\Session\SessionHandler;
use CodingMs\Shop\Service\BasketService;
use Psr\Http\Message\ResponseInterface;
use TYPO3\CMS\Core\Messaging\AbstractMessage;

/**
 * Basket controller
 */
class BasketController extends BaseController
{
    /**
     * Basket (in this case a session based array)
     * @var array
     */
    protected $basket;
    protected BasketService $basketService;

    public function __construct(
        SessionHandler $sessionHandler,
        ProductRepository $productRepository,
        ProductTagRepository $productTagRepository,
        ProductTagCategoryRepository $productTagCategoryRepository,
        ProductCategoryRepository $productCategoryRepository,
        BasketService $basketService
    ) {
        parent::__construct(
            $sessionHandler,
            $productRepository,
            $productTagRepository,
            $productTagCategoryRepository,
            $productCategoryRepository
        );
        $this->basketService = $basketService;
    }

    /**
     * Initialize actions
     */
    public function initializeAction(): void
    {
        // Restore basket data
        $this->session = $this->sessionHandler->restoreFromSession();
        // Is there already a basket?
        if (isset($this->session['basket']) && !empty($this->session['basket'])) {
            $this->basket = $this->session['basket'];
        } else {
            // Initialize empty basket
            $this->basket = [];
            // and write to session
            $this->session['basket'] = $this->basket;
            $this->sessionHandler->writeToSession($this->session);
        }
    }

    /**
     * Display basket overview
     *
     * @return ResponseInterface
     */
    public function showAction(): ResponseInterface
    {
        if (isset($this->session['checkout']['vatOverlayCountryCode'])) {
            unset($this->session['checkout']['vatOverlayCountryCode']);
            $this->sessionHandler->writeToSession($this->session);
        }
        $basket = $this->basketService->getBasketObject($this->basket, $this->settings);
        $this->view->assign('basket', $basket);
        $this->view->assign('settings', $this->settings);
        return $this->htmlResponse();
    }

    protected function getBasketObjectCount(): int
    {
        // Refresh basket object
        // to ensure that the quantity is correct
        $basketObject = $this->basketService->getBasketObject($this->basket, $this->settings);
        return $basketObject->getBasketItemCount();
    }

    public function showBasketButtonAction(): ResponseInterface
    {
        $this->view->assign('basketPid', $this->settings['basket']['pid']);
        $this->view->assign('basketItemCount', $this->getBasketObjectCount());
        return $this->htmlResponse();
    }

    /**
     * Remove a product from basket
     *
     * @return ResponseInterface
     */
    public function removeFromBasketAction(): ResponseInterface
    {
        //
        // Fetch product object
        $product = null;
        if ($this->request->hasArgument('product')) {
            $productUid = (int)$this->request->getArgument('product');
            if ($productUid > 0) {
                $product = $this->productRepository->findByIdentifier($productUid);
            }
        }
        //
        // Ensure the product exists
        if ($product instanceof Product) {
            // Can only remove, if in basket
            if (isset($this->basket[$product->getUid()])) {
                unset($this->basket[$product->getUid()]);
            }
            // Write back into session
            $this->session['basket'] = $this->basket;
            $this->sessionHandler->writeToSession($this->session);
        } else {
            $this->addFlashMessage(
                $this->translate('tx_shop_message.error_product_not_found'),
                $this->translate('tx_shop_message.error_title'),
                AbstractMessage::ERROR
            );
        }
        return $this->redirect('show', 'Basket', null, []);
    }
}
