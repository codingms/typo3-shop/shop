<?php

declare(strict_types=1);

namespace CodingMs\Shop\ViewHelpers\VariantFilter;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use Closure;
use CodingMs\Shop\Domain\Model\Product;
use CodingMs\Shop\Domain\Repository\ProductRepository;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithRenderStatic;
use TYPO3Fluid\Fluid\Core\ViewHelper\ViewHelperInterface as CompilableInterface;

/**
 * Class GenericMatrixViewHelper
 * @noinspection PhpUnused
 */
class GenericMatrixViewHelper extends AbstractViewHelper implements CompilableInterface
{
    use CompileWithRenderStatic;

    protected ProductRepository $productRepository;

    /**
     * @param ProductRepository $productRepository
     */
    public function __construct(
        ProductRepository $productRepository
    )
    {
        $this->productRepository = $productRepository;
    }

    public function initializeArguments(): void
    {
        $this->registerArgument('product', 'int', 'Uid of the current product', true);
        $this->registerArgument('fieldX', 'string', 'Field X for the filter: color, size, title', true);
        $this->registerArgument('fieldY', 'string', 'Field Y for the filter: color, size, title', true);
        $this->registerArgument('sortingX', 'string', 'Optional sorting for X: none, asc, desc', false, 'asc');
        $this->registerArgument('sortingY', 'string', 'Optional sorting for Y: none, asc, desc', false, 'asc');
    }

    /**
     * @param array $arguments
     * @param Closure $renderChildrenClosure
     * @param RenderingContextInterface $renderingContext
     * @return mixed
     */
    public static function renderStatic(array $arguments, Closure $renderChildrenClosure, RenderingContextInterface $renderingContext)
    {
        $filter = [
            'items' => [],
            'itemsFlipped' => [],
        ];
        $fieldX = $arguments['fieldX'];
        $fieldY = $arguments['fieldY'];
        $sortingX = $arguments['sortingX'];
        $sortingY = $arguments['sortingY'];
        //
        // read configured sorting from extension configuration
        $sortingFieldX = 'title';
        $sortingFieldY = 'title';
        $backendConfiguration = GeneralUtility::makeInstance(ExtensionConfiguration::class)
            ->get('shop', 'sorting');
        if (isset($backendConfiguration['orderingFor' . ucfirst($fieldX)])) {
            if ($backendConfiguration['orderingFor' . ucfirst($fieldX)] === 'sorting') {
                $sortingFieldX = 'sorting';
            }
        }
        if (isset($backendConfiguration['orderingFor' . ucfirst($fieldY)])) {
            if ($backendConfiguration['orderingFor' . ucfirst($fieldY)] === 'sorting') {
                $sortingFieldY = 'sorting';
            }
        }
        //
        $labelsX = [];
        $labelsY = [];
        $productUid = (int)$arguments['product'];
        $productRepository = GeneralUtility::makeInstance(ProductRepository::class);
        /** @var Product $product */
        $product = $productRepository->findByIdentifier($productUid);
        if ($product instanceof Product) {
            $labelsX = self::getPropertyKeyValue($product, $fieldX, $labelsX, $sortingFieldX);
            $labelsY = self::getPropertyKeyValue($product, $fieldY, $labelsY, $sortingFieldY);
            foreach ($product->getVariants() as $variant) {
                $labelsX = self::getPropertyKeyValue($variant, $fieldX, $labelsX, $sortingFieldX);
                $labelsY = self::getPropertyKeyValue($variant, $fieldY, $labelsY, $sortingFieldY);
            }
        }
        //
        // Sorting of matrix labels
        switch ($sortingX) {
            case 'asc':
                ksort($labelsX);
                break;
            case 'desc':
                krsort($labelsX);
                break;
        }
        switch ($sortingY) {
            case 'asc':
                ksort($labelsY);
                break;
            case 'desc':
                krsort($labelsY);
                break;
        }
        $filter['labelsX'] = $labelsX;
        $filter['labelsY'] = $labelsY;
        foreach ($filter['labelsX'] as $x => $xValue) {
            foreach ($filter['labelsY'] as $y => $yValue) {
                if (!isset($filter['items'][$x])) {
                    $filter['items'][$x] = [
                        'label' => $xValue->getTitle() ?? 'xValue->getTitle not found on object',
                        'object' => $xValue,
                        'items' => [],
                    ];
                }
                $filter['items'][$x]['items'][$y] = [
                    'label' => 0,
                    'product' => null,
                    'active' => false,
                ];
                if (!isset($filter['itemsFlipped'][$y])) {
                    $filter['itemsFlipped'][$y] = [
                        'label' => $yValue->getTitle() ?? 'yValue->getTitle not found on object',
                        'object' => $yValue,
                        'items' => [],
                    ];
                }
                $filter['itemsFlipped'][$y]['items'][$x] = [
                    'label' => 0,
                    'product' => null,
                    'active' => false,
                ];
            }
        }
        foreach ($filter['labelsX'] as $x => $xValue) {
            foreach ($filter['labelsY'] as $y => $yValue) {
                if (is_object($yValue) && method_exists($yValue, 'getTitle')) {
                    $filter['labelsY'][$y] = $yValue->getTitle();
                }
            }
            $filter['labelsX'][$x] = $xValue->getTitle() ?? 'xValue->getTitle not found on object';
        }
        //
        // Insert all product variants
        if ($product instanceof Product) {
            $indexX = self::getPropertyTitle($product, $fieldX);
            if ($sortingFieldX === 'sorting') {
                $indexX = self::getPropertySorting($product, $fieldX);
            }
            $indexY = self::getPropertyTitle($product, $fieldY);
            if ($sortingFieldY === 'sorting') {
                $indexY = self::getPropertySorting($product, $fieldY);
            }
            $filter['items'][$indexX]['items'][$indexY] = [
                'label' => $product->getStockAmount(),
                'product' => $product,
                'active' => ($product->getUid() === $productUid),
                'fieldY' => $product->_getProperty($fieldY),
                'fieldX' => $product->_getProperty($fieldX),
            ];
            $filter['itemsFlipped'][$indexY]['items'][$indexX] = [
                'label' => $product->getStockAmount(),
                'product' => $product,
                'active' => ($product->getUid() === $productUid),
                'fieldY' => $product->_getProperty($fieldY),
                'fieldX' => $product->_getProperty($fieldX),
            ];
            foreach ($product->getVariants() as $variant) {
                $indexX = self::getPropertyTitle($variant, $fieldX);
                if ($sortingFieldX === 'sorting') {
                    $indexX = self::getPropertySorting($variant, $fieldX);
                }
                $indexY = self::getPropertyTitle($variant, $fieldY);
                if ($sortingFieldY === 'sorting') {
                    $indexY = self::getPropertySorting($variant, $fieldY);
                }
                $filter['items'][$indexX]['items'][$indexY] = [
                    'label' => $variant->getStockAmount(),
                    'product' => $variant,
                    'active' => ($variant->getUid() === $productUid),
                    'fieldY' => $variant->_getProperty($fieldY),
                    'fieldX' => $variant->_getProperty($fieldX),
                ];
                $filter['itemsFlipped'][$indexY]['items'][$indexX] = [
                    'label' => $variant->getStockAmount(),
                    'product' => $variant,
                    'active' => ($variant->getUid() === $productUid),
                    'fieldY' => $variant->_getProperty($fieldY),
                    'fieldX' => $variant->_getProperty($fieldX),
                ];
            }
        }
        return $filter;
    }

    /**
     * @param Product $product
     * @param string $field
     * @param array<mixed> $labelsX
     * @param string $sortingField
     * @return array<mixed>
     */
    protected static function getPropertyKeyValue(Product $product, string $field, array $labelsX, string $sortingField = 'title'): array
    {
        $indexY = self::getPropertyTitle($product, $field);
        if ($sortingField === 'sorting') {
            $indexY = self::getPropertySorting($product, $field);
        }
        $labelsX[$indexY] = $product->_getProperty($field);
        return $labelsX;
    }

    protected static function getPropertyTitle(Product $product, string $field): string
    {
        $productField = $product->_getProperty($field);
        if (is_object($productField)) {
            $title = $productField->getTitle() ?? 'getTitle not found on object';
        } else {
            $title = $productField;
        }
        return $title;
    }

    protected static function getPropertySorting(Product $product, string $field): int
    {
        $sorting = 0;
        $productField = $product->_getProperty($field);
        if (is_object($productField)) {
            $sorting = $productField->getSorting() ?? 0;
        }
        return $sorting;
    }
}
