<?php

declare(strict_types=1);

namespace CodingMs\Shop\Updates;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2024 Roman Derlemenko <typo3@coding.ms>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use Throwable;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Install\Updates\UpgradeWizardInterface;

class MigrateProductShippingCostCountryOverlayWizard implements UpgradeWizardInterface
{
    /**
     * Return the identifier for this wizard
     * This should be the same string as used in the ext_localconf class registration
     *
     * @return string
     */
    public function getIdentifier(): string
    {
        return 'shop_migrateProductShippingCostCountryOverlayWizard';
    }

    /**
     * Return the speaking name of this wizard
     *
     * @return string
     */
    public function getTitle(): string
    {
        return 'EXT:shop: Migrates product shipping cost country overlay from single to multiple relation';
    }

    /**
     * Return the description for this wizard
     *
     * @return string
     */
    public function getDescription(): string
    {
        return 'Searches for product shipping costs with a single country overlay and migrates them to multiple country overlays';
    }

    /**
     * Execute the update - update sizes set to 0 or '' then update sizes set to strings
     *
     * Called when a wizard reports that an update is necessary
     *
     * @return bool
     */
    public function executeUpdate(): bool
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getQueryBuilderForTable('tx_shop_domain_model_productshippingcostcountryoverlay');
        $result = $queryBuilder
            ->select('*')
            ->from('tx_shop_domain_model_productshippingcostcountryoverlay')
            ->where(
                $queryBuilder->expr()->notLike(
                    'country_code',
                    $queryBuilder->createNamedParameter('%,%')
                )
            )
            ->executeQuery();
        $grouped = [];
        foreach ($result->fetchAllAssociative() as $record) {
            $pattern = $record['default_shipping_cost'] . ':' . $record['price'] . ':' . $record['bulky'];
            $grouped[$pattern][] = $record;
        }
        foreach ($grouped as $records) {
            if (count($records) > 1) {
                $uidsToDelete = array_unique(array_map(fn($record) => $record['uid'], $records));
                $countryCodes = array_unique(array_map(fn($record) => $record['country_code'], $records));
                $pid = $records[0]['pid'];
                $price = $records[0]['price'];
                $bulky = $records[0]['bulky'];
                $defaultShippingCost = $records[0]['default_shipping_cost'];
                $time = time();
                /** @var ConnectionPool $connectionPool */
                $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
                $connection = $connectionPool->getConnectionForTable('tx_shop_domain_model_productshippingcostcountryoverlay');
                try {
                    $connection->beginTransaction();
                    $connection->insert('tx_shop_domain_model_productshippingcostcountryoverlay', [
                        'pid' => $pid,
                        'tstamp' => $time,
                        'crdate' => $time,
                        'country_code' => implode(',', $countryCodes),
                        'price' => $price,
                        'bulky' => $bulky,
                        'default_shipping_cost' => $defaultShippingCost,
                    ]);
                    foreach ($uidsToDelete as $uid) {
                        $connection->update('tx_shop_domain_model_productshippingcostcountryoverlay', ['deleted' => 1], ['uid' => $uid]);
                    }
                    $connection->commit();
                } catch (Throwable) {
                    $connection->rollBack();
                    continue;
                }
            }
        }
        return true;
    }

    /**
     * Is an update necessary?
     *
     * Is used to determine whether a wizard needs to be run.
     * Check if data for migration exists.
     *
     * @return bool
     */
    public function updateNecessary(): bool
    {
        return true;
    }

    /**
     * Returns an array of class names of prerequisite classes
     *
     * This way a wizard can define dependencies like "database up-to-date" or
     * "reference index updated"
     *
     * @return string[]
     */
    public function getPrerequisites(): array
    {
        return [];
    }
}
