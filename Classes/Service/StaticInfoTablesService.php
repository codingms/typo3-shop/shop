<?php

namespace CodingMs\Shop\Service;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2024 Roman Derlemenko <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use SJBR\StaticInfoTables\Domain\Model\Country;
use SJBR\StaticInfoTables\Domain\Repository\CountryRepository;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class StaticInfoTablesService
 */
class StaticInfoTablesService implements SingletonInterface
{
    /**
     * @var array<int, Country>|null
     */
    protected static ?array $countries = null;

    /**
     * @return array<int, Country>
     */
    public static function getCountries(): array
    {
        if (!self::$countries) {
            self::fetchCountries();
        }
        return self::$countries;
    }

    /**
     * @param string|int|null $country
     * @return string|null
     */
    public static function getCountryIsoCodeA2(string|int|null $country): ?string
    {
        if (!$country) {
            return null;
        }

        if (is_int($country) || ctype_digit($country)) {
            if ($object = self::getCountries()[$country]) {
                return strtoupper($object->getIsoCodeA2());
            }
        }

        if (strlen($country) === 2) {
            return strtoupper($country);
        }

        if (strlen($country) === 3) {
            foreach (self::getCountries() as $object) {
                if (strtoupper($object->getIsoCodeA3()) === strtoupper($country)) {
                    return strtoupper($object->getIsoCodeA2());
                }
            }
        }

        return null;
    }

    /**
     * @return void
     */
    protected static function fetchCountries(): void
    {
        /** @var CountryRepository $repository */
        $repository = GeneralUtility::makeInstance(CountryRepository::class);
        self::$countries = [];
        foreach ($repository->findAll() as $country) {
            self::$countries[$country->getUid()] = $country;
        }
    }
}
