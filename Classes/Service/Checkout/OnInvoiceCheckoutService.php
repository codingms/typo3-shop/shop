<?php

declare(strict_types=1);

namespace CodingMs\Shop\Service\Checkout;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Thomas Deuling <typo3@coding.ms>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\Shop\Domain\Model\BasketOrder;
use CodingMs\Shop\Event\BasketOrder\MailAttachmentEvent;
use DateTime;
use TYPO3\CMS\Core\EventDispatcher\EventDispatcher;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException;
use TYPO3\CMS\Extbase\Persistence\Exception\UnknownObjectException;

/**
 * OnInvoice checkout
 *
 * @author Thomas Deuling <typo3@coding.ms>
 */
class OnInvoiceCheckoutService extends AbstractCheckoutService
{
    const CUSTOMER_CONFIRMATION_EMAIL_TEMPLATE = 'Checkout/OnInvoice/CustomerConfirmation';
    const ORDER_EMAIL_TEMPLATE = 'Checkout/OnInvoice/Order';

    /**
     * Finish the on-invoice checkout.
     *
     * We only have to send the emails.
     *
     * @param array $configuration
     * @param BasketOrder $basketOrder
     * @param array $settings
     * @return bool
     * @throws IllegalObjectTypeException
     * @throws UnknownObjectException
     */
    public function finishOrder(array $configuration, BasketOrder $basketOrder, array $settings): bool
    {
        $basketOrder->setOrdered(true);
        $basketOrder->setOrderDate(new DateTime());
        $basketOrder->setInvoiceNumber($this->invoiceService->getNextInvoiceNumber((int)$settings['storagePid']));
        $this->basketOrderRepository->update($basketOrder);
        $this->persistenceManager->persistAll();
        //
        // Create required attachments
        $mailAttachmentEvent = new MailAttachmentEvent(
            $basketOrder,
            $configuration,
            $settings
        );
        $eventDispatcher = GeneralUtility::makeInstance(EventDispatcher::class);
        $eventDispatcher->dispatch($mailAttachmentEvent);
        //
        // Fetch required attachments
        $adminAttachment = $mailAttachmentEvent->getAttachmentsAdmin();
        $customerAttachment = $mailAttachmentEvent->getAttachmentsCustomer();
        //
        // Prepare variables for email rendering
        $assign = [
            'configuration' => $configuration,
            'basketOrder' => $basketOrder,
            'settings' => $settings,
        ];
        //
        // Send customer confirmation mail
        $mailSentCustomerConfirmation = true;
        $format = $settings['email']['format'];
        if ((bool)$configuration['email']['customerConfirmation']['active'] ?? false) {
            $mailConfiguration = $configuration['email']['customerConfirmation'];
            $mailSentCustomerConfirmation = $this->sendMail(
                $mailConfiguration['from']['email'],
                $mailConfiguration['from']['name'],
                $mailConfiguration['to']['email'],
                $mailConfiguration['to']['name'],
                $mailConfiguration['subject'],
                self::CUSTOMER_CONFIRMATION_EMAIL_TEMPLATE,
                $assign,
                $format,
                $customerAttachment,
                $mailConfiguration['cc']['email'] ?? '',
                $mailConfiguration['cc']['name'] ?? ''
            );
        }
        //
        // Send order mail
        $mailSentOrder = true;
        if ($configuration['email']['order']['active'] ?? false) {
            $mailConfiguration = $configuration['email']['order'];
            $mailSentOrder = $this->sendMail(
                $mailConfiguration['from']['email'],
                $mailConfiguration['from']['name'],
                $mailConfiguration['to']['email'],
                $mailConfiguration['to']['name'],
                $mailConfiguration['subject'],
                self::ORDER_EMAIL_TEMPLATE,
                $assign,
                $format,
                $adminAttachment,
                $mailConfiguration['cc']['email'] ?? '',
                $mailConfiguration['cc']['name'] ?? ''
            );
        }
        //
        // Save email log and set new state
        $basketOrder->setEmailCopy((string)json_encode($this->log, JSON_PRETTY_PRINT));
        $basketOrder->setStatus('ordered');
        $this->basketOrderRepository->update($basketOrder);
        $this->persistenceManager->persistAll();
        //
        return $mailSentCustomerConfirmation && $mailSentOrder;
    }
}
