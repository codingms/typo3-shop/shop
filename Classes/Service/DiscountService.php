<?php

declare(strict_types=1);

namespace CodingMs\Shop\Service;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2024 Florentin Bauhaus <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\Shop\Domain\Model\BasketItem;
use CodingMs\Shop\Domain\Model\ProductDiscount;
use CodingMs\Shop\Domain\Repository\ProductDiscountRepository;
use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;

class DiscountService
{
    protected ProductDiscountRepository $productDiscountRepository;

    /**
     * @var QueryResultInterface<int, ProductDiscount>
     */
    protected QueryResultInterface $discountsForAll;

    /**
     * @var array<string, QueryResultInterface<int, ProductDiscount>> mapping checkout type to a QueryResultInterface
     */
    protected array $discountsByType;

    /**
     * @param ProductDiscountRepository $productDiscountRepository
     */
    public function __construct(ProductDiscountRepository $productDiscountRepository)
    {
        $this->productDiscountRepository = $productDiscountRepository;
        $this->discountsForAll = $this->productDiscountRepository->findByOrderOption('all');
    }


    public function applyDiscount(BasketItem $item, ?ProductDiscount $discount, ?string $checkoutType = null): BasketItem
    {
        $finalDiscount = $this->findMatchingDiscount($discount, $checkoutType);
        if (isset($finalDiscount) && !$item->getIsDiscounted()) {
            $item = $this->applyDiscountToBasketItem($item, $finalDiscount);
        }
        return $item;
    }

    public function findMatchingDiscount(?ProductDiscount $discount, ?string $checkoutType = null): ?ProductDiscount
    {
        $result = null;
        if (isset($checkoutType)) {
            $this->discountsByType[$checkoutType] = $this->productDiscountRepository->findByOrderOption($checkoutType);
        }

        if ($discount instanceof ProductDiscount) {
            $result = $discount;
        } else {
            //
            // Process the discount for selected order option
            foreach ($this->discountsByType[$checkoutType] ?? [] as $discount) {
                $result = $discount;
            }
            //
            // Process the discount for all order options
            foreach ($this->discountsForAll as $discount) {
                $result = $discount;
            }
        }
        return $result;
    }

    private function applyDiscountToBasketItem(BasketItem $item, ProductDiscount $discount): BasketItem
    {
        $item->setIsDiscounted(true);
        $item->setDiscountType($discount->getType());
        $item->setDiscountLabel($discount->getLabelPublic());
        $item->setDiscountValue($discount->getValue());
        $item->setDiscount($this->calculateDiscountAmount($discount, $item->getPrice()));
        return $item;
    }

    public function calculateDiscountAmount(ProductDiscount $discount, int $price): int
    {
        if ($discount->getType() === 'percent') {
            return intval(($price / 100) * ($discount->getValuePercent() / 100));
        } else {
            return $discount->getValueFixed();
        }
    }
}
