<?php

namespace CodingMs\Shop\Service;

use Doctrine\DBAL\DBALException;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\DataHandling\DataHandler;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager;

class BasketOrderService
{
    /**
     * Calculate and update total sum for a specific basket order in the database.
     *
     * @param int $basketOrderUid The ID of the basket order.
     * @param DataHandler $pObj The DataHandler object containing relevant data.
     * @throws DBALException
     */
    public function getTotalSum(int $basketOrderUid, DataHandler $pObj): void
    {

        $basketOrder = BackendUtility::getRecord('tx_shop_domain_model_basketorder', $basketOrderUid);

        if (isset($basketOrder) && ($basketOrder['type'] === 'return' || $basketOrder['type'] === 'manual')) {

            [$price, $tax, $priceWithTax] = $this->sumUpItemsValues($pObj);
            $this->updateBasketOrder($basketOrderUid, $price, $tax, $priceWithTax);

            if ($basketOrder['type'] === 'manual') {
                return;
            }
            // Set order_date to same as parent basket order
            $parentBasketOrder = BackendUtility::getRecord(
                'tx_shop_domain_model_basketorder',
                (int)str_replace('tx_shop_domain_model_basketorder_', '', (string)$basketOrder['parent'])
            );
            if (!isset($parentBasketOrder)) {
                return;
            }

            /** @var ConnectionPool $connectionPool */
            $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
            $queryBuilder = $connectionPool->getQueryBuilderForTable('tx_shop_domain_model_basketorder');
            $queryBuilder->update('tx_shop_domain_model_basketorder')
                ->where(
                    $queryBuilder->expr()->eq('uid',
                        $queryBuilder->createNamedParameter($basketOrderUid, Connection::PARAM_INT)
                    ))
                ->set('order_date', $parentBasketOrder['order_date'])
                ->execute();
        }
    }

    /**
     * Sums up the prices, taxes, and prices with tax of basket items in the data handler.
     *
     * @param DataHandler $pObj The DataHandler object containing basket item data
     * @return array An array containing the total price, total tax, and total price with tax
     */
    private function sumUpItemsValues(DataHandler $pObj)
    {
        // Add all (IRRE) basket item prices/taxes together and put in basketorder record
        $price = 0;
        $tax = 0;
        $priceWithTax = 0;
        if (is_array($pObj->datamap)) {
            foreach ($pObj->datamap as $key => $datamapArray) {
                if ($key == 'tx_shop_domain_model_basketitem') {
                    foreach ($datamapArray as $fieldArray) {
                        $price += $fieldArray['price'];
                        // 10000 as it must be percentage (/100) for cents (/100)
                        $tax += ($fieldArray['tax'] / 10000) * $fieldArray['price'];
                        $priceWithTax += $fieldArray['price_with_tax'];
                    }
                }
            }
        }
        return [$price, $tax, $priceWithTax];
    }

    /**
     * Update the total sum values for a specific basket order in the database.
     *
     * @param int $basketOrderUid The UID of the basket order to update.
     * @param int $price The total price value to update for the basket order.
     * @param int $tax The total tax value to update for the basket order.
     * @param int $priceWithTax The total price including tax value to update for the basket order.
     */
    private function updateBasketOrder(int $basketOrderUid, int $price, int $tax, $priceWithTax)
    {
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $queryBuilder = $connectionPool->getQueryBuilderForTable('tx_shop_domain_model_basketorder');
        $queryBuilder->update('tx_shop_domain_model_basketorder')
            ->where(
                $queryBuilder->expr()->eq('uid',
                    $queryBuilder->createNamedParameter($basketOrderUid, Connection::PARAM_INT)
                ))
            ->set('price', $price)
            ->set('tax', $tax)
            ->set('price_with_tax', $priceWithTax)
            ->execute();
    }
}

