<?php

declare(strict_types=1);

namespace CodingMs\Shop\Event\BasketOrder;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\Shop\Domain\Model\BasketOrder;

/**
 * This event collects attachments for order mails.
 */
class MailAttachmentEvent
{
    protected BasketOrder $basketOrder;
    protected array $settings;
    protected array $configuration;
    protected array $attachmentsAdmin = [];
    protected array $attachmentsCustomer = [];

    public function __construct(
        BasketOrder $basketOrder,
        array $configuration,
        array $settings
    ) {
        $this->basketOrder = $basketOrder;
        $this->configuration = $configuration;
        $this->settings = $settings;
    }

    public function getBasketOrder(): BasketOrder
    {
        return $this->basketOrder;
    }

    public function getSettings(): array
    {
        return $this->settings;
    }

    public function getConfiguration(): array
    {
        return $this->configuration;
    }

    public function setConfiguration(array $configuration): void
    {
        $this->configuration = $configuration;
    }

    public function addAttachmentsAdmin(array $attachment): void
    {
        $this->attachmentsAdmin[] = $attachment;
    }

    public function getAttachmentsAdmin(): array
    {
        return $this->attachmentsAdmin;
    }

    public function addAttachmentsCustomer(array $attachment): void
    {
        $this->attachmentsCustomer[] = $attachment;
    }

    public function getAttachmentsCustomer(): array
    {
        return $this->attachmentsCustomer;
    }
}
