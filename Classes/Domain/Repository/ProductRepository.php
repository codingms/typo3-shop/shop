<?php

declare(strict_types=1);

namespace CodingMs\Shop\Domain\Repository;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\Shop\Domain\Model\Product;
use CodingMs\Shop\Domain\Model\ProductCategory;
use CodingMs\Shop\Domain\Model\ProductTag;
use Doctrine\DBAL\Query\QueryBuilder;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException;
use TYPO3\CMS\Extbase\Persistence\Generic\Mapper\DataMapper;
use TYPO3\CMS\Extbase\Persistence\QueryInterface;
use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;
use TYPO3\CMS\Extbase\Persistence\Repository;

/**
 * Product repository
 */
class ProductRepository extends Repository
{

    /**
     * Get the data mapper instance for this class.
     *
     * @return DataMapper The data mapper instance.
     */
    protected function getDataMapper(): DataMapper
    {
        /** @var DataMapper $dataMapper */
        $dataMapper = GeneralUtility::makeInstance(DataMapper::class);
        return $dataMapper;
    }

    /**
     * Select or count products by settings
     * @param array $settings
     * @param bool $count
     * @param bool $includingVariants
     * @return array|QueryResultInterface|int
     * @throws InvalidQueryException
     */
    public function findAllByFilter(array $settings, bool $count = false, bool $includingVariants = false)
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_shop_domain_model_product');
        [$constraints, $havingConditions] = $this->getConstraints($queryBuilder, $settings, $includingVariants);
        $query = $this->createQuery();
        $querySettings = $query->getQuerySettings();
        $languageUid = $querySettings->getLanguageUid();
        $constraints[] = $queryBuilder->expr()->eq('product.sys_language_uid', $queryBuilder->createNamedParameter($languageUid));

        $queryBuilder
            ->select('product.*')
            ->from('tx_shop_domain_model_product', 'product')
            ->leftJoin('product', 'tx_shop_product_productcategory_mm', 'product_category', 'product.uid = product_category.uid_local')
            ->leftJoin('product_category', 'tx_shop_domain_model_productcategory', 'category', 'product_category.uid_foreign = category.uid')
            ->leftJoin('product', 'tx_shop_product_producttag_mm', 'product_tag', 'product.uid = product_tag.uid_local')
            ->leftJoin('product_tag', 'tx_shop_domain_model_producttag', 'tag', 'product_tag.uid_foreign = tag.uid')
            ->where(
                ...$constraints
            )->groupBy('uid');

        if ($havingConditions) {
            $queryBuilder->having(...$havingConditions);
        }

        // Limit and offset
        if ($settings['listLimit'] > 0 && !$count) {
            $queryBuilder->setMaxResults($settings['listLimit']);
        }
        if ($settings['listOffset'] > 0 && !$count) {
            $queryBuilder->setFirstResult($settings['listOffset']);
        }

        // Sorting
        if ($settings['sortBy'] == '') {
            $settings['sortBy'] = 'product.uid';
        }

        if (is_string($settings['sortBy'])) {
            $queryBuilder->orderBy('first_in_list', QueryInterface::ORDER_DESCENDING);

            if ($settings['sortOrder'] === 'asc') {
                $queryBuilder->addOrderBy('product.'. $settings['sortBy'], QueryInterface::ORDER_ASCENDING);
            } else {
                $queryBuilder->addOrderBy('product.'. $settings['sortBy'], QueryInterface::ORDER_DESCENDING);
            }
        }

        if ($count) {
            return $queryBuilder->executeQuery()->rowCount();
        }

        return $this->getDataMapper()->map(Product::class, $queryBuilder->executeQuery()->fetchAllAssociative());
    }

    /**
     * Setup constraints for query-builder
     *
     * @param QueryBuilder $queryBuilder
     * @param array $settings
     * @param bool $includingVariants
     * @return array
     * @throws InvalidQueryException
     */
    private function getConstraints($queryBuilder, array $settings, bool $includingVariants): array
    {
        $constraints = [];
        $havingConditions = [];

        if (!$includingVariants) {
            $constraints[] = $queryBuilder->expr()->eq('parent', $queryBuilder->createNamedParameter(0));
        }
        //
        // Show only products, where the highlight flag is selected
        if ((int)$settings['showHighlightedOnly'] === 1) {
            $constraints[] = $queryBuilder->expr()->eq('highlight', $queryBuilder->createNamedParameter(1));
        }

        // Search word
        if (trim($settings['word']['value']) != '') {
            $likes = [];
            $searchWord = trim($settings['word']['value']);
            foreach ($settings['word']['fields'] as $searchField) {
                $likes[] = $queryBuilder->expr()->like('product.' . $searchField, $queryBuilder->createNamedParameter('%' . $searchWord . '%'));
            }
            // Single or multiple search fields
            if (count($likes) > 1) {
                $constraints[] = $queryBuilder->expr()->orX(...$likes);
            } else {
                $constraints[] = $likes[0];
            }
        }

        if (isset($settings['category']['value']) && $settings['category']['value'] instanceof ProductCategory) {
            // Single category
            $constraints[] = $queryBuilder->expr()->eq('category.uid', $queryBuilder->createNamedParameter($settings['category']['value']->getUid()));
        } elseif (count($settings['category']['allowed']) > 0) {
            // Multiple categories
            $constraintProductCategories = [];
            /** @var ProductCategory $constraintProductCategory */
            foreach ($settings['category']['allowed'] as $constraintProductCategory) {
                $constraintProductCategories[] = $queryBuilder->expr()->eq('category.uid', $queryBuilder->createNamedParameter($constraintProductCategory));
            }
            // Single or multiple categories!?
            if (count($constraintProductCategories) > 1) {
                $constraints[] = $queryBuilder->expr()->orX()->addMultiple($constraintProductCategories);
            } else {
                $constraints[] = $constraintProductCategories[0];
            }
        }

        if (isset($settings['tag']['value']) && $settings['tag']['value'] instanceof ProductTag) {
            // Single tag
            $constraints[] = $queryBuilder->expr()->eq('tag.uid', $queryBuilder->createNamedParameter($settings['tag']['value']->getUid()));
        } elseif (isset($settings['tag']['values']) && is_array($settings['tag']['values']) && count($settings['tag']['values']) > 0) {
            // Multiple tags
            $constraintProductTags = [];
            /** @var int $constraintProductTag */
            foreach ($settings['tag']['values'] as $constraintProductTag) {
                if ($constraintProductTag instanceof ProductTag) {
                    /** @var ProductTag $constraintProductTag */
                    $constraintProductTag = $constraintProductTag->getUid();
                }
                $constraintProductTags[$constraintProductTag] = $queryBuilder->expr()->eq(
                    'tag.uid',
                    $queryBuilder->createNamedParameter($constraintProductTag)
                );
            }
            // Single or multiple tags!?
            if (count($constraintProductTags) > 1) {

                if ($settings['tag']['concatenate'] === 'and') {
                    //
                    // Concatenate all giving tags by 'and', it ignores the tag-category!
                    foreach ($constraintProductTags as $constraintProductTag) {
                        preg_match('/:dcValue\d+/i', $constraintProductTag, $matches);
                        if (!(trim($matches[0] ?? '') === '')) {
                            $havingConditions[] = 'SUM(tag.uid IN (' . $matches[0] . ')) > 0';
                        }
                    }
                } else if ($settings['tag']['concatenate'] === 'orButCategoryAnd') {
                    $constraintProductTagsGrouped = [];
                    /** @var ProductTag $option */
                    foreach ($settings['tag']['options'] as $option) {
                        $tagUid = $option->getUid();
                        $tagCategoryUid = $option->getCategory()->getUid();
                        if (isset($constraintProductTags[$tagUid])) {
                            if (!isset($constraintProductTagsGrouped[$tagCategoryUid])) {
                                $constraintProductTagsGrouped[$tagCategoryUid] = [];
                            }
                            preg_match('/:dcValue\d+/i', $constraintProductTags[$tagUid] ?? '', $matches);
                            if (!(trim($matches[0] ?? '') === '')) {
                                $constraintProductTagsGrouped[$tagCategoryUid][] = $matches[0];
                            }
                        }
                    }
                    foreach ($constraintProductTagsGrouped as $group) {
                        $havingConditions[] = 'SUM(tag.uid IN (' . implode(', ', $group) . ')) > 0';
                    }
                } else {
                    //
                    // Concatenate all giving tags by 'or', it ignores the tag-category!
                    $constraints[] = $queryBuilder->expr()->orX()->addMultiple($constraintProductTags);
                }
            } else {
                $constraints[] = $constraintProductTags[array_key_first($constraintProductTags)];
            }
        } elseif (count($settings['tag']['allowed']) > 0) {
            // Multiple tags
            $constraintProductTags = [];
            /** @var ProductTag $constraintProductTag */
            foreach ($settings['tag']['allowed'] as $constraintProductTag) {
                $constraintProductTags[] = $queryBuilder->expr()->eq('tag.uid', $queryBuilder->createNamedParameter($constraintProductTag));
            }
            // Single or multiple tags!?
            if (count($constraintProductTags) > 1) {
                $constraints[] = $queryBuilder->expr()->orX()->addMultiple($constraintProductTags);
            } else {
                $constraints[] = $constraintProductTags[array_key_first($constraintProductTags)];
            }
        }
        return [$constraints, $havingConditions];
    }

    /**
     * @param string $searchWord
     * @param array $searchFields
     * @return array|QueryResultInterface
     * @throws InvalidQueryException
     * @noinspection PhpUnused
     */
    public function findAllBySearch(string $searchWord = '', array $searchFields = [])
    {
        $query = $this->createQuery();

        // Wenn keine Felder vorgegeben sind
        $constraints = [];
        $constraints[] = $query->equals('parent', '0');
        $constraintsOr = [];
        if (empty($searchFields) || $searchFields[0] == '') {
            $constraintsOr[] = $query->like('title', '%' . $searchWord . '%');
            $constraintsOr[] = $query->like('subtitle', '%' . $searchWord . '%');
            $constraintsOr[] = $query->like('product_no', '%' . $searchWord . '%');
            $constraintsOr[] = $query->like('description', '%' . $searchWord . '%');
            $constraintsOr[] = $query->like('www', '%' . $searchWord . '%');
            $constraintsOr[] = $query->like('attribute1', '%' . $searchWord . '%');
            $constraintsOr[] = $query->like('attribute2', '%' . $searchWord . '%');
            $constraintsOr[] = $query->like('attribute3', '%' . $searchWord . '%');
            $constraintsOr[] = $query->like('attribute4', '%' . $searchWord . '%');
        } else {
            // Wenn Felder vorgegeben sind
            foreach ($searchFields as $searchField) {
                $constraintsOr[] = $query->like($searchField, '%' . $searchWord . '%');
            }
        }
        $constraints[] = $query->logicalOr(...$constraintsOr);

        if (!empty($constraints)) {
            $query->matching(
                $query->logicalAnd(...$constraints)
            );
        }

        $query->setOrderings(
            [
                'first_in_list' => QueryInterface::ORDER_DESCENDING,
                'title' => QueryInterface::ORDER_DESCENDING]
        );

        return $query->execute();
    }

    /**
     * @param string $priceType
     * @return array|QueryResultInterface
     * @throws InvalidQueryException
     * @noinspection PhpUnused
     */
    public function findByPriceType(string $priceType = '')
    {
        $query = $this->createQuery();
        $constraints = [
            $query->equals('parent', '0'),
            $query->equals('price_type', $priceType),
        ];
        if (!empty($constraints)) {
            $query->matching(
                $query->logicalAnd(...$constraints)
            );
        }
        return $query->execute();
    }

    /**
     * @param array $filter
     * @param bool $count
     * @return array|QueryResultInterface|int
     * @throws InvalidQueryException
     */
    public function findAllForBackendList(array $filter = [], bool $count = false)
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setIgnoreEnableFields(true);
        $query->getQuerySettings()->setRespectStoragePage(true);
        $constraints = [];
        $constraints[] = $query->equals('parent', '0');

        if (isset($filter['category']) && $filter['category']['selected'] > 0) {
            $constraints[] = $query->contains('categories', $filter['category']['selected']);
        }
        if (isset($filter['tag']) && $filter['tag']['selected'] > 0) {
            $constraints[] = $query->contains('tags', $filter['tag']['selected']);
        }
        if (isset($filter['searchWord']) && $filter['searchWord'] !== '') {
            $searchWordConstraints = [];
            $searchWordConstraints[] = $query->like('title', '%' . $filter['searchWord'] . '%');
            $searchWordConstraints[] = $query->like('subtitle', '%' . $filter['searchWord'] . '%');
            $searchWordConstraints[] = $query->like('teaser', '%' . $filter['searchWord'] . '%');
            $searchWordConstraints[] = $query->like('description', '%' . $filter['searchWord'] . '%');
            $constraints[] = $query->logicalOr(...$searchWordConstraints);
        }
        if (isset($filter['digital']['selected'])) {
            switch ($filter['digital']['selected']) {
                case 'digital':
                    $constraints[] = $query->equals('digital_product', 1);
                    break;
                case 'non_digital':
                    $constraints[] = $query->equals('digital_product', 0);
                    break;
            }
        }
        if (isset($filter['disabled']['selected']) && $filter['disabled']['selected'] > -1) {
            switch ($filter['disabled']['selected']) {
                case 'active':
                    $constraints[] = $query->equals('hidden', 0);
                    break;
                case 'inactive':
                    $constraints[] = $query->equals('hidden', 1);
                    break;
            }
        }
        if (isset($filter['productType']['selected']) && $filter['productType']['selected'] > 0) {
            $constraints[] = $query->equals('product_type', $filter['productType']['selected']);
        }
        if (count($constraints) > 1) {
            $query->matching($query->logicalAnd(...$constraints));
        } elseif (count($constraints) === 1) {
            $query->matching($constraints[0]);
        }
        if (!$count) {
            if (isset($filter['sortingField']) && $filter['sortingField'] != '' && is_string($filter['sortingField'])) {
                if ($filter['sortingOrder'] == 'asc') {
                    $query->setOrderings([$filter['sortingField'] => QueryInterface::ORDER_ASCENDING]);
                } else {
                    if ($filter['sortingOrder'] == 'desc') {
                        $query->setOrderings([$filter['sortingField'] => QueryInterface::ORDER_DESCENDING]);
                    }
                }
            }
            if ((int)$filter['limit'] > 0) {
                $query->setOffset((int)$filter['offset']);
                $query->setLimit((int)$filter['limit']);
            }
            return $query->execute();
        }
        return $query->execute()->count();
    }

    /**
     * @return array|QueryResultInterface
     */
    public function findAllForBackendSelection()
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setIgnoreEnableFields(true);
        $query->getQuerySettings()->setRespectStoragePage(true);
        $query->setOrderings(['title' => QueryInterface::ORDER_ASCENDING]);
        return $query->execute();
    }

    /**
     * @param int $count
     * @return array|QueryResultInterface
     */
    public function findTopClickedProducts(int $count = 10)
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setIgnoreEnableFields(true);
        $query->getQuerySettings()->setRespectStoragePage(true);
        $query->setOrderings(['viewsWeek' => QueryInterface::ORDER_DESCENDING]);
        $query->setLimit($count);
        return $query->execute();
    }

    /**
     * @return array|QueryResultInterface
     */
    public function findVariants()
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setIgnoreEnableFields(true);
        $query->getQuerySettings()->setRespectStoragePage(true);
        $query->greaterThan('parent', '0');
        return $query->execute();
    }

    /**
     * Find data by uid, but ignore enable fields
     *
     * @param int $uid
     * @return object|null
     */
    public function findByIdentifierIgnoreEnableFields(int $uid): ?object
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setIgnoreEnableFields(true);
        $query->matching(
            $query->equals('uid', $uid)
        );
        return $query->execute()->getFirst();
    }

    /**
     * Find data by uid, but ignore enable fields
     *
     * @param int $uid
     * @return object|null
     */
    public function findByIdentifierIgnoreEnableFieldsAndStorage(int $uid): ?object
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setIgnoreEnableFields(true);
        $query->getQuerySettings()->setRespectStoragePage(false);
        $query->matching(
            $query->equals('uid', $uid)
        );
        return $query->execute()->getFirst();
    }
}
