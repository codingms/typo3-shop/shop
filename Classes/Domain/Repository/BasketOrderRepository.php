<?php

declare(strict_types=1);

namespace CodingMs\Shop\Domain\Repository;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\Shop\Domain\Model\BasketOrder;
use CodingMs\Shop\Domain\Model\FrontendUser;
use CodingMs\Shop\Domain\Model\FrontendUserGroup;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Driver\Exception;
use Doctrine\DBAL\Result;
use PDO;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\DomainObject\DomainObjectInterface;
use TYPO3\CMS\Extbase\Persistence\Generic\Mapper\DataMapper;
use TYPO3\CMS\Extbase\Persistence\QueryInterface;
use TYPO3\CMS\Extbase\Persistence\Repository;

/**
 * Basket order repository
 *
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */
class BasketOrderRepository extends Repository
{
    /**
     * @return DataMapper
     */
    protected function getDataMapper(): DataMapper
    {
        /** @var DataMapper $dataMapper */
        $dataMapper = GeneralUtility::makeInstance(DataMapper::class);
        return $dataMapper;
    }

    /**
     * Find the Basket-Order by uid
     *
     * @param int $uid
     * @return object
     */
    public function findOneByStripePaymentIntent(string $stripePaymentIntent)
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(false);
        $query->matching(
            $query->equals('stripe_payment_intent', $stripePaymentIntent)
        );
        $query->setLimit(1);
        return $query->execute()->getFirst();
    }

    /**
     * Find by frontend user
     *
     * @param FrontendUser $frontendUser
     * @return array<BasketOrder>
     */
    public function findByFrontendUser(FrontendUser $frontendUser): array
    {
        /** @var ConnectionPool $connectionPool */
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $queryBuilder = $connectionPool->getQueryBuilderForTable('tx_shop_domain_model_basketorder');
        $queryBuilder->select('*')
            ->from('tx_shop_domain_model_basketorder')
            ->where('frontend_user = :user AND status != "prepared"')
            ->setParameters(['user' => $frontendUser->getUid()]);
        //
        // Map with record type
        $basketOrders = [];
        $result = $queryBuilder->execute();
        if ($result instanceof Result) {
            $basketOrders = $this->getDataMapper()->map(BasketOrder::class, $result->fetchAllAssociative());
        }
        return $basketOrders;
    }

    /**
     * @param array $filter
     * @param bool $count
     * @return array|BasketOrder[]|int|DomainObjectInterface[]
     * @throws \Doctrine\DBAL\Exception
     */
    public function findAllForBackendList(array $filter = [], bool $count = false)
    {
        /** @var ConnectionPool $connectionPool */
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $queryBuilder = $connectionPool->getQueryBuilderForTable('tx_crm_domain_model_basket_order');
        // Use alias for table in case join needed for product filter
        $queryBuilder->select('basketOrder.*')
            ->from('tx_shop_domain_model_basketorder', 'basketOrder');
        $queryBuilder->andWhere($queryBuilder->expr()->eq(
            'basketOrder.pid',
            $queryBuilder->createNamedParameter($filter['pid'], PDO::PARAM_INT)
        ));
        //
        if (isset($filter['status']['selected']) && $filter['status']['selected'] !== 'all') {
            $queryBuilder->andWhere($queryBuilder->expr()->eq(
                'basketOrder.status',
                $queryBuilder->createNamedParameter($filter['status']['selected'])
            ));
        }
        if (isset($filter['processed']['selected']) && $filter['processed']['selected'] !== 'all') {
            $queryBuilder->andWhere($queryBuilder->expr()->eq(
                'basketOrder.processed',
                $queryBuilder->createNamedParameter($filter['processed']['selected'])
            ));
        }
        if (isset($filter['type']['selected']) && $filter['type']['selected'] !== 'all') {
            $queryBuilder->andWhere($queryBuilder->expr()->eq(
                'basketOrder.type',
                $queryBuilder->createNamedParameter($filter['type']['selected'])
            ));
        }
        if (isset($filter['searchWord']) && $filter['searchWord'] !== '') {
            // If search word is a number query invoice number only
            if (is_numeric($filter['searchWord'])) {
                $queryBuilder->andWhere(
                    $queryBuilder->expr()->like(
                        'basketOrder.invoice_number',
                        $queryBuilder->createNamedParameter('%' . $queryBuilder->escapeLikeWildcards($filter['searchWord']) . '%', PDO::PARAM_STR)
                    )
                );
            } else {
                $queryBuilder->andWhere(
                    $queryBuilder->expr()->or(
                        $queryBuilder->expr()->like(
                            'basketOrder.name',
                            $queryBuilder->createNamedParameter( '%' . $queryBuilder->escapeLikeWildcards($filter['searchWord']) . '%', PDO::PARAM_STR)
                        ),
                        $queryBuilder->expr()->like(
                            'basketOrder.email',
                            $queryBuilder->createNamedParameter('%' . $queryBuilder->escapeLikeWildcards($filter['searchWord']) . '%', PDO::PARAM_STR)
                        ),
                    )
                );
            }
        }
        if (isset($filter['product']['selected']) && $filter['product']['selected'] > 0) {
            $queryBuilder->join('basketOrder', 'tx_shop_domain_model_basketitem', 'basketItem', 'basketOrder.basket = basketItem.basket');
            $queryBuilder->join('basketItem', 'tx_shop_domain_model_product', 'product', 'basketItem.product = product.uid');
            $queryBuilder->andWhere(
                $queryBuilder->expr()->eq(
                    'product.uid',
                    $queryBuilder->createNamedParameter($filter['product']['selected'])
                )
            );
        }
        if (!$count) {
            if (isset($filter['sortingField']) && $filter['sortingField'] != '') {
                $sortingField = $filter['sortingField'];
                if (isset($filter['fields'][$filter['sortingField']]['sortingField'])) {
                    $sortingField = $filter['fields'][$filter['sortingField']]['sortingField'];
                }
                if ($filter['sortingOrder'] == 'asc') {
                    $queryBuilder->orderBy($sortingField, 'ASC');
                } else {
                    $queryBuilder->orderBy($sortingField, 'DESC');
                }
            }
            if (isset($filter['limit']) && (int)$filter['limit'] > 0) {
                $queryBuilder->setFirstResult((int)$filter['offset']);
                $queryBuilder->setMaxResults((int)$filter['limit']);
            }
            return $this->getDataMapper()->map(BasketOrder::class, $queryBuilder->executeQuery()->fetchAllAssociative());
        }
        return $queryBuilder->executeQuery()->rowCount();
    }

    /**
     * @param int $pageUid
     * @return array
     * @throws \Doctrine\DBAL\Exception
     */
    public function findAllContainedProducts(int $pageUid = 0)
    {
        /** @var ConnectionPool $connectionPool */
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $queryBuilder = $connectionPool->getQueryBuilderForTable('tx_crm_domain_model_basket_order');
        $queryBuilder->select('product.uid', 'product.title')
            ->from('tx_shop_domain_model_basketorder', 'basketOrder')
            ->join('basketOrder', 'tx_shop_domain_model_basketitem', 'basketItem', 'basketOrder.basket = basketItem.basket')
            ->join('basketItem', 'tx_shop_domain_model_product', 'product', 'basketItem.product = product.uid')
            ->groupBy('product.uid')
            ->orderBy('product.title');

        $queryBuilder->andWhere($queryBuilder->expr()->eq(
            'basketOrder.pid',
            $queryBuilder->createNamedParameter($pageUid, PDO::PARAM_INT)
        ));
        //
        $products = $queryBuilder->executeQuery()->fetchAllAssociative();
        $result = [];
        foreach ($products as $product) {
            $result[$product['uid']] = $product['title'];
        }
        return $result;
    }

    /**
     * Find the Basket-Order by uid
     *
     * @param int $uid
     * @return object
     */
    public function findOneByUid(int $uid)
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(false);

        $query->matching(
            $query->equals('uid', $uid)
        );

        $query->setLimit(1);
        return $query->execute()->getFirst();
    }

    /**
     * @return BasketOrder object
     */
    public function findHighestInvoiceNumber():object|null
    {
        $query = $this->createQuery();
        // For creating return type basket order from web list
        $query->getQuerySettings()->setRespectStoragePage(false);
        $query->setOrderings(['invoiceNumber' => QueryInterface::ORDER_DESCENDING]);
        $query->setLimit(1);
        return $query->execute()->getFirst();
    }

    /**
     * @param FrontendUser $frontendUser
     * @param array<string> $filter
     * @return array
     * @throws DBALException
     * @throws Exception
     */
    public function findByFrontendUserStatus(FrontendUser $frontendUser, array $filter = []): array
    {
        /** @var ConnectionPool $connectionPool */
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $queryBuilder = $connectionPool->getQueryBuilderForTable('tx_shop_domain_model_basketorder');
        $queryBuilder->select('*')
            ->from('tx_shop_domain_model_basketorder')
            ->where(
                $queryBuilder->expr()->eq(
                    'frontend_user',
                    $queryBuilder->createNamedParameter($frontendUser->getUid(), PDO::PARAM_INT)
                )
            );

        if (count($filter) > 0) {
            $constraintsStatus = [];
            foreach ($filter as $status) {
                $constraintsStatus[] = $queryBuilder->expr()->eq(
                    'status',
                    $queryBuilder->createNamedParameter($status)
                );
            }
            $queryBuilder->andWhere(
                $queryBuilder->expr()->orX(...$constraintsStatus)
            );
        }
        $basketOrders = [];
        if (($result = $queryBuilder->execute()) instanceof Result) {
            $basketOrders = $this->getDataMapper()->map(BasketOrder::class, $result->fetchAllAssociative());
        }
        return $basketOrders;
    }

    public function findByFrontendUserAndOrderGroup(FrontendUser $frontendUser, FrontendUserGroup $frontendUserOrderGroup, array $filter = []): array
    {
        /** @var ConnectionPool $connectionPool */
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $queryBuilder = $connectionPool->getQueryBuilderForTable('tx_shop_domain_model_basketorder');
        $queryBuilder->select('user.*')
            ->addSelectLiteral('SUM(order.price_with_tax) AS orderSummary')
            ->addSelectLiteral('COUNT(order.uid) AS orderCount')
            ->addSelect('user.uid as userUid', 'user.tx_shop_order_group as userShopGroup')
            ->from('tx_shop_domain_model_basketorder', 'order')
            ->innerJoin(
                'order',
                'fe_users',
                'user',
                'user.uid = order.frontend_user AND user.tx_shop_order_group = ' . $frontendUserOrderGroup->getUid()
            )->groupBy('user.uid');

        if (count($filter) > 0) {
            $constraintsStatus = [];
            foreach ($filter as $status) {
                $constraintsStatus[] = $queryBuilder->expr()->eq(
                    'status',
                    $queryBuilder->createNamedParameter($status)
                );
            }
            $queryBuilder->andWhere(
                $queryBuilder->expr()->orX(...$constraintsStatus)
            );
        }
        $basketOrders = [];
        if (($result = $queryBuilder->execute()) instanceof Result) {
            foreach ($result->fetchAllAssociative() as $frontendUser) {
                $basketOrders[] = [
                    'orderCount' => $frontendUser['orderCount'],
                    'orderSummary' => $frontendUser['orderSummary'] / 100,
                    'frontendUser' => $this->getDataMapper()->map(FrontendUser::class, [$frontendUser])[0],
                ];
            }
        }
        return $basketOrders;
    }

    /**
     * Find data by uid, but ignore enable fields
     *
     * @param int $uid
     * @return object|null
     */
    public function findByIdentifierIgnoreEnableFieldsAndStorage(int $uid): ?object
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setIgnoreEnableFields(true);
        $query->getQuerySettings()->setRespectStoragePage(false);
        $query->matching(
            $query->equals('uid', $uid)
        );
        return $query->execute()->getFirst();
    }
}
