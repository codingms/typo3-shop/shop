<?php

namespace CodingMs\Shop\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 Thomas Deuling <typo3@coding.ms>, coding.ms
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\Shop\Domain\Model\Traits\OrderGroupTrait;
use CodingMs\Shop\Domain\Model\Traits\StripeIdTrait;

/**
 * Frontend user
 *
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */
class FrontendUser extends \CodingMs\Modules\Domain\Model\FrontendUser
{
    use StripeIdTrait;
    use OrderGroupTrait;

    public function getValueForCheckout(string $field): string
    {
        if ($field === 'street') {
            $field = 'address';
        }
        switch ($field) {
            case 'company':
                return $this->getCompany();
            case 'vatId':
                return $this->getVatNumber();
            case 'name':
                return $this->getName();
            case 'title':
                return $this->getTitle();
            case 'firstname':
                return $this->getFirstName();
            case 'lastname':
                return $this->getLastName();
            case 'gender':
                return $this->getGender();
            case 'address':
                return $this->getAddress();
            case 'postalCode':
                return $this->getZip();
            case 'city':
                return $this->getCity();
            case 'country':
                return $this->getCountry();
            case 'phone':
                return $this->getTelephone();
            case 'email':
                return $this->getEmail();
        }
        return '';
    }

    public function isValueForCheckout(string $field): bool
    {
        return in_array(
            $field,
            [
                'company',
                'vatId',
                'gender',
                'firstname',
                'lastname',
                'address',
                'street',
                'houseNumber',
                'postalCode',
                'city',
                'country',
                'phone',
                'email'
            ]
        );
    }
}
