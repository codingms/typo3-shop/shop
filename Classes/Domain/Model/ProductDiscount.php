<?php

declare(strict_types=1);

namespace CodingMs\Shop\Domain\Model;

use CodingMs\AdditionalTca\Domain\Model\Traits\LabelTrait;
use CodingMs\AdditionalTca\Domain\Model\Traits\TypeStringTrait;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Product discount
 */
class ProductDiscount extends Base
{
    use LabelTrait;
    use TypeStringTrait;

    protected string $labelPublic = '';
    protected string $orderOption = '';
    protected int $valueFixed = 0;
    protected int $valuePercent = 0;

    public function getLabelPublic(): string
    {
        return $this->labelPublic;
    }
    public function setLabelPublic(string $labelPublic): void
    {
        $this->labelPublic = $labelPublic;
    }

    public function getOrderOption(): string
    {
        return $this->orderOption;
    }
    public function setOrderOption(string $orderOption): void
    {
        $this->orderOption = $orderOption;
    }

    public function getValueFixed(): int
    {
        return $this->valueFixed;
    }
    public function setValueFixed(int $valueFixed): void
    {
        $this->valueFixed = $valueFixed;
    }

    public function getValuePercent(): int
    {
        return $this->valuePercent;
    }
    public function setValuePercent(int $valuePercent): void
    {
        $this->valuePercent = $valuePercent;
    }

    public function getValue(): int
    {
        $value = $this->getValueFixed();
        if ($this->getType() === 'percent') {
            $value = $this->getValuePercent();
        }
        return $value;
    }
    public function getValueAsFloat(): float
    {
        return round($this->getValue() / 100, 2);
    }

    /**
     * @return array<string, mixed>
     */
    public function toArray()
    {
        return [
            'label' => $this->getLabel(),
            'type' => $this->getType(),
            'orderOption' => $this->getOrderOption(),
            'value' => $this->getValue(),
            'valueAsFloat' => $this->getValueAsFloat(),
        ];
    }
}
