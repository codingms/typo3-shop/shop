<?php

namespace CodingMs\Shop\Domain\Model\Traits\Product;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\Shop\Domain\Model\ProductTag;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

trait TagsTrait
{
    /**
     * @var ObjectStorage<ProductTag>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Lazy
     */
    protected ObjectStorage $tags;

    public function addTag(ProductTag $tag): void
    {
        $this->tags->attach($tag);
    }

    public function removeTag(ProductTag $tagToRemove): void
    {
        $this->tags->detach($tagToRemove);
    }

    /**
     * Returns the tags
     *
     * @return ObjectStorage<ProductTag> $tags
     */
    public function getTags(): ObjectStorage
    {
        return $this->tags;
    }

    /**
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\Shop\Domain\Model\ProductTag> $tags
     */
    public function setTags(ObjectStorage $tags): void
    {
        $this->tags = $tags;
    }

    /**
     * Returns the tags as a String
     *
     * @return string
     */
    public function getTagsString(): string
    {
        $tagsArray = [];
        $tags = $this->getTags();
        if (!empty($tags)) {
            foreach ($tags as $tag) {
                /** @var \CodingMs\Shop\Domain\Model\ProductTag $tag */
                $tagsArray[] = $tag->getTitle();
            }
        }
        return implode(', ', $tagsArray);
    }

    /**
     * @return string
     */
    public function getTagsForBackend(): string
    {
        $html = [];
        $tags = $this->getTags();
        if (!empty($tags)) {
            foreach ($tags as $tag) {
                $html[$tag->getTitle()] = $tag->getTitle();
            }
        }
        ksort($html);
        return implode('<br />', $html);
    }
}
