<?php

namespace CodingMs\Shop\Domain\Model\Traits\Product;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\Shop\Domain\Model\Product;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

trait AccessoriesRequiredTrait
{
    /**
     * @var ObjectStorage<Product>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Lazy
     */
    protected ObjectStorage $accessoriesRequired;

    /**
     * Adds a required accessory
     *
     * @param Product $accessoryRequired
     */
    public function addAccessoriesRequired(Product $accessoryRequired): void
    {
        if (ExtensionManagementUtility::isLoaded('shop_pro')) {
            $this->accessoriesRequired->attach($accessoryRequired);
        }
    }

    /**
     * Removes a required accessory
     *
     * @param Product $accessoryRequiredToRemove The Product to be removed
     */
    public function removeAccessoriesRequired(Product $accessoryRequiredToRemove): void
    {
        if (ExtensionManagementUtility::isLoaded('shop_pro')) {
            $this->accessoriesRequired->detach($accessoryRequiredToRemove);
        }
    }

    /**
     * Returns the required accessories
     *
     * @return ObjectStorage<Product> $accessoriesRequired
     */
    public function getAccessoriesRequired(): ObjectStorage
    {
        if (ExtensionManagementUtility::isLoaded('shop_pro')) {
            // Ignore own product
            $ignoreProductUids = [
                $this->getUid()
            ];
            if (($parent = $this->getParent()) instanceof Product) {
                // And ignore own parent product
                $ignoreProductUids[] = (int)$parent->getUid();
            }
            //
            // Check if the required accessories are on parent product
            if ($this->isVariant() && !$this->getProductType()->isAccessoriesRequiredEnabledVariant()) {
                $this->accessoriesRequired = $this->parent->getAccessoriesRequired();
            }
            foreach ($this->accessoriesRequired as $accessoryRequired) {
                if (in_array($accessoryRequired->getUid(), $ignoreProductUids)) {
                    $this->accessoriesRequired->detach($accessoryRequired);
                }
            }
        }
        return $this->accessoriesRequired;
    }

    /**
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<Product> $attributes
     */
    public function setAccessoriesRequired(ObjectStorage $accessoriesRequired): void
    {
        if (ExtensionManagementUtility::isLoaded('shop_pro')) {
            $this->accessoriesRequired = $accessoriesRequired;
        }
    }

    /**
     * @return string
     */
    public function getAccessoriesRequiredForBackend(): string
    {
        if (ExtensionManagementUtility::isLoaded('shop_pro')) {
            $html = [];
            foreach ($this->getAccessoriesRequired() as $accessoryRequired) {
                $html[] = $accessoryRequired->getTitle();
            }
            return implode('<br />', $html);
        }
        return '';
    }
}
