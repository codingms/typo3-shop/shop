<?php

namespace CodingMs\Shop\Domain\Model\Traits\Product;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\Shop\Domain\Model\Product;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

trait AccessoriesTrait
{
    /**
     * @var ObjectStorage<Product>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Lazy
     */
    protected ObjectStorage $accessories;

    /**
     * Adds an accessory
     *
     * @param Product $accessory
     */
    public function addAccessories(Product $accessory): void
    {
        if (ExtensionManagementUtility::isLoaded('shop_pro')) {
            $this->accessories->attach($accessory);
        }
    }

    /**
     * Removes an accessory
     *
     * @param Product $accessoryToRemove The Product to be removed
     */
    public function removeAccessories(Product $accessoryToRemove): void
    {
        if (ExtensionManagementUtility::isLoaded('shop_pro')) {
            $this->accessories->detach($accessoryToRemove);
        }
    }

    /**
     * Returns the accessory
     *
     * @return ObjectStorage<Product> $accessories
     */
    public function getAccessories(): ObjectStorage
    {
        if (ExtensionManagementUtility::isLoaded('shop_pro')) {
            // Ignore own product
            $ignoreProductUids = [
                $this->getUid()
            ];
            if (($parent = $this->getParent()) instanceof Product) {
                // And ignore own parent product
                $ignoreProductUids[] = (int)$parent->getUid();
            }
            //
            // Check if the accessories are on parent product
            if ($this->isVariant() && !$this->getProductType()->isAccessoriesEnabledVariant()) {
                $this->accessories = $this->parent->getAccessories();
            }
            foreach ($this->accessories as $accessory) {
                if (in_array($accessory->getUid(), $ignoreProductUids)) {
                    $this->accessories->detach($accessory);
                }
            }
        }
        return $this->accessories;
    }

    /**
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<Product> $attributes
     */
    public function setAccessories(ObjectStorage $accessories): void
    {
        if (ExtensionManagementUtility::isLoaded('shop_pro')) {
            $this->accessories = $accessories;
        }
    }

    /**
     * @return string
     */
    public function getAccessoriesForBackend(): string
    {
        if (ExtensionManagementUtility::isLoaded('shop_pro')) {
            $html = [];
            foreach ($this->getAccessories() as $accessory) {
                $html[] = $accessory->getTitle();
            }
            return implode('<br />', $html);
        }
        return '';
    }
}
