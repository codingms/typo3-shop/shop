<?php

namespace CodingMs\Shop\Domain\Model\Traits\Product;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\Shop\Domain\Model\ProductGraduatedPrice;
use Exception;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

/**
 * GraduatedPrices
 */
trait GraduatedPricesTrait
{
    /**
     * @var ObjectStorage<ProductGraduatedPrice>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Lazy
     */
    protected ObjectStorage $graduatedPrices;

    /**
     * @return ObjectStorage<ProductGraduatedPrice> $graduatedPrices
     */
    public function getGraduatedPrices(): ObjectStorage
    {
        return $this->graduatedPrices;
    }

    /**
     * @param ObjectStorage<ProductGraduatedPrice> $graduatedPrices
     */
    public function setGraduatedPrices(ObjectStorage $graduatedPrices): void
    {
        $this->graduatedPrices = $graduatedPrices;
    }

    /**
     * @param int $quantity
     * @param string|null $countryCode
     * @return array
     * @throws Exception
     */
    public function getGraduatedPrice(int $quantity, ?string $countryCode): array
    {
        //
        // Ensure the product is properly configured
        if (!in_array($this->getPriceType(), ['net', 'gross'])) {
            throw new Exception('Invalid price type - try to run shop product migration wizard or ensure having selected a value price type in product with uid ' . $this->getUid());
        }
        //
        // Initialize the price for quantity one
        $graduatedPricesPossible = [
            'quantity' => 1,
            'price' => $this->getPrice(),
            'priceWithTax' => $this->getPriceWithTax(),
            'tax' => $this->getTaxForCountry($countryCode),
        ];
        $graduatedPrices = $this->getGraduatedPrices();
        if (count($graduatedPrices) > 0) {
            /** @var ProductGraduatedPrice $graduatedPrice */
            foreach ($graduatedPrices as $graduatedPrice) {
                if ($graduatedPrice->getQuantity() <= $quantity) {
                    if ($graduatedPricesPossible['quantity'] <= $graduatedPrice->getQuantity()) {
                        $graduatedPricesPossible['quantity'] = $graduatedPrice->getQuantity();
                        //
                        // Refresh pure tax and price values depending on type net/gross
                        if ($this->getPriceType() === 'net') {
                            $graduatedPricesPossible['price'] = $graduatedPrice->getPrice();
                            if ($graduatedPricesPossible['tax'] === 0) {
                                $graduatedPricesPossible['priceWithTax'] = $graduatedPricesPossible['price'];
                            } else {
                                $graduatedPricesPossible['priceWithTax'] = (int)round(
                                    $graduatedPricesPossible['price'] * (10000 + $graduatedPricesPossible['tax']) / 10000
                                );
                            }
                        } else {
                            $graduatedPricesPossible['priceWithTax'] = $graduatedPrice->getPrice();
                            if ($graduatedPricesPossible['tax'] === 0) {
                                $graduatedPricesPossible['price'] = $graduatedPricesPossible['priceWithTax'];
                            } else {
                                $graduatedPricesPossible['price'] = (int)round(
                                    $graduatedPricesPossible['priceWithTax'] / (1 + ($graduatedPricesPossible['tax'] / 10000))
                                );
                            }
                        }
                    }
                }
            }
        }
        return $graduatedPricesPossible;
    }
}
