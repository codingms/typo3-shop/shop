<?php

namespace CodingMs\Shop\Domain\Model\Traits\Product;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\Shop\Domain\Model\Product;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

trait RelatedProductsTrait
{
    /**
     * @var ObjectStorage<Product>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Lazy
     */
    protected ObjectStorage $relatedProducts;

    /**
     * Adds a related product
     *
     * @param Product $relatedProduct
     */
    public function addRelatedProducts(Product $relatedProduct): void
    {
        $this->relatedProducts->attach($relatedProduct);
    }

    /**
     * Removes a related product
     *
     * @param Product $relatedProductToRemove The Product to be removed
     */
    public function removeRelatedProducts(Product $relatedProductToRemove): void
    {
        $this->relatedProducts->detach($relatedProductToRemove);
    }

    /**
     * Returns the related products
     *
     * @return ObjectStorage<Product> $relatedProducts
     */
    public function getRelatedProducts(): ObjectStorage
    {
        // Ignore own product
        $ignoreProductUids = [
            $this->getUid()
        ];
        if (($parent = $this->getParent()) instanceof Product) {
            // And ignore own parent product
            $ignoreProductUids[] = (int)$parent->getUid();
        }
        //
        // Check if the related products are on parent product
        if ($this->isVariant() && !$this->getProductType()->isRelatedProductsEnabledVariant()) {
            $this->relatedProducts = $this->parent->getRelatedProducts();
        }
        foreach ($this->relatedProducts as $relatedProduct) {
            if (in_array($relatedProduct->getUid(), $ignoreProductUids)) {
                $this->relatedProducts->detach($relatedProduct);
            }
        }
        return $this->relatedProducts;
    }

    /**
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<Product> $attributes
     */
    public function setRelatedProducts(ObjectStorage $relatedProducts): void
    {
        $this->relatedProducts = $relatedProducts;
    }

    /**
     * @return string
     */
    public function getRelatedProductsForBackend(): string
    {
        $html = [];
        foreach ($this->getRelatedProducts() as $relatedProduct) {
            $html[] = $relatedProduct->getTitle();
        }
        return implode('<br />', $html);
    }
}
