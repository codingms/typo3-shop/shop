<?php

namespace CodingMs\Shop\Domain\Model\Traits;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\Shop\Domain\Model\ProductAttribute;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;

trait ProductAttributesTrait
{
    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<ProductAttribute>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Lazy
     */
    protected $attributes;

    public function addAttribute(ProductAttribute $attribute): void
    {
        $this->attributes->attach($attribute);
    }

    public function removeAttribute(ProductAttribute $attribute): void
    {
        $this->attributes->detach($attribute);
    }

    /**
     * Returns the attributes
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<ProductAttribute> $attributes
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\Shop\Domain\Model\ProductAttribute> $attributes
     */
    public function setAttributes(ObjectStorage $attributes): void
    {
        $this->attributes = $attributes;
    }

    public function getAttributesGroupedByCategory(): array
    {
        $grouped = [];
        if ($this->attributes->count() > 0) {
            /** @var ProductAttribute $attribute */
            foreach ($this->attributes as $attribute) {
                $category = $attribute->getCategory();
                $categoryUid = $category->getUid();
                if (!isset($grouped[$categoryUid])) {
                    $grouped[$categoryUid] = [];
                    $grouped[$categoryUid]['category'] = $category;
                }
                $grouped[$categoryUid]['items'][] = $attribute;
            }
        }
        return $grouped;
    }
    public function getAttributesForBackend(): string
    {
        $html = '';
        foreach ($this->getAttributesGroupedByCategory() as $category) {
            $html .= '<b>' . $category['category']->getTitle() . '</b>';
            $html .= '<ul>';
            foreach ($category['items'] as $item) {
                $html .= '<li>' . $item->getTitle() . '</li>';
            }
            $html .= '</ul>';
        }
        return $html;
    }
}
