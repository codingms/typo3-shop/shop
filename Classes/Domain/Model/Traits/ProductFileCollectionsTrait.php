<?php

namespace CodingMs\Shop\Domain\Model\Traits;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Driver\Exception;
use Doctrine\DBAL\Result;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Core\Resource\Collection\AbstractFileCollection;
use TYPO3\CMS\Core\Resource\ResourceFactory;
use TYPO3\CMS\Core\Utility\GeneralUtility;

trait ProductFileCollectionsTrait
{
    /**
     * @var int
     */
    protected int $fileCollections = 0;

    /**
     * @var array
     */
    protected array $fileCollectionsLoaded = [];

    /**
     * Returns the file collections
     * @return array<AbstractFileCollection>
     * @throws DBALException
     */
    public function getFileCollections(): array
    {
        if (count($this->fileCollectionsLoaded)) {
            return $this->fileCollectionsLoaded;
        }
        try {
            /** @var QueryBuilder $queryBuilder */
            $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
                ->getQueryBuilderForTable('tx_shop_product_productfilecollection_mm');
            $result = $queryBuilder
                ->select('uid_foreign')
                ->from('tx_shop_product_productfilecollection_mm')
                ->where(
                    $queryBuilder->expr()->eq('uid_local', $queryBuilder->createNamedParameter($this->getUid()))
                )->orderBy('sorting')->execute();
            if ($result instanceof Result) {
                $resourceFactory = GeneralUtility::makeInstance(ResourceFactory::class);
                foreach ($result->fetchAllAssociative() as $relation) {
                    $fileCollection = $resourceFactory->getCollectionObject($relation['uid_foreign']);
                    $fileCollection->loadContents();
                    $this->fileCollectionsLoaded[] = $fileCollection;
                }
            }
        } catch (Exception $e) {
            $this->fileCollectionsLoaded = [];
        }
        return $this->fileCollectionsLoaded;
    }
}
