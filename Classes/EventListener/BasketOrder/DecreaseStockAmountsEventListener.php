<?php

namespace CodingMs\Shop\EventListener\BasketOrder;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2019 Thomas Deuling <typo3@coding.ms>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\Shop\Domain\Model\Product;
use CodingMs\Shop\Domain\Repository\ProductRepository;
use CodingMs\Shop\Event\BasketOrder\AfterSuccessfulOrderEvent;
use TYPO3\CMS\Core\Log\LogLevel;
use TYPO3\CMS\Core\Log\LogManager;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager;

class DecreaseStockAmountsEventListener
{

    protected ProductRepository $productRepository;
    protected PersistenceManager $persistenceManager;

    public function __construct(
        ProductRepository  $productRepository,
        PersistenceManager $persistenceManager
    )
    {
        $this->productRepository = $productRepository;
        $this->persistenceManager = $persistenceManager;
    }

    public function __invoke(AfterSuccessfulOrderEvent $event): void
    {
        $logData = [];
        $basketOrder = $event->getBasketOrder();
        //
        // Decrease stock amount for items with limited stock
        foreach ($basketOrder->getBasket()->getBasketItems() as $basketItem) {
            /** @var Product $product */
            $product = $basketItem->getProduct();
            if (!$product->isDigitalProduct()) {
                $orderedAmount = $basketItem->getQuantity();
                $logDataEntry[] = 'Product: ' . $product->getTitle();
                $logDataEntry[] = 'Order-Quantity: ' . $orderedAmount;
                $logDataEntry[] = 'Product-IsInStock: ' . ($product->isInStock() ? 'true' : 'false');
                if ($product->isInStock()) {
                    $currentStock = $product->getStockAmount();
                    $logDataEntry[] = 'Product-Stock-Amount-Before: ' . $currentStock;
                    $product->setStockAmount($currentStock - $orderedAmount);
                    $logDataEntry[] = 'Product-Stock-Amount-After: ' . $product->getStockAmount();
                    $this->productRepository->update($product);
                    $this->persistenceManager->persistAll();
                }
                $logData[$product->getUid()] = $logDataEntry;
            }
        }
        foreach($logData as $productUid => $logForProduct) {
            $this->log(
                'DecreaseStockAmountsEventListener: Shop decrease stock amount product[' . $productUid . ']: '
                . implode(', ', $logForProduct)
            );
        }
    }

    /**
     * @param string $message
     */
    protected function log(string $message = ''): void
    {
        /** @var LogManager $logManager */
        $logManager = GeneralUtility::makeInstance(LogManager::class);
        $logger = $logManager->getLogger(__CLASS__);
        $logger->log(LogLevel::DEBUG, str_replace('%', '%%', $message));
    }
}
