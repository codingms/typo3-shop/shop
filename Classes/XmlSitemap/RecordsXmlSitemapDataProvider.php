<?php

declare(strict_types=1);

namespace CodingMs\Shop\XmlSitemap;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Seo\XmlSitemap\RecordsXmlSitemapDataProvider as RecordsXmlSitemapDataProviderParent;

/**
 * XmlSiteDataProvider for shop products
 */
class RecordsXmlSitemapDataProvider extends RecordsXmlSitemapDataProviderParent
{
    /**
     * @param array $data
     * @return array
     */
    protected function defineUrl(array $data): array
    {
        $pageId = $this->config['url']['pageId'] ?? $GLOBALS['TSFE']->id;
        if (substr($data['data']['canonical_link'], 0, strlen('t3://page?uid=')) === 't3://page?uid=') {
            $pageId = (int)str_replace('t3://page?uid=', '', $data['data']['canonical_link']);
        }
        $additionalParams = [];
        $additionalParams = $this->getUrlFieldParameterMap($additionalParams, $data['data']);
        $additionalParams = $this->getUrlAdditionalParams($additionalParams);
        $additionalParamsString = http_build_query(
            $additionalParams,
            '',
            '&',
            PHP_QUERY_RFC3986
        );
        $typoLinkConfig = [
            'parameter' => $pageId,
            'additionalParams' => $additionalParamsString ? '&' . $additionalParamsString : '',
            'forceAbsoluteUrl' => 1,
        ];
        $data['loc'] = $this->cObj->typoLink_URL($typoLinkConfig);
        return $data;
    }
}
