<?php

namespace CodingMs\Shop\DataProcessing;

use Doctrine\DBAL\Result;
use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Context\Exception\AspectNotFoundException;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Domain\Repository\PageRepository;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\CMS\Frontend\ContentObject\DataProcessorInterface;
use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;

class AddProductToMenuProcessor implements DataProcessorInterface
{
    /**
     * @param ContentObjectRenderer $cObj
     * @param array $contentObjectConfiguration
     * @param array $processorConfiguration
     * @param array $processedData
     * @return array
     */
    public function process(ContentObjectRenderer $cObj, array $contentObjectConfiguration, array $processorConfiguration, array $processedData): array
    {
        if (!$processorConfiguration['menus']) {
            return $processedData;
        }
        $productRecord = $this->getProductRecord();
        if ($productRecord) {
            $menus = GeneralUtility::trimExplode(',', $processorConfiguration['menus'], true);
            foreach ($menus as $menu) {
                if (isset($processedData[$menu])) {
                    $this->addProductRecordToMenu($productRecord, $processedData[$menu]);
                }
            }
        }
        return $processedData;
    }

    /**
     * Add the product record to the menu items
     *
     * @param array $productRecord
     * @param array $menu
     */
    protected function addProductRecordToMenu(array $productRecord, array &$menu): void
    {
        foreach ($menu as &$menuItem) {
            $menuItem['current'] = 0;
        }

        $menu[] = [
            'data' => $productRecord,
            'title' => $productRecord['title'],
            'active' => 1,
            'current' => 1,
            'link' => GeneralUtility::getIndpEnv('TYPO3_REQUEST_URL'),
            'isProduct' => true
        ];
    }

    /**
     * Get the product record including possible translations
     *
     * @return array
     */
    protected function getProductRecord(): array
    {
        $row = [];
        $productId = 0;
        $vars = GeneralUtility::_GET('tx_shop_products');
        if (isset($vars['product'])) {
            $productId = (int)$vars['product'];
        }
        if ($productId) {
            $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
                ->getQueryBuilderForTable('tx_shop_domain_model_product');
            $result = $queryBuilder
                ->select('*')
                ->from('tx_shop_domain_model_product')
                ->where(
                    $queryBuilder->expr()->eq('uid', $productId)
                )
                ->execute();
            if ($result instanceof Result) {
                $row = $result->fetchAssociative();
                if ($row) {
                    if (($pageRepository = $this->getTsfe()->sys_page) instanceof PageRepository) {
                        $row = $pageRepository->getRecordOverlay(
                            'tx_shop_domain_model_product',
                            $row,
                            $this->getCurrentLanguage()
                        );
                    }
                }
            }
        }
        return $row;
    }

    /**
     * Get current language
     *
     * @return int
     */
    protected function getCurrentLanguage(): int
    {
        $languageId = 0;
        $context = GeneralUtility::makeInstance(Context::class);
        try {
            $languageId = $context->getPropertyFromAspect('language', 'contentId');
        } catch (AspectNotFoundException $e) {
            // do nothing
        }

        return (int)$languageId;
    }

    /**
     * @return TypoScriptFrontendController
     */
    protected function getTsfe(): TypoScriptFrontendController
    {
        return $GLOBALS['TSFE'];
    }
}
