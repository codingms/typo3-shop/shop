<?php

namespace CodingMs\Shop\Command;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2022 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\Shop\Service\ViewCountService;
use Doctrine\DBAL\Result;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class UpdateViewCountCommand extends Command
{
    protected function configure(): void
    {
        $this
            ->setDescription('Updates view count for products.')
            ->addArgument('pages', InputArgument::OPTIONAL, 'Comma separated list of pids with products that should be updated');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        /** @var ConnectionPool $connectionPool */
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $queryBuilder = $connectionPool->getConnectionForTable('tx_shop_domain_model_product')->createQueryBuilder();
        $queryBuilder->select('uid')->from('tx_shop_domain_model_product');
        $pages = [];
        if ($input->hasArgument('pages')) {
            $pages = $input->getArgument('pages');
            $pages = GeneralUtility::trimExplode(',', $pages, true);
            $pages = array_map('intval', $pages);
        }
        if (count($pages) > 0) {
            $queryBuilder->where($queryBuilder->expr()->in(
                'pid',
                $queryBuilder->createNamedParameter($pages, Connection::PARAM_INT_ARRAY)
            ));
        } else {
            $queryBuilder->where($queryBuilder->expr()->gt(
                'pid',
                0
            ));
        }
        $queryBuilder->where($queryBuilder->expr()->eq(
            'parent',
            0
        ));
        $result = $queryBuilder->execute();
        if ($result instanceof Result) {
            $products = $result->fetchAllAssociative();
            foreach ($products as $product) {
                ViewCountService::updateViewerCountForProduct($product['uid']);
            }
        }
        return 0;
    }
}
